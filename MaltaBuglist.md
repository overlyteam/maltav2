## Change Log 

v0.3.14. 

- Removed those back buttons in Airplanes and Boats



v0.3.13. 

- UI issues fixed in Boat & airplane game
- material changed for AR planes 



v0.3.11. 

- Added background to GPS pin titles

- Scene videos can be moved now (TW)

  

v0.3.10. 

- reduced the area of scan (TW and planes)
- some bugs fixed for Planes HUD
- Removed Arrows from GPS



v0.3.9.

- Added thumbnails to TW ARBuilder menus
- path to thumbnails added to settings json file (see /StreamingAssets/VideoDescriptor)

- Submenus with video preview added to TW  ARBuilder object menu
- Correct fonts have bin added to all AR scenes
- Added watermark for TW ARBuilder (not working for iOS yet)
- Pins added to GPS scene



v0.3.8.

- Removed Scenes option from TW AR Builder 
- removed white horizontal positioning sphere from TW. Horizontal movement now is possible by moving video with single finger, vertical movement - two fingers
- Added button to TW, which allows to save captured content
- Removed yellow target indicator triangles from Airplane and Boat games
- Ground scanning updated for airplane game
- Submenus with video preview added to TW character menu

### Known bugs

- [ ] Not possible to return to main menu

###### GPS

- [x] Remove arrows from gps scene
- [x] add correct background and font for pin titles

###### TimeWindows

- [ ] no video preview (TW)
- [x] sometimes scene view crashes (TW)
- [x] no TW watermark in iOS
- [x] Grid appears when moving video in playmode
- [x] Video not recording
- [x] photos not saved
- [x] timer
- [x] app becomes unusable after video ends while filming
- [x] Video not shown after it fades away
- [x] it's possible to place only two videos



###### FighetrGames

- [x] Should set AR Marker size (might improve tracking time)
- [x] Planes wont get hit