﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using TMPro;
using System;

namespace Overly.FightGamesScripts
{
    public class AR_ManagerBUTTONS : MonoBehaviour
    {
        public Camera Arcamera;
        public ARTrackedImageManager ARTrackedImageManager;
        public ARSessionOrigin arSessionOrigin;
        private Game gameInstance;
        public gun_backup gunScript;
        //public lookAtBUTTONS lookAtScript;
        public GameObject cannon;
        ARTrackedImage trackedImage;
        public BattleshipsGame battleshipsGameScript;

        public GameObject spawnPointLeft;
        public GameObject spawnPointRight;
        public GameObject spawnPointContainer;
        public GameObject spawnPlane;

        public GameObject buttonL;
        public GameObject buttonR;
        public GameObject powerSlider;

        [HideInInspector] public float rotValueChange;
        [HideInInspector] public float xValueChange;
        [HideInInspector] public float yValueChange;
        [HideInInspector] public float zValueChange;
        [HideInInspector] public float sizeValueChange;
        [HideInInspector] public float speedValueChange;

        public TextMeshProUGUI rotValue;
        public TextMeshProUGUI xValue;
        public TextMeshProUGUI yValue;
        public TextMeshProUGUI zValue;
        public TextMeshProUGUI sizeValue;
        public TextMeshProUGUI speedValue;

        public Slider rotSlider;
        public Slider xSlider;
        public Slider ySlider;
        public Slider zSlider;
        public Slider sizeSlider;
        public Slider speedSlider;

        private float xValueSaved;
        private float yValueSaved;
        private float zValueSaved;
        private float sizeValueSaved;
        private float speedValueSaved;

        public GameObject UI_tutorial;

        private Vector3 originPos;

        //===========================FOR GAME LOGIC AND CONTROL=====================
        public timerBattleShipButtons timerManager;
        private bool gameStartedForFirstTime;
        private bool gameActive;
        private bool canScan;
        private bool inputActive;

        public float gameDuration;

        public GameObject HUD;

        public Image scanImage;
        public Image settingButton;

        private TextMeshProUGUI finalScoreText;
        private TextMeshProUGUI scanText;

        //ignore these, public only because other scripts need access these
        bool hasExecuted = false;
        [HideInInspector] public bool containerRotationChanged = false;

        private void Awake()
        {
            cannon.SetActive(false);
            scanText = scanImage.GetComponentInChildren<TextMeshProUGUI>();

            scanText.text = "TAP TO PLAY";
        }

        void Start()
        {
            rotValueChange = PlayerPrefs.GetFloat("rot", -95.33667f);//get saved values
            xValueChange = PlayerPrefs.GetFloat("x", 300f);//get saved values
            yValueChange = PlayerPrefs.GetFloat("y", -83.4846f);
            zValueChange = PlayerPrefs.GetFloat("z", 861.8521f);
            sizeValueChange = PlayerPrefs.GetFloat("size", 0.797451f);
            speedValueChange = PlayerPrefs.GetFloat("speed", 0.1f);


            print("y from save: " + yValueChange);
            xValue.text = "Spawnpoint distance: " + xValueChange;//APPLY SAVED VALUES TO TEXT
            yValue.text = "Height: " + yValueChange;
            rotValue.text = "Rotation: " + rotValueChange;
            zValue.text = "Distance: " + zValueChange;
            sizeValue.text = "Size: " + sizeValueChange;
            speedValue.text = "Speed: " + speedValueChange;

            rotSlider.value = rotValueChange;
            xSlider.value = xValueChange;//apply saved values TO SLIDERS
            ySlider.value = yValueChange;
            zSlider.value = zValueChange;
            sizeSlider.value = sizeValueChange;
            speedSlider.value = speedValueChange;

            spawnPoint_X_change(xValueChange);//APPLY SAVED VALUES TO AR OBJECT
            spawnPoint_Y_change(yValueChange);
            spawnPoint_Z_change(zValueChange);
            spawnPoint_Rot_change(rotValueChange);
            sizeChange(sizeValueChange);
            speedChange(speedValueChange);

            gameInstance = FindObjectOfType<Game>();
            battleshipsGameScript = FindObjectOfType<BattleshipsGame>();

            gameInstance.gameObject.SetActive(false);
            cannon.gameObject.SetActive(false);
            spawnPointLeft.SetActive(false);
            spawnPointRight.SetActive(false);

            buttonL.SetActive(false);
            buttonR.SetActive(false);
            gunScript.aim.SetActive(false);
            powerSlider.SetActive(false);

            originPos = cannon.transform.position;

            ARTrackedImageManager.trackedImagesChanged += OnTrackedImagesChanged;//START SCANNING FOR MARKER
            trackedImage = null;//marker itself?

            //=========================FOR GAME LOGIC AND CONTROL===========================
            gameStartedForFirstTime = false;
            canScan = false;
            inputActive = false;
            gameActive = false;

            timerManager.time = gameDuration;//SET LENGTH OF THE GAME

            HUD.SetActive(false);

            //settingButton.gameObject.SetActive(false);

            //scanText.gameObject.SetActive(true);
            UI_tutorial.SetActive(true);

        }

        void Update()
        {
            if (!timerManager.timerActive && !gameStartedForFirstTime)//BEFORE FIRST GAME - AT STARTUP
            {
                canScan = true;//CAN SCAN
                print("WAITING FOR THE SCAN...");

                scanImage.gameObject.SetActive(false);

                /**/
                if (cannon.activeSelf)//HAS FOUND MARKER
                {
                    print("WAITING FOR THE TAP...");
                    scanImage.gameObject.SetActive(true);

                    if (Input.touchCount > 0 || Input.GetMouseButtonDown(0))//WAIT FOR TAP TO START GAME
                        beginGame();
                }
            }

            if (!gameActive && gameStartedForFirstTime)//START GAME FOR ALL TIMES EXCEPT THE FIRST ONE
            {
                if (inputActive)
                {
                    print("WAITING FOR THE TAP TO PLAY AGAIN...");

                    if (Input.GetTouch(0).phase == TouchPhase.Began || Input.GetMouseButtonDown(0))//TAP TO PLAY AGAIN
                        beginGame();

                }
            }


            if (timerManager.timerActive == false && gameActive)//END GAME
            {
                print("TIMER IS FALSE...");
                if (gameStartedForFirstTime)
                {
                    print("ENDING GAME?...");
                    endGame();
                }
            }
        }

        public void beginGame()//START THE GAME
        {
            Debug.Log("GAME HAS STARTED!");

            scoreManager.score = 0;//score reset

            gameStartedForFirstTime = true;
            inputActive = false;
            gameActive = true;

            scanImage.gameObject.SetActive(false);

            timerManager.timerActive = true;
            canScan = false;

            HUD.SetActive(true);
            gunScript.aim.gameObject.SetActive(true);
            cannon.SetActive(true);

            buttonL.SetActive(true);
            buttonR.SetActive(true);
            powerSlider.SetActive(true);
            spawnPointLeft.SetActive(true);
            spawnPointRight.SetActive(true);
            //settingButton.gameObject.SetActive(true);

            gameInstance.gameObject.SetActive(true);
            gameInstance.enabled = true;
            gameInstance.StartGame();

        }

        public void isRecognized()//HAS RECOGNIZED MARKER
        {
            if (hasExecuted == false)
            {
                print("isRecognized");

                canScan = true;

                gameInstance.gameObject.SetActive(true);//turns on game - UI and spawn points, etc
                scanImage.gameObject.SetActive(false);

                spawnPointLeft.SetActive(true);
                spawnPointRight.SetActive(true);

                gunScript.aim.gameObject.SetActive(true);//crosshair turned on

                cannon.SetActive(true);
                gameInstance.StartGame();//start spawning boats
                print("game active? " + gameInstance.gameObject.activeSelf);

                buttonL.SetActive(true);
                buttonR.SetActive(true);
                powerSlider.SetActive(true);

                hasExecuted = true;
            }
        }

        public void endGame()//END GAME
        {
            Debug.LogError("### endgame");
            gameActive = false;
            scanImage.gameObject.SetActive(false);
            inputActive = false;
            hasExecuted = true;

            gameInstance.gameObject.SetActive(false);
            gameInstance.enabled = false;

            HUD.SetActive(false);
            gunScript.aim.gameObject.SetActive(false);

            buttonL.SetActive(false);
            buttonR.SetActive(false);
            powerSlider.SetActive(false);
            //settingButton.gameObject.SetActive(false);

            string winLoseText;
            bool won = false;
            if (scoreManager.score >= 1)
            {
                //PLAYER WON
                winLoseText = "YOU WON";
                won = true;
            }
            else
            {
                //PLAYER LOST
                winLoseText = "YOU LOSE";
            }

            GameUIController.instance.EndGame(winLoseText, won, scoreManager.score);

            timerManager.timerActive = false;
            timerManager.time = gameDuration;

            //battleshipsGameScript.destroyAllBoats();
            battleshipsGameScript.EndGame();

            gunScript.RonPointerUp(); //If the game ends while player holds button, reset the touch, so cannon doesnt keeps spinning
            gunScript.LonPointerUp();

            canScan = false;



            print("game active? " + gameInstance.gameObject.activeSelf);

            StartCoroutine(delayEndGame());//DELAY FOR THE TAP TO START THE GAME AGAIN
        }

        IEnumerator delayEndGame()
        {
            scanImage.gameObject.SetActive(false);

            yield return new WaitForSeconds(3f);

            inputActive = true;
            //gameActive = false;
            scanImage.gameObject.SetActive(true);

            canScan = true;
        }

        IEnumerator disableInputFor(float time)//animācijām
        {
            yield return new WaitForSeconds(time);
        }

        public void OnTrackedImagesChanged(ARTrackedImagesChangedEventArgs eventArgs)//scanning marker
        {
            for (int i = 0; i < eventArgs.updated.Count; i++)
            {
                trackedImage = eventArgs.updated[i];

                if (trackedImage.trackingState == TrackingState.Tracking)//IF MARKER IS RECOGNIZED AND UPDATED
                {
                    if (canScan)//IF CAN SCAN
                    {
                        Vector3 modRotation = new Vector3(0f, trackedImage.transform.rotation.eulerAngles.y + 90f, 0f);//MAKE CANNON PLUS 90 DEGREES OF MARKER

                        cannon.transform.SetParent(arSessionOrigin.transform);

                        cannon.transform.eulerAngles = modRotation;
                        cannon.transform.position = trackedImage.transform.position;
                        cannon.SetActive(true);

                        UI_tutorial.SetActive(false);
                    }
                }
            }
        }

        //VOIDS
        public void containerRotation()
        {
            if (containerRotationChanged == false)
            {
                //====================NEW-METHOD======================
                float camRotY = Arcamera.transform.rotation.eulerAngles.y;

                containerRotationChanged = true;
            }

        }



        /*===========================================FOR-BUTTONS===========================================================*/


        public void resetTracking()//NOT USED
        {
            ARTrackedImageManager.enabled = false;//sets tracking on
            scanText.gameObject.SetActive(true);
            gameInstance.EndGame();//stop spawning boats
            gunScript.aim.gameObject.SetActive(false);
        }

        public void spawnPoint_X_change(float value)
        {

            xValueChange = value;
            PlayerPrefs.SetFloat("x", xValueChange);

            spawnPointLeft.transform.localPosition = new Vector3(-xValueChange, yValueChange, zValueChange);
            spawnPointRight.transform.localPosition = new Vector3(xValueChange, yValueChange, zValueChange);

            xValue.text = "Spawnpoint distance: " + xValueChange;
        }

        public void spawnPoint_Rot_change(float value)
        {

            rotValueChange = value;
            PlayerPrefs.SetFloat("rot", rotValueChange);

            Quaternion newRot = Quaternion.Euler(0, rotValueChange, 0);

            Vector3 eulerRot = newRot * arSessionOrigin.transform.eulerAngles;
            spawnPointContainer.transform.localRotation = newRot;// arSessionOrigin.transform.eulerAngles * new Vector3(0, rotValueChange, 0);

            rotValue.text = "Rotation: " + rotValueChange;
        }

        public void spawnPoint_Y_change(float value)
        {

            yValueChange = value;
            PlayerPrefs.SetFloat("y", yValueChange);
            spawnPointLeft.transform.localPosition = new Vector3(-xValueChange, yValueChange, zValueChange);
            spawnPointRight.transform.localPosition = new Vector3(xValueChange, yValueChange, zValueChange);//set local pos to 0, so they have same location

            //spawnPointContainer.transform.position = new Vector3(spawnPointContainer.transform.position.x, yValueChange, spawnPointContainer.transform.position.z);

            yValue.text = "Height: " + yValueChange;
        }

        public void spawnPoint_Z_change(float value)
        {
            zValueChange = value;
            PlayerPrefs.SetFloat("z", zValueChange);

            spawnPointLeft.transform.localPosition = new Vector3(-xValueChange, yValueChange, zValueChange);
            spawnPointRight.transform.localPosition = new Vector3(xValueChange, yValueChange, zValueChange);//set local pos to 0, so they have same location

            zValue.text = "Distance: " + zValueChange;
        }

        public void sizeChange(float value)
        {
            sizeValueChange = value;
            PlayerPrefs.SetFloat("size", sizeValueChange);

            sizeValue.text = "Size: " + sizeValueChange + " / " + battleshipsGameScript.dispersion;
        }

        public void speedChange(float value)
        {
            speedValueChange = value;
            PlayerPrefs.SetFloat("speed", speedValueChange);

            battleshipsGameScript.maxBoatSpeed = speedValueChange + 1f;
            battleshipsGameScript.minBoatSpeed = speedValueChange;

            speedValue.text = "Speed: " + speedValueChange;
        }

        void OnApplicationQuit()
        {
            PlayerPrefs.Save();
        }

        public void SaveData()
        {
            PlayerPrefs.Save();
        }

    }


}