﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Overly.FightGamesScripts
{
public class boatPlaneCollider : MonoBehaviour
{

    public GameObject particleSyst;


    //private void OnCollisionEnter(Collision collision)
    //{
    //    Debug.LogError("triggered by " + collision.gameObject.tag);
    //    if (collision.gameObject.tag == "Ball")
    //    {
    //        //particleSyst.transform.position = col.transform.position;
    //        //particleSyst.SetActive(true);
    //        GameObject go = Instantiate(particleSyst, collision.transform.position, Quaternion.Euler(transform.TransformDirection(Vector3.left) * 90), transform.parent);

    //        // StartCoroutine(destroySplash(go));
    //        Destroy(go, 2);
    //        Destroy(collision.gameObject);
    //    }
    //}
    private void OnTriggerEnter(Collider col)
    {
        if (col.gameObject.tag == "Ball")
        {
            //particleSyst.transform.position = col.transform.position;
            //particleSyst.SetActive(true);
            GameObject go = Instantiate(particleSyst, col.transform.position, transform.rotation, transform.parent);

            //StartCoroutine(destroySplash(go));
            Destroy(go, 2);
            Destroy(col.gameObject);
        }
    }

    public void OnDisable()
    {
        foreach (Transform child in transform)
        {
            Destroy(child.gameObject);
        }
    }


}
}