﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using TMPro;
using System;
using Wikitude;

namespace Overly.FightGamesScripts
{
    public class AR_ManagerWikitude : MonoBehaviour
    {
        public ImageTrackable imageTrackable;
        public Camera Arcamera;
        public GameObject UI_Tutorial;

        private Game gameInstance;
        public gun_backup gunScript;

        public GameObject cannon;

        //ARTrackedImage trackedImage;
        public BattleshipsGame battleshipsGameScript;

        public GameObject spawnPointLeft;
        public GameObject spawnPointRight;
        public GameObject waterLevel;
        public GameObject spawnPointContainer;
        public GameObject spawnPlane;

        public GameObject ButtonL;
        public GameObject ButtonR;
        public GameObject powerSlider;

        [HideInInspector] public float rotValueChange;
        [HideInInspector] public float xValueChange;
        [HideInInspector] public float yValueChange;
        [HideInInspector] public float zValueChange;
        [HideInInspector] public float sizeValueChange;
        [HideInInspector] public float speedValueChange;

        public TextMeshProUGUI rotValue;
        public TextMeshProUGUI xValue;
        public TextMeshProUGUI yValue;
        public TextMeshProUGUI zValue;
        public TextMeshProUGUI sizeValue;
        public TextMeshProUGUI speedValue;

        public Slider rotSlider;
        public Slider xSlider;
        public Slider ySlider;
        public Slider zSlider;
        public Slider sizeSlider;
        public Slider speedSlider;

        private float xValueSaved;
        private float yValueSaved;
        private float zValueSaved;
        private float sizeValueSaved;
        private float speedValueSaved;

        private Vector3 originPos;

        //===========================FOR GAME LOGIC AND CONTROL=====================
        public timerBattleShipButtons timerManager;
        private bool gameStartedForFirstTime;
        private bool gameActive;
        private bool canScan;
        private bool inputActive;

        public float gameDuration;

        public Image scoreImage;
        public Image timerImage;
        public Image scanImage;
        public Image settingButton;

        private TextMeshProUGUI scoreText;
        private TextMeshProUGUI timerText;
        private TextMeshProUGUI scanText;


        //ignore these, public only because other scripts need access these
        bool hasExecuted = false;
        [HideInInspector] public bool containerRotationChanged = false;
        public ImageTarget overlymarker_temp;
        ImageTarget OVERLY_Marker;
        bool gameIsRunning;

        private void Awake()
        {
            cannon.SetActive(false);
            imageTrackable.OnImageRecognized.AddListener(OnImageRecognized);
            imageTrackable.OnImageLost.AddListener(OnImageLost);
            scoreText = scoreImage.GetComponentInChildren<TextMeshProUGUI>();
            timerText = timerImage.GetComponentInChildren<TextMeshProUGUI>();
            scanText = scanImage.GetComponentInChildren<TextMeshProUGUI>();

            scanText.text = "TAP TO PLAY";
        }

        void Start()
        {
            ResetGame();
        }

        public void RestartGame()
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }
        public void ResetGame()
        {
            foreach (Boat boat in FindObjectsOfType<Boat>())
            {
                boat.Destroy();
            }
            if (PlayerPrefs.HasKey("rot"))//GET SAVED VALUES
                rotValueChange = PlayerPrefs.GetFloat("rot");//get saved values
            if (PlayerPrefs.HasKey("x"))
                xValueChange = PlayerPrefs.GetFloat("x");//get saved values

            if (PlayerPrefs.HasKey("y"))
                yValueChange = PlayerPrefs.GetFloat("y");

            if (PlayerPrefs.HasKey("z"))
                zValueChange = PlayerPrefs.GetFloat("z");

            if (PlayerPrefs.HasKey("size"))
                sizeValueChange = PlayerPrefs.GetFloat("size");

            if (PlayerPrefs.HasKey("speed"))
                speedValueChange = PlayerPrefs.GetFloat("speed");


            print("y from save: " + yValueChange);
            xValue.text = "Spawnpoint distance: " + xValueChange;
            yValue.text = "Height: " + yValueChange;
            zValue.text = "Distance: " + zValueChange;
            rotValue.text = "Rotation: " + rotValueChange;
            sizeValue.text = "Size: " + sizeValueChange;
            speedValue.text = "Speed: " + speedValueChange;

            rotSlider.value = rotValueChange;
            xSlider.value = xValueChange;//apply saved values
            ySlider.value = yValueChange;
            zSlider.value = zValueChange;
            sizeSlider.value = sizeValueChange;
            speedSlider.value = speedValueChange;

            spawnPoint_X_change(xValueChange);
            SpawnPoint_Y_change(yValueChange);
            spawnPoint_Z_change(zValueChange);
            spawnPoint_Rot_change(rotValueChange);
            sizeChange(sizeValueChange);
            speedChange(speedValueChange);

            gameInstance = FindObjectOfType<Game>();
            battleshipsGameScript = FindObjectOfType<BattleshipsGame>();

            gameInstance.gameObject.SetActive(false);
            cannon.SetActive(false);
            scanImage.gameObject.SetActive(true);
            //cannon.gameObject.SetActive(false);
            //waterLevel.SetActive(false);
            spawnPointLeft.SetActive(false);
            spawnPointRight.SetActive(false);
            ButtonL.SetActive(false);
            ButtonR.SetActive(false);
            powerSlider.SetActive(false);
            Debug.Log(gunScript.name);
            Debug.Log("AAAAA1");
            gunScript.aim.gameObject.SetActive(false);

            //originPos = cannon.transform.position;

            //ARTrackedImageManager.trackedImagesChanged += OnTrackedImagesChanged;
            //trackedImage = null;//marker itself?

            //=========================FOR GAME LOGIC AND CONTROL===========================
            gameStartedForFirstTime = false;
            canScan = true;
            print("START: " + canScan);
            inputActive = false;
            gameActive = false;
            gameIsRunning = false;

            timerManager.time = gameDuration;

            scoreImage.gameObject.SetActive(false);
            timerImage.gameObject.SetActive(false);
            settingButton.gameObject.SetActive(false);
            UI_Tutorial.SetActive(true);
        }

        void Update()
        {
            if (!timerManager.timerActive && !gameStartedForFirstTime)//BEFORE FIRST GAME - AT STARTUP
            {
                canScan = true;
                print("WAITING FOR THE SCAN...");

                scanImage.gameObject.SetActive(false);

                if (cannon.activeSelf)
                {
                    print("WAITING FOR THE TAP...");
                    scanImage.gameObject.SetActive(true);

                    if (Input.touchCount > 0 || Input.GetMouseButtonDown(0))
                        beginGame();
                }
            }

            if (!gameActive && gameStartedForFirstTime)//START GAME FOR ALL TIMES EXCEPT THE FIRST ONE
            {
                //scanImage.gameObject.SetActive(true);

                if (inputActive && !gameActive)
                {
                    print("WAITING FOR THE TAP TO PLAY AGAIN...");

                    if (Input.GetTouch(0).phase == TouchPhase.Began || Input.GetMouseButtonDown(0))
                        beginGame();

                }
            }


            if (timerManager.timerActive == false && gameActive)//END GAME
            {
                print("TIMER IS FALSE...");
                if (gameStartedForFirstTime)
                {
                    print("ENDING GAME?...");
                    endGame();
                }
            }
        }



        public void beginGame()
        {
            UI_Tutorial.SetActive(false);
            gameIsRunning = true;
            scoreManager.score = 0;//score reset

            gameStartedForFirstTime = true;
            inputActive = false;
            gameActive = true;

            scanImage.gameObject.SetActive(false);

            timerManager.timerActive = true;
            canScan = true;

            scoreImage.gameObject.SetActive(true);
            timerImage.gameObject.SetActive(true);
            gunScript.aim.gameObject.SetActive(true);
            cannon.SetActive(true);


            ButtonL.SetActive(true);
            ButtonR.SetActive(true);
            powerSlider.SetActive(true);

            //waterLevel.SetActive(true);
            spawnPointLeft.SetActive(true);
            spawnPointRight.SetActive(true);
            settingButton.gameObject.SetActive(true);

            gameInstance.gameObject.SetActive(true);
            gameInstance.enabled = true;
            gameInstance.StartGame();
        }

        public void isRecognized()
        {
            if (hasExecuted == false)
            {
                //print("isRecognized");

                canScan = true;

                gameInstance.gameObject.SetActive(true);//turns on game - UI and spawn points, etc
                gameInstance.enabled = true;

                scanImage.gameObject.SetActive(false);

                //waterLevel.SetActive(true);
                spawnPointLeft.SetActive(true);
                spawnPointRight.SetActive(true);

                gunScript.aim.gameObject.SetActive(true);//crosshair turned on

                cannon.SetActive(true);
                gameInstance.StartGame();//start spawning boats
                //print("game active? " + gameInstance.gameObject.activeSelf);

                ButtonL.SetActive(true);
                ButtonR.SetActive(true);
                powerSlider.SetActive(true);

                hasExecuted = true;

            }
        }

        public void endGame()
        {
            gameIsRunning = false;
            inputActive = false;
            hasExecuted = true;

            gameInstance.gameObject.SetActive(false);
            gameInstance.enabled = false;

            scoreImage.gameObject.SetActive(false);
            timerImage.gameObject.SetActive(false);
            Debug.Log("AAAAA2");
            gunScript.aim.gameObject.SetActive(false);

            ButtonL.SetActive(false);
            ButtonR.SetActive(false);
            powerSlider.SetActive(false);
            settingButton.gameObject.SetActive(false);

            string winLoseText;
            bool won = false;
            if (scoreManager.score >= 0)
            {
                //PLAYER WON
                winLoseText = "YOU WON";
                won = true;
            }
            else
            {
                //PLAYER LOST
                winLoseText = "YOU LOSE";
            }

            GameUIController.instance.EndGame(winLoseText, won, scoreManager.score);

            timerManager.timerActive = false;
            timerManager.time = gameDuration;

            //battleshipsGameScript.destroyAllBoats();
            battleshipsGameScript.EndGame();

            gunScript.RonPointerUp(); //If the game ends while player holds button, reset the touch, so cannon doesnt keeps spinning
            gunScript.LonPointerUp();


            canScan = false;
            gameActive = false;

            scanImage.gameObject.SetActive(false);


            StartCoroutine(delayEndGame());
        }

        IEnumerator delayEndGame()
        {
            scanImage.gameObject.SetActive(false);

            yield return new WaitForSeconds(3f);

            inputActive = true;
            //gameActive = false;
            scanImage.gameObject.SetActive(true);
            canScan = true;

            //StartCoroutine(disableInputFor(2f));

        }

        public void OnImageRecognized(ImageTarget recognizedTarget)
        {
            if (canScan)
            {
                if (gameIsRunning)
                {
                    ButtonL.SetActive(true);
                    ButtonR.SetActive(true);
                    powerSlider.SetActive(true);
                    gunScript.aim.gameObject.SetActive(true);

                }
                //isRecognized();

                //gameInstance.gameObject.SetActive(true);

                Vector3 modRotation = new Vector3(0f, recognizedTarget.Drawable.transform.rotation.eulerAngles.y + 90f, 0f);

                overlymarker_temp = recognizedTarget;
                cannon.transform.SetParent(recognizedTarget.Drawable.transform);
                cannon.transform.position = recognizedTarget.Drawable.transform.position;
                cannon.transform.localEulerAngles = new Vector3(0f, 90f, 0f);// modRotation;

                cannon.transform.localPosition = Vector3.zero;
                cannon.SetActive(true);
                UI_Tutorial.SetActive(false);
            }
        }

        public void OnImageLost(ImageTarget recognizedTarget)
        { //{
            ButtonL.SetActive(false);
            ButtonR.SetActive(false);
            powerSlider.SetActive(false);
            gunScript.aim.gameObject.SetActive(false);
            cannon.SetActive(false);
            //endGame();
            gameStartedForFirstTime = false;
            timerManager.timerActive = false;
            hasExecuted = false;

            cannon.transform.SetParent(null);


            gameInstance.gameObject.SetActive(true);
            gameInstance.enabled = true;
            gameInstance.StartGame();

            OVERLY_Marker = recognizedTarget;
            print("LOST TARGET: " + OVERLY_Marker.Name);
            UI_Tutorial.SetActive(true);
        }

        IEnumerator disableInputFor(float time)
        {
            yield return new WaitForSeconds(time);
        }

        /* //-----------FOR-FOUNDATION-----------\\
        public void OnTrackedImagesChanged(ARTrackedImagesChangedEventArgs eventArgs)//scanning marker
        {
            

            for (int i = 0; i < eventArgs.updated.Count; i++)
            {
                trackedImage = eventArgs.updated[i];

                if (trackedImage.trackingState == TrackingState.Tracking)
                {
                    if (canScan)
                    {

                        Vector3 modRotation = new Vector3(0f, trackedImage.transform.rotation.eulerAngles.y + 90f, 0f);
                        //Vector3 rot = new Vector3(0f, trackedImage.transform.rotation.y + 90f, 0f);
                        //Quaternion modQuat = Quaternion.Euler(rot);

                        Quaternion tmpQuat = new Quaternion(0f, trackedImage.transform.rotation.y - 0.707f, 0f, trackedImage.transform.rotation.y + 0.707f);

                        //arSessionOrigin.MakeContentAppearAt(cannon.transform, trackedImage.transform.position, trackedImage.transform.rotation);
                        cannon.transform.SetParent(arSessionOrigin.transform);

                        cannon.transform.eulerAngles = modRotation;
                        cannon.transform.position = trackedImage.transform.position;

                        spawnPointContainer.transform.eulerAngles = modRotation;
                        spawnPointContainer.transform.position = trackedImage.transform.position;

                        isRecognized();

                        //ARTrackedImageManager.trackedImagesChanged -= OnTrackedImagesChanged;
                    }

                    
                }
            }
        }
        */




        //VOIDS
        public void containerRotation()
        {
            if (containerRotationChanged == false)
            {
                //====================NEW-METHOD======================
                float camRotY = Arcamera.transform.rotation.eulerAngles.y;

                //cannon.transform.rotation = Quaternion.Euler(new Vector3(0f, camRotY - 90f, 0f));
                //spawnPointContainer.transform.rotation = Quaternion.Euler(new Vector3(0f, camRotY, 0f));

                //cannon.transform.rotation = Quaternion.Euler(0f, trackedImage.transform.rotation.y, 0f);
                //spawnPointContainer.transform.rotation = Quaternion.Euler(0f, trackedImage.transform.rotation.y, 0f);

                containerRotationChanged = true;
            }

        }

        /*===========================================FOR-BUTTONS===========================================================*/


        public void resetTracking()
        {
            //ARTrackedImageManager.enabled = false;//sets tracking on
            //cannon.SetActive(false);
            //gunScript.gyroContainer.SetActive(false);
            //cannon.transform.position = new Vector3(0f, 0f, 0f);//put cannon in 0,0,0 again
            scanImage.gameObject.SetActive(true);
            gameInstance.EndGame();//stop spawning boats
            Debug.Log("AAAAA3");
            gunScript.aim.gameObject.SetActive(false);
        }

        public void spawnPoint_X_change(float value)
        {

            xValueChange = value;
            PlayerPrefs.SetFloat("x", xValueChange);
            spawnPointLeft.transform.localPosition = new Vector3(-xValueChange, yValueChange, zValueChange);
            spawnPointRight.transform.localPosition = new Vector3(xValueChange, yValueChange, zValueChange);

            xValue.text = "Spawnpoint distance: " + xValueChange;
        }

        public void SpawnPoint_Y_change(float value)
        {

            yValueChange = value;
            PlayerPrefs.SetFloat("y", yValueChange);
            // waterLevel.transform.localPosition = new Vector3(0, yValueChange, 0);
            spawnPointLeft.transform.localPosition = new Vector3(-xValueChange, yValueChange, zValueChange);
            spawnPointRight.transform.localPosition = new Vector3(xValueChange, yValueChange, zValueChange);//set local pos to 0, so they have same location

            //spawnPointContainer.transform.position = new Vector3(spawnPointContainer.transform.position.x, yValueChange, spawnPointContainer.transform.position.z);

            yValue.text = "Height: " + yValueChange;
        }

        public void spawnPoint_Rot_change(float value)
        {

            rotValueChange = value;
            PlayerPrefs.SetFloat("rot", rotValueChange);

            spawnPointContainer.transform.eulerAngles = new Vector3(0, rotValueChange, 0);

            rotValue.text = "Rotation: " + rotValueChange;
        }

        public void spawnPoint_Z_change(float value)
        {
            zValueChange = value;
            PlayerPrefs.SetFloat("z", zValueChange);

            spawnPointLeft.transform.localPosition = new Vector3(-xValueChange, yValueChange, zValueChange);
            spawnPointRight.transform.localPosition = new Vector3(xValueChange, yValueChange, zValueChange);//set local pos to 0, so they have same location

            zValue.text = "Distance: " + zValueChange;
        }

        public void sizeChange(float value)
        {
            sizeValueChange = value;
            PlayerPrefs.SetFloat("size", sizeValueChange);

            sizeValue.text = "Size: " + sizeValueChange + " / " + battleshipsGameScript.dispersion;
        }

        public void speedChange(float value)
        {
            speedValueChange = value;
            PlayerPrefs.SetFloat("speed", speedValueChange);

            battleshipsGameScript.maxBoatSpeed = speedValueChange + 1f;
            battleshipsGameScript.minBoatSpeed = speedValueChange;

            speedValue.text = "Speed: " + speedValueChange;
        }

        void OnApplicationQuit()
        {
            PlayerPrefs.Save();
        }

    }


}