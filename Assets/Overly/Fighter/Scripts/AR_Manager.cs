﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using TMPro;
using System;

namespace Overly.FightGamesScripts
{
    public class AR_Manager : MonoBehaviour
    {
        public bool wikitude = false;
        public Camera Arcamera;
        public ARTrackedImageManager ARTrackedImageManager;
        public ARReferencePointManager aRReferencePointManager;
        public ARSessionOrigin arSessionOrigin;
        private Game gameInstance;
        private Gun gunScript;
        public lookAt lookAtScript;
        public GameObject cannon;
        public TextMeshProUGUI scanText;
        ARTrackedImage trackedImage;
        private Boat activeBoats;
        public BattleshipsGame battleshipsGameScript;
        public TextMeshProUGUI yValueTxt;
        public TextMeshProUGUI zValueTxt;

        public GameObject spawnPointLeft;
        public GameObject spawnPointRight;
        public GameObject spawnPointContainer;

        public float xValueChange;
        public float yValueChange;
        public float zValueChange;
        public float sizeValueChange;
        public float speedValueChange;

        public TextMeshProUGUI xValue;
        public TextMeshProUGUI yValue;
        public TextMeshProUGUI zValue;
        public TextMeshProUGUI sizeValue;
        public TextMeshProUGUI speedValue;

        public Slider xSlider;
        public Slider ySlider;
        public Slider zSlider;
        public Slider sizeSlider;
        public Slider speedSlider;

        

        //ignore these, public only because other scripts need access these
        bool hasExecuted = false;
        [HideInInspector] public bool containerRotationChanged = false;
        Vector3 tmpContainer;
        [HideInInspector]public Vector3 tmpClamp;
        [HideInInspector]public Vector3 spawnAngle;
        private void Awake()
        {
            cannon.SetActive(false);

            if (wikitude)
            {
            }
            else
            {
                ARTrackedImageManager.trackedImagesChanged += OnTrackedImagesChanged;
            }
        }

        void Start()
        {
            gameInstance = FindObjectOfType<Game>();
            gunScript = FindObjectOfType<Gun>();
            battleshipsGameScript = FindObjectOfType<BattleshipsGame>();

            gameInstance.gameObject.SetActive(false);
            scanText.gameObject.SetActive(true);
            cannon.gameObject.SetActive(false);
            spawnPointLeft.SetActive(false);
            spawnPointRight.SetActive(false);

            xSlider.value = xValueChange;
            ySlider.value = yValueChange;
            zSlider.value = zValueChange;
            sizeSlider.value = sizeValueChange;
            speedSlider.value = speedValueChange;

            spawnPoint_X_change(xValueChange);
            spawnPoint_Y_change(yValueChange);
            spawnPoint_Z_change(zValueChange);
            sizeChange(sizeValueChange);
            speedChange(speedValueChange);

            //gunScript.gyroContainer.SetActive(false);

            xValue.text = "Spawnpoint distance: " + xValueChange;
            yValue.text = "Height: " + yValueChange;
            zValue.text = "Distance: " + zValueChange;
            sizeValue.text = "Size: " + sizeValueChange;
            speedValue.text = "Speed: " + speedValueChange;

        }

        void Update()
        {
            //0 - right one, 1 - left one
            //yValueChange = pos.y;
            //if (cannon.transform.position != new Vector3(0f, 0f, 0f))
            
            if (cannon.gameObject.activeInHierarchy == true)//if the default location has changed, we know the marker has been scanned
            {
                isRecognized();
                Invoke("containerRotation", 2f);
            }

            //getRotation();

            //Debug.Log("pos: " + spawnPointController.transform.position);
        }

        
        public void OnTrackedImagesChanged(ARTrackedImagesChangedEventArgs eventArgs)//scanning marker
        {
            trackedImage = null;//marker itself?

            // Check the new tracked images
            for (int i = 0; i < eventArgs.added.Count; i++)
            {
                trackedImage = eventArgs.added[i];
                //cannon.SetActive(true);
            }


            for (int i = 0; i < eventArgs.updated.Count; i++)
            {
                trackedImage = eventArgs.updated[i];

                if (trackedImage.trackingState == TrackingState.Tracking)
                {
                    print("TRACKING!!!!!!!");
                    arSessionOrigin.MakeContentAppearAt(cannon.transform, trackedImage.transform.position);

                    cannon.SetActive(true);
                }
                else
                {
                    //cannon.SetActive(false);
                }
            }

            for (int i = 0; i < eventArgs.removed.Count; i++)
            {
                if (trackedImage != null)
                {
                    if (trackedImage.trackingState == TrackingState.None)
                    {
                        //cannon.SetActive(false);
                    }
                }
            }
        }

        //VOIDS

        public void containerRotation()
        {
            if (containerRotationChanged == false)
            {
                spawnAngle = lookAtScript.rotationOnlyY.eulerAngles;

                tmpContainer = new Vector3(0f, spawnAngle.y, 0f);

                spawnPointContainer.transform.rotation = Quaternion.Euler(tmpContainer);
                print("CONTAINER ROTATION CHANGED TO " + spawnPointContainer.transform.rotation.eulerAngles.y);

                containerRotationChanged = true;
            }
            
        }

        public void isRecognized()
        {

            if(hasExecuted == false)
            {
                print("isRecognized");
                gameInstance.gameObject.SetActive(true);//turns on game - UI and spawn points, etc
                scanText.gameObject.SetActive(false);

                spawnPointLeft.SetActive(true);
                spawnPointRight.SetActive(true);

                lookAtScript.aim.gameObject.SetActive(true);//crosshair turned on

                gameInstance.StartGame();//start spawning boats

                hasExecuted = true;
            }
            
        }

        /*===========================================FOR-BUTTONS===========================================================*/


        public void resetTracking()
        {
            ARTrackedImageManager.enabled = false;//sets tracking on
            cannon.SetActive(false);
            //gunScript.gyroContainer.SetActive(false);
            cannon.transform.position = new Vector3(0f, 0f, 0f);//put cannon in 0,0,0 again
            scanText.gameObject.SetActive(true);
            gameInstance.EndGame();//stop spawning boats
            lookAtScript.aim.gameObject.SetActive(false);
        }

        public void spawnPoint_X_change(float value)
        {
            xValueChange = value;
            spawnPointLeft.transform.localPosition = new Vector3(-xValueChange, yValueChange, zValueChange);
            spawnPointRight.transform.localPosition = new Vector3(xValueChange, yValueChange, zValueChange);
            xValue.text = "Spawnpoint distance: " + xValueChange;
        }

        public void spawnPoint_Y_change(float value)
        {
            yValueChange = value;
            spawnPointLeft.transform.localPosition = new Vector3(-xValueChange, yValueChange, zValueChange);
            spawnPointRight.transform.localPosition = new Vector3(xValueChange, yValueChange, zValueChange);//set local pos to 0, so they have same location
            yValue.text = "Height: " + yValueChange;
        }

        public void spawnPoint_Z_change(float value)
        {
            zValueChange = value;
            spawnPointLeft.transform.localPosition = new Vector3(-xValueChange, yValueChange, zValueChange);
            spawnPointRight.transform.localPosition = new Vector3(xValueChange, yValueChange, zValueChange);//set local pos to 0, so they have same location
            zValue.text = "Distance: " + zValueChange;
        }

        public void sizeChange(float value)
        {
            sizeValueChange = value;

            sizeValue.text = "Size: " + sizeValueChange + " / " + battleshipsGameScript.dispersion;
        }

        public void speedChange(float value)
        {
            speedValueChange = value;

            battleshipsGameScript.maxBoatSpeed = speedValueChange + 3f;
            battleshipsGameScript.minBoatSpeed = speedValueChange;

            speedValue.text = "Speed: " + speedValueChange;
        }

    }
}