﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;

namespace Overly.FightGamesScripts
{
    public class ScanFloor : MonoBehaviour
    {
        enum STATES { SCAN, SCANNED };
        STATES state = STATES.SCAN;

        [Header("UI")]
        public Image imgPercentageDone;
        [Header("Controls")]
        public bool scan = true;
        [Header("Values")]
        public ARPlane groundPlane;


        private ARPlaneManager planeManager;
        private ARRaycastManager raycastManager;

        public GameObject hideThisOnDone;
        // private touchManager touchManager;

        private void Awake()
        {
            planeManager = FindObjectOfType<ARPlaneManager>();
            raycastManager = FindObjectOfType<ARRaycastManager>();
            //  touchManager = gameObject.GetComponent<touchManager>();
            //  touchManager.enabled = false;
        }
        // Start is called before the first frame update
        void Start()
        {
            InitScanning();
        }

        private void CheckPlanes(ARPlanesChangedEventArgs planes)
        {
            foreach (ARPlane plane in planes.added)
            {
            }
            float fillAmount = 0;
            float fillAmountX = 0;
            float fillAmountY = 0;
            foreach (ARPlane plane in planes.updated)
            {
                plane.gameObject.SetActive(true);
                if (plane.size.x > 1.7f && plane.size.y > 1.7f && plane.isActiveAndEnabled)
                {
                    groundPlane = plane;
                    return;
                }
                else
                {
                    if (plane.isActiveAndEnabled)
                    {
                        float tempfillAmount = (plane.size.x / 1.7f) * (plane.size.y / 1.7f) * 0.9f;
                        float tempfillAmountX = (plane.size.x / 1.7f);
                        float tempfillAmountY = (plane.size.y / 1.7f);
                        if (tempfillAmount > fillAmount)
                        {
                            fillAmount = tempfillAmount;
                        }
                    }
                }
            }
            if (groundPlane != null)
            {
                fillAmount = 1;
            }
            else
            {
                fillAmount *= 0.9f;
            }
            imgPercentageDone.fillAmount = fillAmount;
        }

        // Update is called once per frame
        void Update()
        {
            if (scan)
            {
                if (groundPlane != null)
                {
                    DoneScanning();
                }
            }
        }


        public void InitScanning()
        {
            hideThisOnDone.SetActive(true);
            imgPercentageDone.gameObject.SetActive(true);
            groundPlane = null;
            HideUnusedPlanes();
            planeManager.planesChanged += CheckPlanes;
            planeManager.enabled = true;
            scan = true;
        }

        void DoneScanning()
        {
            hideThisOnDone.SetActive(false);
            imgPercentageDone.color = Color.green;
            imgPercentageDone.fillAmount = 1;
            planeManager.planesChanged -= CheckPlanes;
            planeManager.enabled = false;
            scan = false;
            HideUnusedPlanes();
            FindObjectOfType<XrManager>().DropGun();
        }

        public void ShowScanProgress(bool show)
        {
            imgPercentageDone.color = Color.white;
            imgPercentageDone.fillAmount = 0;
            imgPercentageDone.gameObject.SetActive(show);

        }

        void HideUnusedPlanes()
        {
            foreach (ARPlane plane in planeManager.trackables)
            {
                if (plane != groundPlane)
                {
                    plane.gameObject.SetActive(false);
                }
            }
        }
    }
}