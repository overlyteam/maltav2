﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Overly.FightGamesScripts
{
    public class EditLine : MonoBehaviour
    {
        LineRenderer renderer;
        public GameObject SpawnerL;
        public GameObject SpawnerR;

        private void Awake()
        {
            renderer = GetComponent<LineRenderer>();
        }
        private void Update()
        {
            if (renderer.enabled == true)
            {
                Vector3[] pos = new Vector3[2];
                pos[0] = SpawnerL.transform.position;
                pos[1] = SpawnerR.transform.position;
                renderer.positionCount = 2;
                renderer.SetPositions(pos);
            }
        }

        public void ShowLine(bool show)
        {
            renderer.enabled = show;
        }
    }
}