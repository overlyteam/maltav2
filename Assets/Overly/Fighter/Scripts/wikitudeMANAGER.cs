﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Wikitude;

namespace Overly.FightGamesScripts
{
public class wikitudeMANAGER : MonoBehaviour
{
    public Transform arCamera;

    public GameObject cannon;

    private bool hasScanned;

    void Start()
    {
        hasScanned = false;
    }

    // Update is called once per frame
    void Update()
    {
        print("updating");
        
    }

    public void imageRecognized(ImageTarget recognizedTarget)
    {
        print("scanning");/*
        cannon.position = recognizedTarget.Drawable.transform.position;
        cannon.rotation = recognizedTarget.Drawable.transform.rotation;

        spawnObjects.position = recognizedTarget.Drawable.transform.position;
        spawnObjects.rotation = recognizedTarget.Drawable.transform.rotation;*/

        cannon.gameObject.SetActive(true);

        hasScanned = true;
    }
}
}