﻿using UnityEngine;

namespace Overly.FightGamesScripts
{

    public class Aircraft : MonoBehaviour
    {
        public float Speed;
        public float LivingTimeAfterExplode = 3f;
        [Tooltip("When following distance (squred meters) will be reached, the airplane will be destroyed.")]
        public float DestroyDistance = 1000000;
        public AudioClip crashSound;
        public AudioClip explsSound;
        public GameObject smokePSys;
        

        private Collider _collider;
        private Vector3 _direction;
        private DeathFromAbove _game;
        private Vector3 _passedDistance;
        private AudioSource _audioSource;



        /****************** INNER LOGIC ******************/

        private void Awake()
        {
            _direction = transform.forward;
            _collider = GetComponent<Collider>();
            _game = FindObjectOfType<DeathFromAbove>();            
            _audioSource = GetComponent<AudioSource>();
        }

        private void Start()
        {
            TargetMarkerManager.instance?.AddMarker(gameObject);
        }
        private void OnDestroy()
        {
            TargetMarkerManager.instance?.RemoveMarker(gameObject);
        }
        private void Update()
        {
            if (transform.position.y < 0)
            {
                Debug.Log("Scheise!!");
            }
            Vector3 pos = _direction * Speed * Time.deltaTime;
            transform.Translate(pos, Space.World);
            _passedDistance += pos;
            if (_passedDistance.sqrMagnitude > DestroyDistance)
                DestroyAircraft(0);
        }

        public void Explode()
        {
            TargetMarkerManager.instance?.RemoveMarker(gameObject);
            scoreManager.score++;
            _collider.enabled = false; //turn off physics
            _audioSource.enabled = false; // turn off sounds of engines
            smokePSys.SetActive(true); //turn on smoke particle system

            //airplane should fall dawn - angle changed to 30 degree
            Vector3 rot = transform.localEulerAngles;
            rot.x = 30f;
            transform.localEulerAngles = rot;

            _direction = transform.forward; //get new direction of plane where it is facing now

            //play sounds
            PlayCrashSound();
            PlayExplosionSound();            

            //destroy
            DestroyAircraft(transform.position.y / (Speed * Mathf.Cos(30 * Mathf.Deg2Rad)) + 1f);
        }

        private void DestroyAircraft(float time = 0)
        {
            TargetMarkerManager.instance?.RemoveMarker(gameObject);
            _game.DestroyPlane(this);
            Destroy(gameObject, time);
        }

        private void PlayExplosionSound()
        {
            AudioSource audioSource = gameObject.AddComponent<AudioSource>();
            audioSource.clip = explsSound;
            audioSource.spatialBlend = 0.5f;
            audioSource.spatialize = true;
            audioSource.loop = false;
            audioSource.maxDistance = 400;
            audioSource.time = 3f;
            audioSource.Play();
        }

        private void PlayCrashSound()
        {
            AudioSource audioSource = gameObject.AddComponent<AudioSource>();
            audioSource.clip = crashSound;
            audioSource.spatialBlend = 0.5f;
            audioSource.spatialize = true;
            audioSource.playOnAwake = true;
            audioSource.loop = false;
            audioSource.maxDistance = 400;
            audioSource.Play();
        }
    }

}