﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Overly.FightGamesScripts
{
    [DisallowMultipleComponent]
    public class TargetMarkerManager : MonoBehaviour
    {
        [HideInInspector]
        public static TargetMarkerManager instance;

        public GameObject prefTargetMarker;
        public Dictionary<GameObject, GameObject> targetList; // target, UI_marker


        public void AddMarker(GameObject target)
        {
            GameObject targetMarker = Instantiate(prefTargetMarker, transform);
            targetList.Add(target, targetMarker);


            bool targetVisible = false;
            foreach (Renderer r in target.GetComponentsInChildren<Renderer>())
            {
                if (r.isVisible)
                {
                    targetVisible = true;
                    break;
                }
            }

            if (targetVisible)
            {
                targetMarker.SetActive(true);
                Vector3 targetPos = target.transform.position;
                Vector3 pos = Camera.main.WorldToScreenPoint(targetPos + Vector3.up * 3);
                targetMarker.transform.position = pos + Vector3.up * 50;
            }
            else
            {
                targetMarker.SetActive(false);
            }
        }
        public void RemoveMarker(GameObject target)
        {
            if (targetList.ContainsKey(target))
            {
                GameObject targetMarker = targetList[target];
                targetList.Remove(target);
                Destroy(targetMarker);
            }
        }

        // Start is called before the first frame update
        void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
                return;
            }
            targetList = new Dictionary<GameObject, GameObject>();
        }

        // Update is called once per frame
        void Update()
        {
            List<GameObject> deleteThese = new List<GameObject>();
            int index = 0;
            foreach (KeyValuePair<GameObject, GameObject> obj in targetList)
            {
                GameObject target = obj.Key;
                GameObject targetMarker = obj.Value;
                if (target != null && targetMarker != null)
                {
                    bool targetVisible = false;
                    foreach (Renderer r in target.GetComponentsInChildren<Renderer>())
                    {
                        if (r.isVisible)
                        {
                            targetVisible = true;
                            break;
                        }
                    }
                    if (targetVisible)
                    {
                        if (targetMarker.activeSelf == false)
                        {
                            targetMarker.SetActive(true);
                        }
                        Vector3 targetPos = target.transform.position;
                        Vector3 pos = Camera.main.WorldToScreenPoint(targetPos + Vector3.up * 3);
                        targetMarker.transform.position = pos + Vector3.up * 50;
                    }
                    else
                    {
                        targetMarker.SetActive(false);
                    }
                }
                else
                {
                    Debug.Log("#####################################");
                }
                index++;
            }
            for (int i = deleteThese.Count-1; i >=0; i--)
            {
                Destroy(deleteThese[i]);
            }
        }
    }
}
