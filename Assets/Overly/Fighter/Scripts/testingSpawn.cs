﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Overly.FightGamesScripts
{
public class testingSpawn : MonoBehaviour
{
    //public Transform spawnPoint;
    private int spawnedBoats = 0;
    public int maxBoatsCount = 20;
    [SerializeField] private float delay;
    private bool isActive;
    public GameObject boatPrefab;


    void spawnObjects()
    {
        GameObject obj = Instantiate(boatPrefab, gameObject.transform) as GameObject;
        Boat boat = obj.GetComponent<Boat>();
        boat.speed = 0.5f;
        obj.SetActive(true);

        spawnedBoats++;
        delay = 2f;
    }

    void Update()
    {
        if (spawnedBoats < maxBoatsCount && delay <= 0)
            spawnObjects();
        if (delay > 0)
            delay -= Time.deltaTime;

    }
}
}