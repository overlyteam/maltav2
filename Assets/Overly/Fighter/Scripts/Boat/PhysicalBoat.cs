﻿using System.Collections;
using System.Linq;
using UnityEngine;

namespace Overly.FightGamesScripts
{
    [RequireComponent(typeof(Boat))]
    public class PhysicalBoat : MonoBehaviour
    {
        [Tooltip("Tag of collider, which starts explosion")]
        public string activationTag;        
        [Tooltip("Amount of time, for how long the broken boat should be visible after explosion")]
        public float stayAtSceneTime = 10f;
        [Tooltip("Reference to explosion particle system")]
        public GameObject Explosion;
        [Tooltip("Reference to splash of water particle system")]
        public GameObject Splash;
        public GameObject boatMesh;
        public GameObject boatMeshA;

        [Header("Debug")]
        public bool autoExplode;
        public float autoExplodeAfter = 2f;

        private PhysicalBoatPart[] parts;
        public bool isExploded = false;
        private Boat boatObj;


        /*************************** INTERNAL LOGIC *****************************/

        public void Explode()
        {
            if (isExploded)
                return;

            scoreManager.score++;
            TargetMarkerManager.instance?.RemoveMarker(gameObject);

            isExploded = true;
            Splash.SetActive(true); //turn on paricle sustem
            Explosion.SetActive(true);
            boatMesh.SetActive(false);
            boatMeshA.SetActive(true);
            parts.ToList().ForEach(t => t.Activate());
            StartCoroutine(Disable());
        }



        /*************************** INTERNAL LOGIC *****************************/

        private void Awake()
        {
            parts = transform.GetComponentsInChildren<PhysicalBoatPart>(true);
            boatObj = GetComponent<Boat>();
        }

        private void Start()
        {
            if (autoExplode)
                Invoke("Activate", autoExplodeAfter);

            boatMesh.SetActive(true);
            boatMeshA.SetActive(false);
        }

        IEnumerator Disable()
        {
            boatObj.isActive = false;
            yield return new WaitForSeconds(stayAtSceneTime);
            boatObj.Destroy();
        }

        private void OnTriggerEnter(Collider other)
        {
            Debug.Log("Hit");

            if (other.CompareTag(activationTag))
            {
                Explode();
            }
        }
    }
}