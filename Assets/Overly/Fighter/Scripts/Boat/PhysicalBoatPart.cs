﻿using UnityEngine;

namespace Overly.FightGamesScripts
{
    [RequireComponent(typeof(Rigidbody))]
    public class PhysicalBoatPart : MonoBehaviour
    {
        private Rigidbody _rigidbody;
        private Renderer _renderer;
        


        /*************************** PUBLIC INTERFACE *************************/

        public void Activate()
        {
            //do explosion by enabling of physics (high pressure)
            _rigidbody.isKinematic = false;
            _renderer.material.color = Color.black;
            _renderer.material.SetFloat("_Smoothness", 0.3f);
        }



        /*************************** INTERNAL LOGIC *************************/

        private void Awake()
        {
            _rigidbody = GetComponent<Rigidbody>();
            _renderer = GetComponent<Renderer>();
        }
    }
}