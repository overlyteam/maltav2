﻿using System;
using UnityEngine;

namespace Overly.FightGamesScripts
{
    public class Boat : MonoBehaviour
    {
        [NonSerialized] public float speed;
        [NonSerialized] public float deadDistance;
        [NonSerialized] public bool isActive = true;
        public BattleshipsGame gameManager;

        public float passedDistance = 0f;


        private void Start()
        {
            TargetMarkerManager.instance?.AddMarker(gameObject);
        }

        /******************** PUBLIC INTERFACE *****************/
        public void Destroy()
        {
            //when object is destroyed, unregister this object in manager 
            gameManager.DestroyBoat(this);
        }

        private void OnDestroy()
        {
            TargetMarkerManager.instance?.RemoveMarker(gameObject);
        }



        /********************* INTERNAL LOGIC ******************/

        private void Update()
        {
            if (!isActive)
                return;

            if(gameManager == null)
                

            passedDistance += Time.deltaTime;
            transform.Translate(Vector3.right * speed * Time.deltaTime);
            if (passedDistance >= deadDistance)
            {
                Destroy();
            }
                
        }
    }
}