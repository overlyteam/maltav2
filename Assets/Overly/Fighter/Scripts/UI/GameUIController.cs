﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.EventSystems;

namespace Overly.FightGamesScripts
{
    public class GameUIController : MonoBehaviour
    {
        public static GameUIController instance;

        public GameObject endScreen;
        [Header("EndScreen")]
        public TextMeshProUGUI title;
        public List<GameObject> highestScores;
        List<string> highnames = new List<string>();
        List<int> highscore = new List<int>();
        int lastPlace = -1;
        int lastScore = -1;
        Game game;

        private void Awake()
        {
            Init();
            game = FindObjectOfType<Game>();
        }

        void Init()
        {
            if (instance == null)
            {
                instance = this;
            }
            else if (instance != this)
            {
                Destroy(gameObject);
            }
        }

        private void Start()
        {

            //   highestScores[3].GetComponent<InputField>().ActivateInputField();
        }

        // Start is called before the first frame update
        public void RestartGame()
        {
            game.ResetGame();
            endScreen.SetActive(false);
        }

        public void EndGame(string message, bool win, int score = 0)
        {
            endScreen.SetActive(true);
            Debug.Log(message + score.ToString());
            highscore = new List<int>();
            highnames = new List<string>();
            title.text = message;
            lastPlace = -1;
            int j = 0;
            for (int i = 0; i < 6; i++)
            {
                string scoreKey = game.gameName + "_hs_int_" + (i + 1).ToString();
                string nameKey = game.gameName + "_hs_str_" + (i + 1).ToString();
                string _name = "---";
                int _score = 0;
                if (PlayerPrefs.HasKey(scoreKey))
                {
                    _score = PlayerPrefs.GetInt(scoreKey);
                }
                if (PlayerPrefs.HasKey(nameKey))
                {
                    _name = PlayerPrefs.GetString(nameKey);
                }
                if (j < highestScores.Count)
                {
                    if (_score < score && lastPlace < 0 && win)
                    {
                        // new high score
                        lastPlace = j;
                        lastScore = score;
                        j++;
                    }

                    if (_name == "")
                    {
                        _name = "---";
                    }
                    highestScores[j].GetComponent<InputField>().text = _name + " - " + _score.ToString() + "pts";
                    highestScores[j].GetComponent<InputField>().interactable = false;
                    highestScores[j].GetComponent<InputField>().readOnly = true;
                    j++;
                    highscore.Add(_score);
                    highnames.Add(_name);

                }
            }
            if (lastPlace >= 0)
            {
                Debug.LogError("### Last place " + lastPlace.ToString());
                highestScores[lastPlace].GetComponent<InputField>().interactable = true;
                highestScores[lastPlace].GetComponent<InputField>().text = "Roger";
                highestScores[lastPlace].GetComponent<InputField>().readOnly = false;
                highestScores[lastPlace].GetComponent<InputField>().onEndEdit.AddListener(OnHighScoreSubmitted);
                highestScores[lastPlace].GetComponent<InputField>().ActivateInputField();
                highestScores[lastPlace].GetComponent<InputField>().Select();
            }
        }


        public void ClearHighScores()
        {
            foreach (GameObject obj in highestScores)
            {
                obj.GetComponent<InputField>().text = "--- 0";
            }
            for (int i = 0; i < 6; i++)
            {
                string scoreKey = game.gameName + "_hs_int_" + (i + 1).ToString();
                string nameKey = game.gameName + "_hs_str_" + (i + 1).ToString();

                if (PlayerPrefs.HasKey(scoreKey))
                {
                    PlayerPrefs.DeleteKey(scoreKey);
                }
                if (PlayerPrefs.HasKey(nameKey))
                {
                    PlayerPrefs.DeleteKey(nameKey);
                }
                PlayerPrefs.Save();
            }
        }

        public void OnHighScoreSubmitted(string text)
        {
            string name = text;
            if (name == "")
            {
                name = "---";
            }
            highestScores[lastPlace].GetComponent<InputField>().text = name + " - " + lastScore.ToString() + "pts";
            highestScores[lastPlace].GetComponent<InputField>().interactable = false;
            highestScores[lastPlace].GetComponent<InputField>().onEndEdit.RemoveAllListeners();
            highestScores[lastPlace].GetComponent<InputField>().readOnly = true;


            highscore.Insert(lastPlace, lastScore);
            highnames.Insert(lastPlace, name);
            //save data
            int count = Mathf.Min(highscore.Count, 6); // save no more thatn 6
            for (int i = 0; i < count; i++)
            {
                string scoreKey = game.gameName + "_hs_int_" + (i + 1).ToString();
                string nameKey = game.gameName + "_hs_str_" + (i + 1).ToString();
                Debug.LogError(nameKey);
                PlayerPrefs.SetInt(scoreKey, highscore[i]);
                PlayerPrefs.SetString(nameKey, highnames[i]);
            }
            PlayerPrefs.Save();
        }

        public void CloseGame()
        {
            if (SceneManager.sceneCount > 1)
            {
                SceneManager.UnloadSceneAsync(gameObject.scene);
            }
            else
            {
                SceneManager.LoadScene(0);
            }
        }
    }
}