﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
#if PLATFORM_ANDROID
using UnityEngine.Android;
#endif
#if PLATFORM_IOS
using UnityEngine.iOS;
#endif

namespace Overly.FightGamesScripts
{
    public class MenuUIController : MonoBehaviour
    {
        public string levelName;
        public GameObject uiLoading;
        public GameObject uiDescription;
        public GameObject uiTitleBar;
        public void ShowOnMap()
        {

        }


        public void OnBackBtnPressed()
        {
            SceneManager.LoadScene(GameSettings.instance.LevelToReturnTo);
        }

        public void RunGame()
        {
#if PLATFORM_ANDROID
            if (!Permission.HasUserAuthorizedPermission(Permission.Camera))
            {
                StartCoroutine("CheckForCameraPermissionAndRunGame");
            }
            else
            {
                LoadLevel();
            }
#endif
#if PLATFORM_IOS
        if (Application.HasUserAuthorization(UserAuthorization.WebCam)){
            LoadLevel();
        }
        else
        {
            StartCoroutine("CheckForCameraPermissionAndRunGame");
        }
#endif
        }

        IEnumerator CheckForCameraPermissionAndRunGame()
        {
#if PLATFORM_ANDROID
            Permission.RequestUserPermission(Permission.Camera);
            while (!Permission.HasUserAuthorizedPermission(Permission.Camera))
            {
                yield return new WaitForSeconds(.1f);
            }
#endif
#if PLATFORM_IOS
        Application.RequestUserAuthorization(UserAuthorization.WebCam);
        while (!Application.HasUserAuthorization(UserAuthorization.WebCam))
        {
            yield return new WaitForSeconds(.1f);
        }
#endif
            LoadLevel();
            yield return null;
        }

        void LoadLevel()
        {
            uiTitleBar.SetActive(false);
            uiDescription.SetActive(false);
            uiLoading.SetActive(true);
            SceneManager.LoadScene(levelName);
        }
    }
}