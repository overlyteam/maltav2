﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Overly.FightGamesScripts
{
public class AnimationHelper : MonoBehaviour
{
    public UnityEvent events;
    public void OnAnimationEvent()
    {
        events?.Invoke();
    }
}
}