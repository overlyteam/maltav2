﻿using System.Linq;
using UnityEngine;
using System.Collections.Generic;
using UnityEngine.XR.ARFoundation;
using TMPro;
using System.Collections;

namespace Overly.FightGamesScripts
{
    public class XrManager : MonoBehaviour
    {
        public bool ignoreTimer = false;
        [HideInInspector]
        public static XrManager instance;
        public Camera ArCamera;
        public ARPlaneManager arPlaneManager;
        public ARRaycastManager aRRaycastManager;
        public ScanFloor floorScanner;

        public GameObject startGameText;
        public GameObject timerText;
        public GameObject scoreText;
        public GameObject arGun;
        public GameObject gunPosMimic;
        public GameObject reticle;
        public GameObject crosshair;
        public GameObject finalScore;
        public TextMeshProUGUI winLoseText;
        public GameObject redBar;
        public GameObject greenBar;

        bool floorFound;
        [HideInInspector] public bool ins_gameEnded;

        [HideInInspector] public DeathFromAbove gameInstance;
        public timer timer;
        public float gameDuration;

        private ARSessionOrigin xrSession;
        private bool gameActive;
        private bool inputActive;

        static List<ARRaycastHit> hits = new List<ARRaycastHit>();

        private float highscore;
        public TextMeshProUGUI highscoreText;



        /************************** INTERNAL LOGIC *****************************/

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            gameInstance = FindObjectOfType<DeathFromAbove>();
            xrSession = FindObjectOfType<ARSessionOrigin>();
        }

        private void Start()
        {
            Screen.sleepTimeout = (int)SleepTimeout.NeverSleep;
            floorFound = false;
            RestartGame();
        }

        public void RestartGame()
        {
            if (!floorFound)
            {
                Debug.LogError("Resetting the game");
                gameInstance.gameObject.SetActive(false);
                startGameText.SetActive(true);
                arPlaneManager.enabled = true;
                floorScanner.ShowScanProgress(true);

                timer.time = gameDuration;
                timer.timerActive = true;
                gameActive = false;
                inputActive = true;

                timerText.SetActive(false);
                scoreText.SetActive(false);
                reticle.SetActive(false);
                crosshair.SetActive(false);


                finalScore.SetActive(false);
                winLoseText.gameObject.SetActive(false);
                highscoreText.gameObject.SetActive(false);
                redBar.SetActive(false);
                greenBar.SetActive(false);
                floorScanner.InitScanning();

                if (PlayerPrefs.HasKey("highscore"))
                    highscore = PlayerPrefs.GetFloat("highscore");
            }
            else
            {
                beginGame();
            }
        }


        public void DropGun()
        {
            floorFound = true;
            floorScanner.ShowScanProgress(false);
            beginGame();
        }
        void LateUpdate()
        {

            if (inputActive)
            {
                //if (Input.touchCount > 0 || Input.GetMouseButtonDown(0))//if touch hits an ar planem begin game
                //{
                //    Vector2 touchPositon = Input.GetTouch(0).position;
                //    //beginGame();


                //    if (aRRaycastManager.Raycast(touchPositon, hits, UnityEngine.XR.ARSubsystems.TrackableType.PlaneWithinPolygon))
                //    {
                //        foreach (ARRaycastHit hit in hits)
                //        {
                //            if (hit.trackableId == floorScanner.groundPlane.trackableId)
                //            {
                //                DropGun();
                //            }
                //        }

                //        //StartGame(hitPose);
                //    }

                //}
            }


            if (timer.timerActive == false)//end game, if timer ends!!!
            {
                if (gameActive == true)
                {
                    if (!ignoreTimer)
                    {
                        endGame();
                    }
                }

            }


            if (gameActive)
            {
                Vector3 gunOneLevel = new Vector3(gunPosMimic.transform.position.x, ArCamera.transform.position.y - 1.25f, gunPosMimic.transform.position.z);//set gun under camera
                //Vector3 gunRotation = new Vector3(0f, ArCamera.transform.eulerAngles.y, 0f);
                arGun.transform.position = gunOneLevel;// Vector3.Lerp(arGun.transform.position, gunOneLevel, 2f * Time.deltaTime);//for smooth following to camera
                //arGun.transform.eulerAngles = gunRotation;

                gameInstance.transform.rotation = Quaternion.LookRotation(-ArCamera.transform.forward); //face to cam
                Vector3 rt = gameInstance.transform.eulerAngles;
                rt.x = 0; rt.y += 180; rt.z = 0;
                gameInstance.transform.eulerAngles = rt;
            }
        }

        void beginGame()
        {
            Debug.LogError("Starting the game");
            StopCoroutine(delayEndGame());

            scoreManager.score = 0;//score reset

            gameInstance.gameObject.SetActive(true);//turn on airplane game            
            timerText.SetActive(true);
            scoreText.SetActive(true);
            reticle.SetActive(true);
            crosshair.SetActive(true);
            redBar.SetActive(true);
            greenBar.SetActive(true);

            startGameText.SetActive(false);
            finalScore.gameObject.SetActive(false);
            winLoseText.gameObject.SetActive(false);
            highscoreText.gameObject.SetActive(false);

            //plane detection
            //gameInstance.transform.position = pose.position; //place a pivor of the game where tap occured
            gameInstance.transform.position = ArCamera.transform.position;
            /*          
            Vector3 tmpPos = ArCamera.transform.position;//for pos
            tmpPos.x = arGun.transform.position.x;
            tmpPos.y = arGun.transform.position.y - 1f;
            tmpPos.z = arGun.transform.position.z;

            float yRotation = ArCamera.transform.rotation.eulerAngles.y;//for rot

            gameInstance.transform.eulerAngles = new Vector3(0f, yRotation, 0f);
            arGun.transform.eulerAngles = new Vector3(0f, yRotation, 0f);

            arGun.transform.localPosition = tmpPos;
            */
            //============



            arPlaneManager.enabled = false;
            SetAllPlanesActive(false); //hide planes
            //till here

            timer.timerActive = true;//start timer

            gameInstance.StartGame();//start game - init plane spawning

            gameActive = true;
            inputActive = false;


        }

        void endGame()
        {
            string message = "";
            bool win = false;
            if (scoreManager.score >= 1)
            {
                //PLAYER WON
                message = "WELL DONE, GUNNER!";
                win = true;
            }
            else
            {
                //PLAYER LOST
                message = "MISSION FAILED!";
            }
            GameUIController.instance.EndGame(message, win, scoreManager.score);

            gameInstance.EndGame();

            Debug.Log("END GAME - timer = 0");
            inputActive = false;
            gameInstance.gameObject.SetActive(false);//turn off game - plane spawning etc

            timerText.SetActive(false);
            scoreText.SetActive(false);
            reticle.SetActive(false);
            crosshair.SetActive(false);
            redBar.SetActive(false);
            greenBar.SetActive(false);

            timer.timerActive = false;

            timer.time = gameDuration;

            StartCoroutine(delayEndGame());

            startGameText.SetActive(false);
            gameActive = false;
        }
        public void ObBtnDone()
        {
            if (gameActive)
            {
                endGame();
            }
            else if (inputActive)
            {
                DropGun();
            }
        }

        IEnumerator delayEndGame()
        {
            //inputActive = false;
            yield return new WaitForSeconds(2f);
            //arPlaneManager.enabled = true;
            //SetAllPlanesActive(true);
            // these are not needed, already scanned the floor
            //startGameText.SetActive(true);
            StartCoroutine(disableInputFor(2f));

        }

        IEnumerator disableInputFor(float time)
        {
            yield return new WaitForSeconds(time);
            inputActive = true;
        }

        private void StartGame(Pose first)//if using plane detection
        {
            gameInstance.gameObject.SetActive(true);
            gameInstance.transform.position = first.position; //place a pivor of the game where tap occured

            gameInstance.transform.rotation = Quaternion.LookRotation(ArCamera.transform.forward); //face to cam
            Vector3 rt = gameInstance.transform.eulerAngles;
            rt.x = 0; rt.y -= 180; rt.z = 0;
            gameInstance.transform.eulerAngles = rt;

            //arPlaneManager.detectionFlags = UnityEngine.XR.ARExtensions.PlaneDetectionFlags.None; //turn off ArCore or ArKit
            arPlaneManager.enabled = false;

            SetAllPlanesActive(false); //hide planes
            gameObject.SetActive(false); //turn off XrManager
            gameInstance.StartGame(); //start game
        }

        // Hide all floor planes. Not working.
        private void SetAllPlanesActive(bool value)
        {
            foreach (var plane in arPlaneManager.trackables)
            {
                plane.gameObject.SetActive(value);
            }
        }
    }
}