﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

namespace Overly.FightGamesScripts
{
public class vrCamera : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        XRSettings.enabled = true;
        XRSettings.LoadDeviceByName("cardboard");
    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = InputTracking.GetLocalRotation(XRNode.CenterEye);

    }
}
}