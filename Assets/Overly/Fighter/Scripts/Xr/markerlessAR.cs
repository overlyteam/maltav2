﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Overly.FightGamesScripts
{
public class markerlessAR : MonoBehaviour
{
    //GYRO
    private Gyroscope gyro;
    private GameObject cameraContainer;
    private Quaternion rot;

    void Start()
    {
        //Check if we support both services

        //GYRO
        if (!SystemInfo.supportsGyroscope)
        {
            Debug.Log("does not supp gyro");
            //return;
        }


        //BOTH ARE ENABLED

        cameraContainer = new GameObject("Camera Container");
        cameraContainer.transform.position = transform.position;
        transform.SetParent(cameraContainer.transform);

        gyro = Input.gyro;
        gyro.enabled = true;

        print("enabled? " + gyro.enabled);

    }

    // Update is called once per frame
    void Update()
    {

        //Update gyro
        transform.localRotation = gyro.attitude * rot;
        Debug.Log("ROTATION = " + transform.localRotation);
    }
}
}