﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Overly.FightGamesScripts
{
    public class timer : MonoBehaviour
    {
        [HideInInspector] public float time;
        [HideInInspector] public int minutes;
        [HideInInspector] public int seconds;
        [HideInInspector] public bool timerActive;

        public TextMeshProUGUI timer_txt;

        private XrManager xrManager;

        void Start()
        {
            xrManager = FindObjectOfType<XrManager>();
            //timer_txt.GetComponent<TextMeshProUGUI>();
            timerActive = false;
            minutes = Mathf.FloorToInt(time / 60f);
            seconds = Mathf.FloorToInt(time - minutes * 60);
            string timeFormat = string.Format("{0:0}:{1:00}", minutes, seconds);

            timer_txt.text = timeFormat;// time.ToString("0:00");
        }


        void Update()
        {
            if (timerActive)
            {
                time -= 1 * Time.deltaTime;
                minutes = Mathf.FloorToInt(time / 60f);
                seconds = Mathf.FloorToInt(time - minutes * 60);
                string timeFormat = string.Format("{0:0}:{1:00}", minutes, seconds);
                timer_txt.text = timeFormat;

                if (time <= 0)//when timer ends
                {

                    time = 0f;
                    //xrManager.gameInstance.EndGame();
                    timerActive = false;

                    Debug.Log("time = " + time);

                }
            }
        }
    }
}