﻿using UnityEngine;
using UnityEngine.XR.ARFoundation;

namespace Overly.FightGamesScripts
{
    [RequireComponent(typeof(Light))]
    public class ARLightAmbient : MonoBehaviour
    {
        private Light l;

        void Start()
        {
            l = GetComponent<Light>();
            FindObjectOfType<ARCameraManager>().frameReceived += OnCameraFrameReceived;
        }

        void OnCameraFrameReceived(ARCameraFrameEventArgs eventArgs)
        {
            if (eventArgs.lightEstimation.averageBrightness != null)
            {
                l.intensity = eventArgs.lightEstimation.averageBrightness.Value;
            }
            if (eventArgs.lightEstimation.averageColorTemperature != null)
            {
                l.colorTemperature = eventArgs.lightEstimation.averageColorTemperature.Value;
                Debug.LogError(eventArgs.lightEstimation.averageColorTemperature.Value);
            }
            //        l.colorTemperature = eventArgs.lightEstimation.averageColorTemperature.Value;
        }

        void OnDisable()
        {
            FindObjectOfType<ARCameraManager>().frameReceived -= OnCameraFrameReceived;
        }
    }
}