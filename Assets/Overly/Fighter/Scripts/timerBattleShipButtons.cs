﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

namespace Overly.FightGamesScripts
{
    public class timerBattleShipButtons : MonoBehaviour
    {
        [HideInInspector] public float time;
        [HideInInspector] public int minutes;
        [HideInInspector] public int seconds;
        [HideInInspector] public bool timerActive;

        bool freeze;

        public TextMeshProUGUI timer_txt;


        void Start()
        {
            timerActive = false;
            timer_txt.text = "00:00";
        }

        public void FreezeTimer(bool freeze)
        {
            this.freeze = freeze;
        }

        public void ClearTimer()
        {
            timerActive = false;
        }


        void Update()
        {
            if (timerActive && !freeze)
            {
                time -= 1 * Time.deltaTime;
                minutes = Mathf.FloorToInt(time / 60f);
                seconds = Mathf.FloorToInt(time - minutes * 60);
                string timeFormat = string.Format("{0:0}:{1:00}", minutes, seconds);
                timer_txt.text = timeFormat;

                if (time <= 0)//when timer ends
                {

                    time = 0f;
                    //xrManager.gameInstance.EndGame();
                    timerActive = false;

                    Debug.Log("time = " + time);

                }
            }
        }
    }
}