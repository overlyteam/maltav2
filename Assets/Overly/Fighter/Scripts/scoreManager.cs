﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Overly.FightGamesScripts
{
    public class scoreManager : MonoBehaviour
    {
        public static int score;
        public TextMeshProUGUI scoreText;

        // Start is called before the first frame update
        void Start()
        {
            score = 0;
        }

        // Update is called once per frame
        void Update()
        {
            scoreText.text = score.ToString();
        }
    }
}