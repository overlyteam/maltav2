﻿
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;

namespace Overly.FightGamesScripts
{
    public class BattleshipsGame : Game
    {
        [Header("Settings")]
        public int maxBoatsCount;//10
        [Tooltip("How far from each other boats will be spawned")]
        public int dispersion;//3
        public float minBoatSpeed;//3
        public float maxBoatSpeed;//7
        [Tooltip("Length of game field. When distance will be reached - the boat will be destroyed")]
        public float totalDistance;//5

        [Header("References")]
        public Object boatPrefab;
        public Transform[] spawnPoints;
        private AR_ManagerBUTTONS arManager;
        private AR_ManagerWikitude arManagerW;

        private int spawnedBoats = 0;
        [SerializeField] private float delay;
        private bool isActive;
        private List<Boat> spawnedBoatList = new List<Boat>();

        public bool exploded;
        /// <summary>
        /// Energy that should be applied to force of cannon ball on next shoot
        /// </summary>}



        /*********************** PUBLIC INTERFACE **************************/

        public void DestroyBoat(Boat boat) // Unregister objects
        {
            spawnedBoatList.Remove(boat);
            spawnedBoats--;
            Destroy(boat.gameObject);
        }

        public void destroyAllBoats()
        {
            

            foreach (Boat boat in spawnedBoatList)
            {
                spawnedBoatList.Remove(boat);
                Destroy(boat.gameObject);
                spawnedBoats--;
            }
        }

        public void SpawnBoat()
        {

            if (spawnPoints.Length > 0 && spawnedBoats <= maxBoatsCount && isActive)
            {
                //=========SPAWN FROM A RIGHT SIDE=============================
                /*spawnedBoats++;
                int _rnd = new System.Random().Next(0, spawnPoints.Length);

                GameObject boatObj = Instantiate(boatPrefab, transform) as GameObject;
                Boat boat = boatObj.GetComponent<Boat>();
                spawnedBoatList.Add(boat);

                boat.gameManager = this;
                boat.speed = Random.Range(minBoatSpeed, maxBoatSpeed);

                Vector3 position = spawnPoints[_rnd].position;
                position.z += Random.Range(-dispersion, dispersion);
                boat.transform.position = position;
                boat.transform.rotation = spawnPoints[_rnd].rotation;
                */
                
                /*============SPAWN FROM LEFT SIDE===================================*/
                int randomL = Random.Range(-dispersion, dispersion);//spawn from different place on 1 axis
                GameObject boatObjL = Instantiate(boatPrefab, spawnPoints[0].position, spawnPoints[0].rotation, spawnPoints[0].transform) as GameObject;//create boats from left spawnPoint
                Boat boatL = boatObjL.GetComponent<Boat>();
                spawnedBoatList.Add(boatL);

                exploded = boatObjL.GetComponent<PhysicalBoat>().isExploded;

                Vector3 pos = boatL.transform.position;

                spawnedBoats++;
                if (arManager != null)
                {
                    boatL.transform.localScale = new Vector3(arManager.sizeValueChange, arManager.sizeValueChange, arManager.sizeValueChange);//get size from sliders and apply to boats
                }
                else
                {
                    boatL.transform.localScale = new Vector3(arManagerW.sizeValueChange, arManagerW.sizeValueChange, arManagerW.sizeValueChange);//get size from sliders and apply to 
                }
                boatL.transform.localPosition = new Vector3(0, 0, randomL);//randomize spawn position
                //print("spawn pos = " + pos);
                //boatL.transform.SetParent(spawnPoints[0].transform);//make boats a children of spawnpoint
                boatL.gameObject.SetActive(true);
                boatL.speed = Random.Range(minBoatSpeed, maxBoatSpeed);//speed range

                boatL.gameManager = this;

                boatL.deadDistance = totalDistance;
                
                delay = 2f;
            }
        }

        public override void EndGame()//end game - dont spawn anymore
        {
            destroyAllBoats();
            isActive = false;
        }

        public override void StartGame()//start spawning
        {
            isActive = true;
            SpawnBoat();
        }

        public override void ResetGame()//start spawning
        {
            if (arManager != null)
            {
                arManager.beginGame();
            }
            if (arManagerW != null)
            {
                arManagerW.RestartGame();
            }
        }


        /***************************** INTERNAL LOGIC **********************************/

        private void Start()
        {
            arManager = FindObjectOfType<AR_ManagerBUTTONS>();
            arManagerW = FindObjectOfType<AR_ManagerWikitude>();
        }

        // Update is called once per frame
        void Update()
        {
            if (spawnedBoats < maxBoatsCount && delay <= 0 && isActive)
            {
                SpawnBoat();
            }  
            if (delay > 0)
                delay -= Time.deltaTime;
        }

        //117 height
        //1.3 size
        //370 distance
    }
}