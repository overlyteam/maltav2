﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Overly.FightGamesScripts
{
    public class DeathFromAbove : Game
    {
        public Gun aicraftGun;
        public Object junker;
        public Transform[] spawnPoint;
        public float spawnOffset;
        public float maximumAircraftsOnScene = 4;
        public bool gameEnded;
        public bool allowToShoot = false;
        public Animator CountdownAnimator;
        public GameObject HUD;

        private List<Aircraft> spawnedObjects = new List<Aircraft>();



        /******************* PUBLIC INTERFACE **********************/

        public void OnCountdownDone()
        {
            allowToShoot = true;
        }

        public override void StartGame()//start spawning planes
        {
            HUD.SetActive(true);
            CountdownAnimator.SetTrigger("Play");
            StartCoroutine(PlaneSpawner());
            gameEnded = false;
            allowToShoot = false;
        }
        public override void ResetGame()
        {
            XrManager.instance.RestartGame();
        }

        public override void EndGame()//stop spawning planes
        {
            HUD.SetActive(false);
            Debug.Log("ENDED");
            gameEnded = true;
            StopAllCoroutines();

            //____ Destroy all instances of airplane
            for (int i = 0; i < spawnedObjects.Count; i++)
                if (spawnedObjects[i].gameObject != null)
                    Destroy(spawnedObjects[i].gameObject);
            spawnedObjects.Clear();
            //__ end
        }

        public void returnVariable()
        {
            //return gameEnded;
        }

        public void DestroyPlane(Aircraft aircraft) //Unregister aircraft
        {
            spawnedObjects.Remove(aircraft);
        }

        private void OnEnable()
        {
            // StartGame();
        }

        private void OnDisable()
        {
            EndGame();
        }



        /********************* INNER LOGIC ************************/

        private IEnumerator PlaneSpawner()
        {
            while (true)
            {
                //Spawn airplane after 1-2.5 sec if airplanes are less than maximumAircraftsOnScene value
                yield return new WaitForSeconds(UnityEngine.Random.Range(1f, 2.5f));
                if (spawnedObjects.Count < maximumAircraftsOnScene)
                {
                    int point = Random.Range(0, spawnPoint.Length); //random direction for airplane
                    GameObject go = Instantiate(junker, spawnPoint[point].position, spawnPoint[point].rotation) as GameObject; //spawn
                    spawnedObjects.Add(go.transform.GetComponent<Aircraft>()); //register airplane

                    //____ random position for airplane
                    Vector3 pos = go.transform.localPosition;
                    if (point == 0)
                        pos.x = UnityEngine.Random.Range(-spawnOffset, spawnOffset);
                    else
                        pos.z = UnityEngine.Random.Range(-spawnOffset, spawnOffset);
                    go.transform.localPosition = pos;
                    //__ end
                }
            }
        }

        // Update input
        void Update()
        {
            if (allowToShoot)
            {
                if (Input.touchCount > 0)
                {
                    Touch t = Input.GetTouch(0);
                    if (t.phase == TouchPhase.Began)
                    {
                        aicraftGun.Shoot();
                    }
                }
            }
        }


    } //end of class

}