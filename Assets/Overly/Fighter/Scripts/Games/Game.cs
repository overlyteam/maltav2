﻿using UnityEngine;

namespace Overly.FightGamesScripts
{
    public abstract class Game : MonoBehaviour
    {
        public string gameName;
        public abstract void StartGame();
        public abstract void EndGame();
        public abstract void ResetGame();
    }
}