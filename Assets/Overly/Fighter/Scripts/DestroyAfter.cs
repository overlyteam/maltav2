﻿using System.Collections;
using UnityEngine;

namespace Overly.FightGamesScripts
{
    public class DestroyAfter : MonoBehaviour
    {
        public float delay;

        // Start is called before the first frame update
        private IEnumerator Start()
        {
            yield return new WaitForSeconds(delay);
            Destroy(gameObject);
        }

        private void OnCollisionEnter(Collision collision)
        {
            if (collision.collider.transform.GetComponent<PhysicalBoat>() || collision.collider.transform.GetComponent<Boat>())
            {
                PhysicalBoat boat = collision.collider.transform.GetComponent<PhysicalBoat>();
                boat?.Explode();
                Destroy(gameObject);//destroy ball if boat has been hit
            }
        }

    }
}