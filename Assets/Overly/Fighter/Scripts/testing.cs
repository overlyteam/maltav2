﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Overly.FightGamesScripts
{
public class testing : MonoBehaviour
{
    public float speed;
    public float deadDistance;
    public bool isActive = true;

    public float passedDistance = 0f;

    /******************** PUBLIC INTERFACE *****************/

    public void Destroy()
    {
        //when object is destroyed, unregister this object in manager 
        Destroy(this);
    }



    /********************* INTERNAL LOGIC ******************/

    private void Update()
    {
        if (!isActive)
            return;

        passedDistance += Time.deltaTime;
      //  transform.Translate(Vector3.forward * Time.deltaTime);
        if (passedDistance >= deadDistance)
            Destroy();
    }
}
}