﻿using UnityEngine;

namespace Overly.FightGamesScripts
{
public class lookAtBUTTONS : MonoBehaviour
{
    public float speed = 5f;
    public int clampAngle;
    public Transform[] ballSpawnPoints;
    public Camera ArCamera;
    public AR_ManagerBUTTONS arManager;
    public GameObject explosion;
    public AudioSource explosionSound;

    [HideInInspector] public float tmp;
    [HideInInspector] public Quaternion rotationOnlyY;
    [HideInInspector] public Vector3 tmpClamp;

    void Update()
    {
        Vector3 rayOrigin = ArCamera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0));//ray from middle of the screen
        RaycastHit hit;
        
        if (Physics.Raycast(rayOrigin, ArCamera.transform.forward, out hit, float.MaxValue)) //moving cannon to crossahir position
        {
            Quaternion rotation = Quaternion.LookRotation(hit.point);
            rotationOnlyY = Quaternion.Euler(transform.rotation.eulerAngles.x, rotation.eulerAngles.y, transform.rotation.eulerAngles.z);//rotate only around y axis
            //print("rotation of camera#: " + rotationOnlyY.eulerAngles.y);

            //print("spawnROTATION!!! " + arManager.spawnAngle.y);
            //float minAngle = arManager.spawnAngle.y - clampAngle;
            //float maxAngle = arManager.spawnAngle.y + clampAngle;

            //print("MIN | CURRENT | MAX: " + minAngle + " -> " + arManager.spawnAngle.y + " <- " + maxAngle);

            //float tmp = Mathf.Clamp(rotationOnlyY.eulerAngles.y, minAngle, maxAngle);//clamping angle
        }
    }
}
}