﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Overly.FightGamesScripts
{
public class innerCrosshairController : MonoBehaviour
{
    private Gyroscope gyro;
    public Transform reticleCenter;
    public Transform reticleOutline;
    public float circleRadius;
    public float speed;

    private float LerpSpeed;
    private Vector3 newLocation;

    void Start()
    {
        gyro = Input.gyro;
        gyro.enabled = true;
    }

    // Update is called once per frame
    void Update()
    {
        circleRadius = Vector3.Distance(reticleCenter.position, reticleOutline.position);
        
        moveCrosshair();
        movementController();
    }

    void moveCrosshair()
    {
        LerpSpeed = speed * Time.deltaTime;
        transform.position = Vector3.Lerp(transform.position, reticleCenter.position, 1f * Time.deltaTime);
        print("reticle pos: " + transform.position);
        print("mid pos: " + reticleCenter.position);
    }

    void movementController()
    {
        //distance - check 
        newLocation = transform.position;
        Vector3 centerPosition = reticleCenter.position; //center of outer crosshair(reticle)
        float distance = Vector3.Distance(newLocation, centerPosition); //distance from crosshair to reticle

        if (distance > circleRadius) //If the distance is less than the radius, it is already within the circle.
        {
            print("OUT OF BOUNDS!");
            Vector3 fromOriginToObject = newLocation - centerPosition; //~GreenPosition~ - *BlackCenter*
            fromOriginToObject *= circleRadius / distance; //Multiply by radius //Divide by Distance
            newLocation = centerPosition + fromOriginToObject; //*BlackCenter* + all that Math
            transform.position = newLocation;

        }
    }
    
}
}