﻿using UnityEngine;
using UnityEngine.UI;

namespace Overly.FightGamesScripts
{
    public class Gun : MonoBehaviour
    {
        [Header("Title")]
        public bool useYaw = false;
        public float yawSpeed = 10f;
        [Range(0, 180)]
        public float minYaw = 0f;
        [Range(180, 360)]
        public float maxYaw = 360f;
        public Transform yawTransform;

        [Header("Pitch")]
        public bool usePitch = false;
        public float pitchSpeed = 10f;
        [Range(0, 180)]
        public float minPitch = 180f;
        [Range(180, 360)]
        public float maxPitch = 265f;
        public Transform pitchTransform;

        [Header("Settings")]
        public GameObject explosion;
        public AudioSource explosionSound;
        public Object ballObject;
        public Transform[] ballSpawnPoints;
        public RectTransform reticle;
        public bool useCameraToRotate;
        public float powderEnergy = 7000;
        public int raycastReceiver = 20;
        public float horizontalDegreesOffset = 45f;
        public GameObject aim;
        public shootManager shootmanager;
        public Aircraft aircraft;
        public bool hasExploded;
        public Image screenFlash;
        public Transform reticleCenter;
        //public bool useOfBullets = true;

        private float pitch;
        private float yaw;
        private bool flashScreenNow;


        /************************ PUBLIC INTERFACE ****************************/

        // Start is called before the first frame update
        public void Shoot()
        {
            if (shootmanager.canShoot)
            {
                shootmanager.canShoot = false;
                shootmanager.resetGreenBar();
                shootmanager.flashScreenNow = true;

                explosion?.SetActive(false); //Activate particle system
                explosion?.SetActive(true);
                explosionSound.Play(); //Play explosion sound

                Ray ray = new Ray(ballSpawnPoints[0].position, ballSpawnPoints[0].forward);
                Ray uiRay = Camera.main.ScreenPointToRay(reticleCenter.position);
                RaycastHit hit;

                if (Physics.Raycast(uiRay, out hit, 1 << 10)) //check for collision with airplane
                {
                    aircraft = hit.collider.transform.GetComponent<Aircraft>();
                    aircraft?.Explode();
                }
            }
            
        }     


        /************************* INTERNAL LOGIC *****************************/

        private void Awake()
        {
            pitch = minPitch;
            yaw = 180f;
            flashScreenNow = false;
        }

        // Update is called once per frame
        private void Update()
        {
            //print("pitch: " + pitch);

            //CROSSHAIR CONTROLLS
            Ray uiRay = Camera.main.ScreenPointToRay(reticleCenter.position);
            Ray aimRay = new Ray(ballSpawnPoints[0].position, ballSpawnPoints[0].forward);
            RaycastHit aimHit;
            if (Physics.Raycast(uiRay, out aimHit, float.MaxValue, 1 << raycastReceiver))
            {
                if (aim != null)
                {
                    //Debug.Log("AIM ENABLED");
                    aim.SetActive(true);
                    aim.transform.position = Vector3.Lerp(aim.transform.position, aimHit.point, 5f * Time.deltaTime);//crosshair tries to "catch" the same position as the ray in X speed
                    aim.transform.LookAt(transform.position);//crosshair always "looks" at the camera
                }
            }
            else
            {
                if (aim != null)
                    aim.SetActive(false);
            }

            if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Space))
                Shoot();

            if (useCameraToRotate) //version for mobiles
            {
                //Ray raySc = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2f, Screen.height / 4f));
                Ray ray = Camera.main.ScreenPointToRay(reticleCenter.position);
                //Debug.DrawRay(new Vector3(Screen.width /2f, Screen.height / 2f,0), Camera.main.transform.forward);
                RaycastHit hit;
                if (Physics.Raycast(ray, out hit, float.PositiveInfinity, 1 << raycastReceiver))
                {
                    Vector3 targetPos = hit.point;
                    Vector3 gunPos = ballSpawnPoints[0].position;
                    Vector3 targetDir = targetPos - gunPos;
                    if (targetDir.y > 0)
                    {

                        float p = Mathf.Asin(targetDir.y / targetDir.magnitude) * Mathf.Rad2Deg; //Get difference of pitch between gun and required direction
                        pitch = 180 + p;

                        Vector3 v_x = Vector3.ProjectOnPlane(targetDir, Vector3.up); //Make projection of required direction vector to XZ plane. Now, Y can be ignore (it is 0)
                        Vector3 v_y = yawTransform.forward; //Get gun's current direction
                        float angle = Vector3.SignedAngle(v_x, v_y, Vector3.up) * -1f; //Get angle between both vectors
                        yaw = yawTransform.localEulerAngles.y + angle; //apply changes
                    }
                }
            }
            else //version for Editor. Here all is easy
            {
                if (Input.GetKey(KeyCode.W))
                    pitch += Time.deltaTime * pitchSpeed;
                if (Input.GetKey(KeyCode.S))
                    pitch -= Time.deltaTime * pitchSpeed;
                if (Input.GetKey(KeyCode.A))
                    yaw -= Time.deltaTime * yawSpeed;
                if (Input.GetKey(KeyCode.D))
                    yaw += Time.deltaTime * yawSpeed;

                pitch = Mathf.Clamp(pitch, minPitch, maxPitch);
                yaw = Mathf.Clamp(yaw, minYaw, maxYaw);
            }

            //____ smootly rotate gun to target direction
            if (useYaw)
            {
                Vector3 angles = yawTransform.localEulerAngles;
                angles.y = Mathf.LerpAngle(angles.y, yaw, Time.deltaTime * 5);
                yawTransform.localEulerAngles = angles;
            }

            if (usePitch)
            {
                Vector3 angles = pitchTransform.localEulerAngles;
                angles.z = Mathf.LerpAngle(angles.z, pitch, Time.deltaTime * 5);
                pitchTransform.localEulerAngles = angles;
                pitch = Mathf.Clamp(pitch, minPitch, maxPitch);
            }
        }

        private static float ClampRotation(float w)
        {
            w = w % 360;
            if (w < 0)
                w += 360;
            return w;
        }

        
    }
}
