﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Overly.FightGamesScripts
{
public class shootManager : MonoBehaviour
{
    public RectTransform redBar;
    public RectTransform greenBar;
    public float barLoadingSize;
    [HideInInspector] public bool canShoot;

    private bool barNull;
    private float widthGrow;

    // Start is called before the first frame update
    void Start()
    {
        canShoot = false;
        resetGreenBar();
    }

    // Update is called once per frame
    void Update()
    {
        loadingBar();
        flashScreen();
    }

    void loadingBar()
    {
        if (!canShoot)
        {
            // (barLoadingSize / 2f) = speed
            widthGrow += (barLoadingSize / 2f)  * Time.deltaTime;
            greenBar.sizeDelta = new Vector2(greenBar.rect.width, widthGrow);
            //greenBar.sizeDelta = new Vector2(widthGrow, greenBar.localScale.y);
            //print("width: " + greenBar.sizeDelta.x + "|| duration: " + loadDuration);

            if (widthGrow >= barLoadingSize)
            {
                widthGrow = barLoadingSize;
                greenBar.sizeDelta = new Vector2(greenBar.rect.width, widthGrow);
                //greenBar.sizeDelta = new Vector2(widthGrow, greenBar.localScale.y);
                canShoot = true;
                print("CAN SHOOT " + canShoot);
            }
        }
    }

    public void resetGreenBar()
    {
        //greenBar.sizeDelta = new Vector2(0f, greenBar.localScale.y);
        greenBar.sizeDelta = new Vector2(greenBar.rect.width, 0f);
        widthGrow = 0f;
    }

    [HideInInspector] public bool flashScreenNow;
    public Image screenFlash;
    public void flashScreen()
    {
        Color opaque = new Color(255f, 255f, 255f, 0f);
        Color flash = new Color(255f, 255f, 255f, 180f);

        if (flashScreenNow)
        {
            screenFlash.color = Color.Lerp(screenFlash.color, flash, 10f * Time.deltaTime);

            if (screenFlash.color.a >= 100f)
                flashScreenNow = false;
        }

        if (!flashScreenNow)
        {
            //Color theFlash = new Color(255f, 255f, 255f, 130f);
            screenFlash.color = Color.Lerp(screenFlash.color, opaque, 10f * Time.deltaTime);
        }
    }
}
}