﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Overly.FightGamesScripts
{
public class shootManagerBUTTONS : MonoBehaviour
{
    public Button button;
    //public float loadDuration;
    public bool canShoot;
    public GameObject imageShotCompleted;
    public float reloadTime = 1;
    float reloadTimer;

    private Color buttonColor;
    // Start is called before the first frame update
    void Start()
    {
        resetShot();
        buttonColor = button.colors.normalColor;
        imageShotCompleted.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        //print("can shoot = " + canShoot);
        loadingShot();
    }

    void loadingShot()
    {
        if (!canShoot)
        {
            Vector3 fullScale = new Vector3(1f, 1f, 1f);
            Vector3 lowScale = new Vector3(0.1f, 0.1f, 0.1f);
            reloadTimer = Mathf.Clamp(reloadTimer + Time.deltaTime, 0, reloadTime);

            button.transform.localScale = Vector3.Lerp(lowScale, fullScale, reloadTimer / reloadTime);

            if (reloadTimer == 1)
            {
               // button.transform.localScale = fullScale;
                canShoot = true;
                imageShotCompleted.SetActive(true);
            }

        }
    }


    public void resetShot()
    {
        //Color greenWeak = new Color(35f, 255f, 274f, 10f);
        Vector3 lowScale = new Vector3(0.1f, 0.1f, 0.1f);

        button.transform.localScale = Vector3.Lerp(button.transform.localScale, lowScale, 30f * Time.deltaTime);
        canShoot = false;
        reloadTimer = 0;
        imageShotCompleted.SetActive(false);
    }
}
}