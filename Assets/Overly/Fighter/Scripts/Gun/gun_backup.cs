﻿using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.EventSystems;

namespace Overly.FightGamesScripts
{
    public class gun_backup : MonoBehaviour
    {
        [Header("Title")]
        public bool useYaw = false;
        public float yawSpeed = 10f;
        [Range(-90, 180)]
        public float minYaw = 0f;
        [Range(0, 180)]
        public float maxYaw = 360f;
        public Transform yawTransform;

        [Header("Pitch")]
        public bool usePitch = false;
        public float pitchSpeed = 10f;
        [Range(0, 180)]
        public float minPitch = 180f;
        [Range(180, 360)]
        public float maxPitch = 265f;
        public Transform pitchTransform;

        [Header("Buttons / UI")]
        public Button leftButton;
        public Button rightButton;
        public Slider powderSlider;
        public shootManagerBUTTONS shootManagerButtons;

        [Header("Settings")]
        public AR_ManagerWikitude arWiki;
        public GameObject explosion;
        public AudioSource explosionSound;
        public GameObject ballObject;
        public Transform[] ballSpawnPoints;
        public bool useCameraToRotate;
        public float powderEnergy = 7000;
        public int raycastReceiver = 20;
        public float horizontalDegreesOffset = 45f;
        public GameObject aim;

        public float pitch;
        public float yaw;

        private bool LpointerDown;
        private bool RpointerDown;

        private float powerValueSaved;

        public Animator char_anim;
        public Animator cannon_anim;
        public Animator push_char_anim;
        public Animator reload_char_anim;

        float reloadSpeed;

        float yawCog = 0f;

        /************************ PUBLIC INTERFACE ****************************/

        // Start is called before the first frame update
        public void Shoot()
        {
            if (shootManagerButtons.canShoot)//SHOOTING
            {
                shootManagerButtons.canShoot = false;
                shootManagerButtons.resetShot();

                char_anim.SetBool("shoot", true);//ANIMATIONS OF 3 CHARACTERS
                cannon_anim.SetTrigger("shoot");


                reload_char_anim.SetFloat("Speed", reloadSpeed);
                reload_char_anim.SetTrigger("hasShot");

                explosion?.SetActive(false); //Activate particle system
                explosion?.SetActive(true);
                explosionSound.Play(); //Play explosion sound

                Ray ray = new Ray(ballSpawnPoints[0].position, ballSpawnPoints[0].forward);
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit)) //check for collision WITH PLAN
                {
                    GameObject instantiatedBall;
                    Rigidbody rb;
                    if (arWiki != null)
                    {
                        if (arWiki.overlymarker_temp != null)
                        {
                            float localscale = 0.3f;
                            instantiatedBall = Instantiate(ballObject, ballSpawnPoints[0].position, ballSpawnPoints[0].rotation, arWiki.overlymarker_temp.Drawable.transform) as GameObject;//SPAWN CANNON BALL
                            instantiatedBall.transform.localScale = Vector3.one * localscale;
                            rb = instantiatedBall.GetComponent<Rigidbody>();
                            rb.AddForce(ballSpawnPoints[0].forward * powderEnergy);//ADD FORCE TO THE BALL
                        }
                    }
                    else
                    {
                        instantiatedBall = Instantiate(ballObject, ballSpawnPoints[0].position, ballSpawnPoints[0].rotation) as GameObject;//SPAWN CANNON BALL
                        rb = instantiatedBall.GetComponent<Rigidbody>();
                        rb.AddForce(ballSpawnPoints[0].forward * powderEnergy);//ADD FORCE TO THE BALL
                    }
                }
            }

        }


        /************************* INTERNAL LOGIC *****************************/

        private void Start()
        {
            if (PlayerPrefs.HasKey("powerSaved"))
                powerValueSaved = PlayerPrefs.GetFloat("powerSaved");


            powderSlider.value = powerValueSaved;
            powderEnergy = powderSlider.value;

            LpointerDown = false;
            RpointerDown = false;

            AnimationClip[] clips = reload_char_anim.runtimeAnimatorController.animationClips;
            reloadSpeed = 1f;
            foreach (AnimationClip clip in clips)
            {
                if (clip.name == "RELOAD")
                {
                    reloadSpeed = clip.length / shootManagerButtons.reloadTime;
                }
            }
        }

        // Update is called once per frame
        private void Update()
        {
            Ray aimRay = new Ray(ballSpawnPoints[0].position, ballSpawnPoints[0].forward);
            RaycastHit aimHit;
            //if (Physics.Raycast(aimRay, out aimHit, float.MaxValue))
            if (Physics.Raycast(aimRay, out aimHit, float.MaxValue, 1 << raycastReceiver))
            {
                if (aim != null)
                {

                }
            }
            else
            {
                if (aim != null)
                {
                    //  Debug.Log("AAAAA4");
                    //  aim.SetActive(false);
                }
            }

            if (Input.GetKeyDown(KeyCode.Return) || Input.GetKeyDown(KeyCode.Space))
                Shoot();

            if (useCameraToRotate) //version for mobiles, TO CONTROL WITH CAMERAS MOVEMENT
            {
                Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2f, Screen.height / 2f));
                RaycastHit hit;

                if (Physics.Raycast(ray, out hit, float.PositiveInfinity, 1 << raycastReceiver))
                {
                    Vector3 targetPos = hit.point;
                    Vector3 gunPos = ballSpawnPoints[0].position;
                    Vector3 targetDir = targetPos - gunPos;

                    float p = Mathf.Asin(targetDir.y / targetDir.magnitude) * Mathf.Rad2Deg; //Get difference of pitch between gun and required direction
                    pitch = 180 + p;

                    Vector3 v_x = Vector3.ProjectOnPlane(targetDir, Vector3.up); //Make projection of required direction vector to XZ plane. Now, Y can be ignore (it is 0)
                    Vector3 v_y = yawTransform.forward; //Get gun's current direction
                    float angle = Vector3.SignedAngle(v_x, v_y, Vector3.up) * -1f; //Get angle between both vectors
                    yaw = yawTransform.localEulerAngles.y + angle; //apply changes

                    // different method to get pitch and yaw
                    // Quaternion quatAngles = gameObject.transform.rotation;//gets gameobjects roation in quaternions
                    //     float x = quatAngles.normalized.x;
                    //     float y = quatAngles.normalized.y;
                    //     float z = quatAngles.normalized.z;
                    //     float w = quatAngles.normalized.w;

                    Quaternion quat = transform.rotation;
                    float x = quat.x;
                    float y = quat.y;
                    float z = quat.z;
                    float w = quat.w;

                    // roll  = Mathf.Atan2(2*y*w - 2*x*z, 1 - 2*y*y - 2*z*z);
                    // pitch = Mathf.Asin(2*x*y + 2*z*w);
                    // yaw = Mathf.Atan2(2*x*w - 2*y*z, 1 - 2*x*x - 2*z*z);
                }
            }
            else //version for Editor. Here all is easy
            {

                if (Input.GetKey(KeyCode.W))
                    pitch += Time.deltaTime * pitchSpeed;
                if (Input.GetKey(KeyCode.S))
                    pitch -= Time.deltaTime * pitchSpeed;
                if (Input.GetKey(KeyCode.A))
                    yaw -= Time.deltaTime * yawSpeed;
                if (Input.GetKey(KeyCode.D))
                    yaw += Time.deltaTime * yawSpeed;
            }

            if (Input.GetKeyDown(KeyCode.LeftArrow))
                LonPointerDown();

            if (Input.GetKeyDown(KeyCode.RightArrow))
                RonPointerDown();

            if (Input.GetKeyUp(KeyCode.LeftArrow))
                LonPointerUp();

            if (Input.GetKeyUp(KeyCode.RightArrow))
                RonPointerUp();


            //=======================TO-MOVE-CANNON-WITH-UI-BUTTONS=========================
            if (LpointerDown)//LEFT BUTTON PRESSED
            {
                yaw -= Time.deltaTime * yawSpeed / 3f;//MAKE ROTATION
                yaw = Mathf.Clamp(yaw, minYaw, maxYaw);//CLAMP ROTATION

                yawCog += Time.deltaTime * yawSpeed / 1.2f;//MAKE THE LITTLE COGS GO OTHER WAY
                yawCog = Mathf.Clamp(yawCog, -113f, 113f);

                if (yaw <= maxYaw && yaw >= minYaw)
                {
                    //leftCog.transform.localEulerAngles = new Vector3(-90f, 0f, yawCog);
                    //rightCog.transform.localEulerAngles = new Vector3(-90f, 0f, yawCog);
                    //rotaryCog.transform.localEulerAngles = new Vector3(-90f, 0f, yaw);
                }
            }

            if (RpointerDown)//RIGHT BUTTON PRESSED
            {
                yaw += Time.deltaTime * yawSpeed / 3f;
                yaw = Mathf.Clamp(yaw, minYaw, maxYaw);

                yawCog -= Time.deltaTime * yawSpeed / 1.2f;
                yawCog = Mathf.Clamp(yawCog, -113f, 113f);

                if (yaw <= maxYaw && yaw >= minYaw)
                {
                    //leftCog.transform.localEulerAngles = new Vector3(-90f, 0f, yawCog);
                    //rightCog.transform.localEulerAngles = new Vector3(-90f, 0f, yawCog);
                    //rotaryCog.transform.localEulerAngles = new Vector3(-90f, 0f, yaw);
                }
            }

            //____ smootly rotate gun to target direction
            if (useYaw)
            {
                Vector3 angles = yawTransform.localEulerAngles;
                angles.y = Mathf.LerpAngle(angles.y, yaw, Time.deltaTime * 5);
                yawTransform.localEulerAngles = angles;
            }

            if (usePitch)
            {
                Vector3 angles = pitchTransform.localEulerAngles;
                angles.z = Mathf.LerpAngle(angles.z, pitch, Time.deltaTime * 5);
                pitchTransform.localEulerAngles = angles;
            }

        }

        private static float ClampRotation(float w)
        {
            w = w % 360;
            if (w < 0)
                w += 360;
            return w;
        }

        public void LonPointerUp()//l POINTER UP
        {
            LpointerDown = false;
            push_char_anim.SetBool("isMoving", false);
        }

        public void LonPointerDown()//l POINTER DOWN
        {
            LpointerDown = true;
            push_char_anim.SetBool("isMoving", true);
        }

        public void RonPointerUp()//R POINTER UP
        {
            RpointerDown = false;
            push_char_anim.SetBool("isMoving", false);
        }

        public void RonPointerDown()//R POINTER DOWN
        {
            RpointerDown = true;
            push_char_anim.SetBool("isMoving", true);
        }

        public void moveLeft()
        {
            yaw -= Time.deltaTime * yawSpeed;
            yaw = Mathf.Clamp(yaw, minYaw, maxYaw);
        }

        public void moveRight()
        {
            yaw += Time.deltaTime * yawSpeed;
            yaw = Mathf.Clamp(yaw, minYaw, maxYaw);
        }

        public void powderSliderChange(float value)//GET VALUE FROM SLIDER
        {
            powderEnergy = value;

            PlayerPrefs.SetFloat("powerSaved", powderEnergy);
        }
    }
}
