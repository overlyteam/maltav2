﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Overly.FightGamesScripts
{
public class lookAt : MonoBehaviour
{
    public float speed = 5f;
    public int clampAngle;
    public Transform[] ballSpawnPoints;
    public GameObject aim;
    public Camera ArCamera;
    public AR_Manager arManager;
    public GameObject explosion;
    public AudioSource explosionSound;
    public shootManagerBUTTONS shootManager;

    [HideInInspector] public float tmp;
    [HideInInspector] public Quaternion rotationOnlyY;
    [HideInInspector] public Vector3 tmpClamp;

    void Update()
    {
        Vector3 rayOrigin = ArCamera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0));//ray from middle of the screen
        RaycastHit hit;

        //if (Physics.Raycast(rayOrigin, ArCamera.transform.forward, out hit, float.MaxValue)) //moving cannon to crossahir position
        //if (Physics.Raycast(ballSpawnPoints[0].position, ballSpawnPoints[0].forward, out hit, float.MaxValue)) //moving cannon to crossahir position
        if (Physics.Raycast(rayOrigin, ArCamera.transform.forward, out hit, float.MaxValue)) //moving cannon to crossahir position
        {
            Quaternion rotation = Quaternion.LookRotation(hit.point);
            rotationOnlyY = Quaternion.Euler(transform.rotation.eulerAngles.x, rotation.eulerAngles.y, transform.rotation.eulerAngles.z);//rotate only around y axis
            //print("rotation of camera#: " + rotationOnlyY.eulerAngles.y);

            //print("spawnROTATION!!! " + arManager.spawnAngle.y);
            float minAngle = arManager.spawnAngle.y - clampAngle;
            float maxAngle = arManager.spawnAngle.y + clampAngle;
            //print("MIN | CURRENT | MAX: " + minAngle + " -> " + arManager.spawnAngle.y + " <- " + maxAngle);

            float tmp = Mathf.Clamp(rotationOnlyY.eulerAngles.y, minAngle, maxAngle);//clamping angle
            //print("tmp = clampRotation: " + tmp);
            //tmpClamp = new Vector3(tmpGetRotation.x, tmp, tmpGetRotation.z);

            Quaternion tmpQuat = Quaternion.Euler(rotationOnlyY.x - 90, tmp, rotationOnlyY.z);//apply clamp

            transform.rotation = Quaternion.Lerp(transform.rotation, tmpQuat, speed * Time.deltaTime);//controls cannons movement speed to match with crosshair direction
        }
    }

    public void Shoot()
    {
        if (shootManager.canShoot)
        {
            shootManager.resetShot();

            explosion?.SetActive(false); //Activate particle system
            explosion?.SetActive(true);
            explosionSound.Play(); //Play explosion sound

            Vector3 rayOrigin = ArCamera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 0));
            //Ray ray = new Ray(ballSpawnPoints[0].position, ballSpawnPoints[0].forward);
            RaycastHit hit;

            //if (Physics.Raycast(rayorigin, ArCamera.transform.forward, out hit, float.MaxValue))
            //if (Physics.Raycast(ballSpawnPoints[0].position, ballSpawnPoints[0].forward, out hit, float.MaxValue)) //check for collision
            if (Physics.Raycast(rayOrigin, ArCamera.transform.forward, out hit, float.MaxValue))
            {
                print(hit.transform.gameObject.name);
                PhysicalBoat boat = hit.collider.transform.GetComponent<PhysicalBoat>();
                boat?.Explode();

            }
        }
        
    }
}
}