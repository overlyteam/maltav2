﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Overly.FightGamesScripts
{
    public enum Games { DeathFromAbove, Battleships }
    public enum ArModes { CoreKit, Geo, ARFoundation, BattleshipsWithButtons }

    public class GameLoader : MonoBehaviour
    {
        public Games game = Games.Battleships;
        public ArModes mode = ArModes.Geo;
        public int xrCoreScene = 1;
        public int geoArScene = 2;
        public int battleShipsScene = 3;
        public int deathFromAboveScene = 4;
        public int ARFoundationScene = 5;
        public int BattleshipsWithButtons = 6;
        public Text outputLabel;

        private AsyncOperation asyncOperation;
        private int stage = 0;



        /*************************** INNER LOGIC ******************************/

        // Start is called before the first frame update
        void Start()
        {
            // if app uses ArCore or ArKit - game should be loaded first
            // if app uses GPS - "GeoAR" scene should be loaded first
            if (mode == ArModes.CoreKit)
                asyncOperation = LoadGame();
            else if (mode == ArModes.Geo)
                asyncOperation = SceneManager.LoadSceneAsync(geoArScene, LoadSceneMode.Additive);
            else if (mode == ArModes.ARFoundation)
                asyncOperation = SceneManager.LoadSceneAsync(ARFoundationScene, LoadSceneMode.Additive);
            else
                asyncOperation = SceneManager.LoadSceneAsync(BattleshipsWithButtons, LoadSceneMode.Additive);
        }

        // Update is called once per frame
        void Update()
        {
            if (asyncOperation == null)
                return;

            if (asyncOperation.isDone) //if previously created async operation is finedshed
            {
                switch (stage)
                {
                    case 0: //if first operation is done
                        if (mode == ArModes.CoreKit)
                            asyncOperation = SceneManager.LoadSceneAsync(xrCoreScene, LoadSceneMode.Additive);
                        else
                            asyncOperation = LoadGame();
                        break;
                    case 1: // if second operation is done
                        SceneManager.UnloadSceneAsync(0); asyncOperation = null; // unload scene
                        break;
                }

                stage++;
            }
            else
            {
                switch (stage)
                {
                    case 0: outputLabel.text = string.Format("Loading of game {0}", (int)asyncOperation.progress * 100); break;
                    case 1: string.Format("Loading of ArCore services {0}", (int)asyncOperation.progress * 100); break;
                }
            }
        }

        private AsyncOperation LoadGame()
        {
            if (game == Games.Battleships)
                return SceneManager.LoadSceneAsync(battleShipsScene, LoadSceneMode.Additive);
            else
                return SceneManager.LoadSceneAsync(deathFromAboveScene, LoadSceneMode.Additive);
        }



    }//end of class
}