﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

namespace Overly.FightGamesScripts
{
public class AR_recognized : MonoBehaviour
{
    public GameObject arObject;
    public GameObject[] objectsToHide;

    public ARTrackedImageManager ARTrackedImageManager;

    private ARTrackedImage trackedImage;
    private bool canTurnOff;

    void Start()
    {
        ARTrackedImageManager.trackedImagesChanged += OnTrackedImagesChanged;
        canTurnOff = false;
    }

    
    void Update()
    {
        if(arObject.activeSelf == true)
        {
            if (canTurnOff)
            {
                turnOff();
            }
        }
    }

    
    public void OnTrackedImagesChanged(ARTrackedImagesChangedEventArgs eventArgs)//scanning marker
    {
        trackedImage = null;//marker itself?

        // Check the new tracked images
        for (int i = 0; i < eventArgs.added.Count; i++)
        {
            trackedImage = eventArgs.added[i];
            //cannon.SetActive(true);
        }


        for (int i = 0; i < eventArgs.updated.Count; i++)
        {
            trackedImage = eventArgs.updated[i];

            if (trackedImage.trackingState == TrackingState.Tracking)
            {
                canTurnOff = true;
            }
            else
            {
                //cannon.SetActive(false);
            }
        }

        for (int i = 0; i < eventArgs.removed.Count; i++)
        {
            if (trackedImage != null)
            {
                if (trackedImage.trackingState == TrackingState.None)
                {
                    //cannon.SetActive(false);
                }
            }
        }
    }

    public void turnOff()
    {
        foreach (var obj in objectsToHide)
        {
            obj.SetActive(false);
            print("name: " + obj.name);
        }

    }

}
}