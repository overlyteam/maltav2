﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

namespace Overly.Test
{
    public class MenuScript : MonoBehaviour
    {
        public void LoadScene(string levelName)
        {
            GameSettings.instance.SetReturnLevel(SceneManager.GetActiveScene().name);
            SceneManager.LoadScene(levelName);
        }

        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
    }
}