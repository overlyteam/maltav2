﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Overly.ChapelGpsScripts
{
    public class PinButton : MonoBehaviour
    {
        public void OnButtonPressed(string tag)
        {
            Debug.Log("Enabling object with Tag: " + tag);
            GameObject taggedObject = GameObject.FindGameObjectWithTag(tag);
            if (taggedObject != null)
            {
                taggedObject.SetActive(true);
            }
        }
    }
}