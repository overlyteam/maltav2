﻿using UnityEngine;
using UnityEngine.XR.ARFoundation;

namespace Overly.ChapelGpsScripts
{
[RequireComponent(typeof(Light))]
public class ARLightAmbient : MonoBehaviour
{
    private Light l;

    void Start()
    {
        l = GetComponent<Light>();
        FindObjectOfType<ARCameraManager>().frameReceived += OnCameraFrameReceived;
    }

    void OnCameraFrameReceived(ARCameraFrameEventArgs eventArgs)
    {
        l.intensity = eventArgs.lightEstimation.averageBrightness.Value;
        l.colorTemperature = eventArgs.lightEstimation.averageColorTemperature.Value;
    }

    void OnDisable()
    {
        FindObjectOfType<ARCameraManager>().frameReceived -= OnCameraFrameReceived;
    }
}
}