﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Overly.ChapelGpsScripts
{
public class DeviceCamera : MonoBehaviour
{
    [SerializeField]
    RawImage webCamRenderer;

    void Start()
    {
        WebCamTexture webcamTexture = new WebCamTexture();
        webCamRenderer.material.mainTexture = webcamTexture;
        webcamTexture.Play();
    }
}
}