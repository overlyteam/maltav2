﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

namespace Overly.ChapelGpsScripts
{
public class placeObject : MonoBehaviour
{
    public ARCameraManager arcameramanager;
    public TextMeshProUGUI lightText;
    public Light currentLight;

    public GameObject objectToPlace;
    public GameObject arCamera;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.touchCount > 0 || Input.GetMouseButtonDown(0))
        {
            objectToPlace.transform.position = arCamera.transform.position;
        }


    }

    private void OnEnable()
    {
        arcameramanager.frameReceived += FrameUpdated;
    }

    private void OnDisable()
    {
        arcameramanager.frameReceived -= FrameUpdated;
    }

    private void FrameUpdated(ARCameraFrameEventArgs args)
    {
        if (args.lightEstimation.averageBrightness.HasValue)
        {
            
            currentLight.intensity = args.lightEstimation.averageBrightness.Value * 5f;
            

            lightText.text = "Intensity: " + args.lightEstimation.averageBrightness.Value + " | " + currentLight.intensity;
        }

        if (args.lightEstimation.averageColorTemperature.HasValue)
        {
            currentLight.colorTemperature = args.lightEstimation.averageColorTemperature.Value * 5f;
        }

        if (args.lightEstimation.colorCorrection.HasValue)
        {
            currentLight.color = args.lightEstimation.colorCorrection.Value;
        }
    }
}
}