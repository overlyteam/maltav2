﻿using ARLocation;
using System;
using UnityEngine;
using UnityEngine.UI;

public class uiController : MonoBehaviour
{
    [SerializeField] InputField latitudeInput;
    [SerializeField] InputField longitudeInput;
    [SerializeField] InputField scaleInput;
    private ARLocationProvider locationProvider;

    [SerializeField]
    Text currentLocationText;

    [SerializeField]
    Text debugText;

    private Guid entryID = Guid.Empty;
    public ARLocationManager manager;

    public GameObject arCamera;
    public Text newGPS_text;
    //public ARLocationPlaceAtLocation objLocation;
    public PlaceAtLocation objLocation;
    public GameObject obj;
    public GameObject church;

    float latitude;
    float longitude;
    float altitude;
    float scale;
    Vector3 location;

    double objlatitude;
    double objlongitude;
    float objscale;

    bool hasInvoked = false;
    Location newlocation = null;
    // Start is called before the first frame update
    void Start()
    {
        /*
        if (PlayerPrefs.HasKey("latitude"))
            latitude = PlayerPrefs.GetFloat("latitude");
        if (PlayerPrefs.HasKey("longitude"))
            longitude = PlayerPrefs.GetFloat("longitude");
        */

        //objLocation = obj.GetComponent<ARLocationPlaceAtLocation>();
        //latitude = objLocation.location.latitude;
        //longitude = objLocation.location.longitude;


        objlatitude = PlayerPrefs.GetFloat("latitude");
        objlongitude = PlayerPrefs.GetFloat("longitude");
        objscale = PlayerPrefs.GetFloat("scale");
        print(objlatitude + "| " + objlongitude);

        latitudeInput.text = objlatitude.ToString();
        longitudeInput.text = objlongitude.ToString();
        scaleInput.text = objscale.ToString();

        locationProvider = ARLocationProvider.Instance;
        manager = ARLocationManager.Instance;
        //newGPS_text.text = string.Format(newGPS_text.text, objLocation.location.latitude, objLocation.location.longitude);
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.location.status == LocationServiceStatus.Running)//if gps is running
        {
            latitude = Input.location.lastData.latitude;
            altitude = Input.location.lastData.altitude;
            longitude = Input.location.lastData.longitude;

            location = new Vector3(latitude, altitude, longitude);
        }

        currentLocationText.text = "Current GPS: " + location.x + ", " + location.y + ", " + location.z;

        newGPS_text.text = "CHURCH GPS COORD: " + objLocation.Location;

        if(!hasInvoked)
            InvokeRepeating("updateDistance", 2f, 2f);
    }

    public void updateDistance()
    {
        float unitz = Vector3.Distance(obj.transform.position, arCamera.transform.position);
        debugText.text = "Distance: " + unitz + "m";
        hasInvoked = true;

        print("updated distance...");
    }
    public void changeObjectLocationButton()//change object location from input
    {
        objlatitude = double.Parse(latitudeInput.text);
        objlongitude = double.Parse(longitudeInput.text);
        objscale = float.Parse(scaleInput.text);

        PlaceAtLocation.PlaceAtOptions opt = new PlaceAtLocation.PlaceAtOptions();//create new location data
            opt.HideObjectUntilItIsPlaced = false;
            opt.MaxNumberOfLocationUpdates = 0;
            opt.MovementSmoothing = 0.15f;
            opt.UseMovingAverage = false;
        
        newlocation = new Location(objlatitude, objlongitude, 1f);

        PlayerPrefs.SetFloat("latitude", (float)objlatitude);
        PlayerPrefs.SetFloat("longitude", (float)objlongitude);
        PlayerPrefs.SetFloat("scale", objscale);
        PlayerPrefs.Save();

        changeGPS(newlocation);//update location here
        
        print("new location is " + objLocation.Location);

        obj.transform.localScale = new Vector3(objscale, objscale, objscale);
        print("changed location!");
    }

    public PlaceAtLocation changeGPS(Location location)//use plugin to change location of object
    {
        ARLocationManager.Instance.Restart();

        var placeAt = obj.GetComponent<PlaceAtLocation>();
        //placeAt.LocationOptions.LocationInput.Location = location.Clone();
        placeAt.Location = location;
        print("***LOCATION = " + placeAt.Location);

        return placeAt;
    }

    public void changeToCurrentPosButton()//change object to current position
    {
        Location newlocation = new Location(locationProvider.CurrentLocation.latitude, locationProvider.CurrentLocation.latitude, 1);

        latitudeInput.text = locationProvider.CurrentLocation.latitude.ToString();
        longitudeInput.text = locationProvider.CurrentLocation.longitude.ToString();

        objlatitude = double.Parse(latitudeInput.text);
        objlongitude = double.Parse(longitudeInput.text);

        PlayerPrefs.SetFloat("latitude", (float)objlatitude);
        PlayerPrefs.SetFloat("longitude", (float)objlongitude);

        print("current location: " + objlatitude + "| " + objlongitude);
    }

    public void gps_coordinates_for_buttons(int buttonID)//buttons for different gps locations
    {
        switch (buttonID)
        {
            case 1:
                print("Brivibas ielas krustojums");
                latitudeInput.text = "56.96202187961434f";
                longitudeInput.text = "24.134068585506427f";
                return;
            case 2:
                print("Sporta Pils krustojums");
                latitudeInput.text = "56.96054197584224f";
                longitudeInput.text = "24.13648257361983f";
                return;
            case 3:
                print("Parkings");
                latitudeInput.text = "56.96124391381247f";
                longitudeInput.text = "24.134776688686358f";
                return;
        }
    }

    void OnApplicationQuit()
    {
        Input.location.Stop();
    }
}