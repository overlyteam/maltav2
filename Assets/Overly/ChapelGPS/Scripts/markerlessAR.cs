﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Overly.ChapelGpsScripts
{
public class markerlessAR : MonoBehaviour
{
    public static markerlessAR Instance { set; get; }

    [SerializeField]
    Text debugText;
    [SerializeField]
    Text currentLocationText;
    [SerializeField]
    InputField latitudeInput;
    [SerializeField]
    InputField longitudeInput;
    [SerializeField]
    InputField altitudeInput;
    [SerializeField]
    InputField scaleInput;

    float latitude;
    float longitude;
    float altitude;
    float scale;

    //gyro
    private Gyroscope gyro;
    private GameObject cameraContainer;
    private Quaternion rotation;

    //cam
    private WebCamTexture cam;
    public RawImage background;
    public AspectRatioFitter fit;

    private bool arReady = false;

    bool canUpdate = false;

    bool altitudePlus = false, altitudeMinus = false;
    bool scalePlus = false, scaleMinus = false;

    public Vector3 location;
    public GameObject church;

    void Start()
    {

        //check if we support both services
        //gyro

        if (!SystemInfo.supportsGyroscope)
        {
            Debug.Log("Gyro not supported");
            return;
        }

        //back camera
        for (int i = 0; i < WebCamTexture.devices.Length; i++)
        {
            if (!WebCamTexture.devices[i].isFrontFacing)
            {
                cam = new WebCamTexture(WebCamTexture.devices[i].name, Screen.width, Screen.height);
                Debug.Log("cam texture: " + cam.name);
                Debug.Log("assigned camera - " + WebCamTexture.devices[i].name);
                break;
            }
        }

        // if we didnt find back camera
        if (cam == null)
        {
            Debug.Log("Unable to find back camera");
            return;
        }

        //Both servies are supported - lets enable them!
        cameraContainer = new GameObject("Camera Container");
        cameraContainer.transform.position = transform.position;
        transform.SetParent(cameraContainer.transform);

        gyro = Input.gyro;
        gyro.enabled = true;

        cameraContainer.transform.rotation = Quaternion.Euler(90f, 0, 0);
        rotation = new Quaternion(0, 0, 1, 0);

        cam.Play();
        background.texture = cam;
        Debug.Log("cam texture: " + cam);

        arReady = true;

        Instance = this;
        DontDestroyOnLoad(gameObject);

        //startLocationService
        StartCoroutine(StartLocationService());

        Debug.Log("START ENDED");
    }

    IEnumerator StartLocationService()
    {
        Debug.Log("yee yee");
        // First, check if user has location service enabled
        if (!Input.location.isEnabledByUser)
        {
            Debug.Log("Gyro not supported");
            yield break;
        }

        // Start GPS tracking
        Debug.Log("Starting GPS tracking");
        Input.location.Start(5, 1);

        // Wait until service initializes
        int maxWait = 20;//20
        while (Input.location.status == LocationServiceStatus.Initializing && maxWait > 0)
        {
            yield return new WaitForSeconds(1);
            maxWait--;
            Debug.Log("Initializing Location Services.." + maxWait);
        }

        // Service didn't initialize in 20 seconds
        if (maxWait < 1)
        {
            Debug.LogError("Timed out");
            yield break;
        }

        // Connection has failed
        if (Input.location.status == LocationServiceStatus.Failed)
        {
            Debug.LogError("Unable to determine device location");
            yield break;
        }

        if (Input.location.status == LocationServiceStatus.Running)
        {


            if (PlayerPrefs.HasKey("latitude"))
                latitude = PlayerPrefs.GetFloat("latitude");
            if (PlayerPrefs.HasKey("longitude"))
                longitude = PlayerPrefs.GetFloat("longitude");
            if (PlayerPrefs.HasKey("altitude"))
                altitude = PlayerPrefs.GetFloat("altitude");
            if (PlayerPrefs.HasKey("scale"))
                scale = PlayerPrefs.GetFloat("scale");


            latitudeInput.text = latitude.ToString();
            longitudeInput.text = longitude.ToString();
            altitudeInput.text = altitude.ToString();
            scaleInput.text = scale.ToString();

            //====Update GPS COORDINATES====

            Debug.Log("updating coord..");
            latitude = Input.location.lastData.latitude;//x
            longitude = Input.location.lastData.longitude;//z
            altitude = Input.location.lastData.altitude;//y
            //Location
            location = new Vector3(latitude, altitude, longitude);
            Debug.Log("location? " + location);

            church.transform.localScale = Vector3.one * scale;

            Debug.Log("Location: " + (float)Input.location.lastData.latitude + "/ " + Input.location.lastData.longitude + "/ " + Input.location.lastData.altitude);
            canUpdate = true;
        }

        //Input.location.Stop();
    }

    void Update()
    {

        if (arReady)
        {
            //====Update Camera=====
            float ratio = (float)cam.width / (float)cam.height;
            fit.aspectRatio = ratio;

            float scaleY = cam.videoVerticallyMirrored ? -1f : 1f;

            background.rectTransform.localScale = new Vector3(1f, scaleY, 1f);

            int orient = -cam.videoRotationAngle;
            background.rectTransform.localEulerAngles = new Vector3(0, 0, orient);

            //=====Update Gyro=====
            transform.localRotation = gyro.attitude * rotation;

            //Bearing
            float bearing = Bearing(location, new Vector2(latitude, longitude));

            //Distance from user
            float unitz = CalculateDistanceMeters(location, new Vector2(latitude, longitude));
            debugText.text = "Distance: " + unitz + "m";
            //transform.position = new Vector3(Mathf.Sin(ToRadians(bearing)) * unitz, altitude - location.z, Mathf.Cos(ToRadians(bearing)) * unitz);
        }

        if (Time.frameCount % 5 == 0)
        {
            if (altitudePlus)
            {
                altitude++;
                altitudeInput.text = altitude.ToString();
            }
            else if (altitudeMinus)
            {
                altitude--;
                altitudeInput.text = altitude.ToString();
            }

            if (scalePlus)
            {
                scale++;
                scaleInput.text = scale.ToString();
                church.transform.localScale = Vector3.one * scale;
            }
            else if (scaleMinus)
            {
                if ((scale - 1) > 0)
                {
                    scale--;
                }
                else
                {
                    scale = 0.1f;
                }

                scaleInput.text = scale.ToString();
                church.transform.localScale = Vector3.one * scale;
            }
        }

        if (Input.location.status == LocationServiceStatus.Running)
        {
            latitude = Input.location.lastData.latitude;//x
            longitude = Input.location.lastData.longitude;//z
            altitude = Input.location.lastData.altitude;//y
                                                        //Location
            location = new Vector3(latitude, altitude, longitude);
            Debug.Log("location update? " + location);

            currentLocationText.text = string.Format(currentLocationText.text, location.x, location.z, location.y);
        }
    }


    #region GPS Calculations

    /// <summary>
    /// Maths to calculate distance from user to object
    /// </summary>
    /// <param name="loc">Current location</param>
    /// <param name="target">Target location</param>
    /// <returns>Distance (meters)</returns>
    float CalculateDistanceMeters(Vector3 loc, Vector2 target)
    {
        float R = 6371000; // metres
        float φ1 = ToRadians(loc.x);
        float φ2 = ToRadians(target.x);
        float Δφ = ToRadians(target.x - loc.x);
        float Δλ = ToRadians(target.y - loc.y);

        float a = Mathf.Sin(Δφ / 2) * Mathf.Sin(Δφ / 2) + Mathf.Cos(φ1) * Mathf.Cos(φ2) * Mathf.Sin(Δλ / 2) * Mathf.Sin(Δλ / 2);
        float c = 2 * Mathf.Atan2(Mathf.Sqrt(a), Mathf.Sqrt(1 - a));
        //float d = Mathf.Acos(Mathf.Sin(φ1) * Mathf.Sin(φ2) + Mathf.Cos(φ1) * Mathf.Cos(φ2) * Mathf.Cos(Δλ)) * R;
        float d = R * c;
        return d;
    }

    /// <summary>
    /// Maths to calculate bearing between current and target location
    /// </summary>
    /// <param name="loc">Current location</param>
    /// <param name="target">Target location</param>
    /// <returns>Bearing (degrees)</returns>
    float Bearing(Vector3 loc, Vector2 target)
    {
        float φ1 = ToRadians(loc.x);
        float φ2 = ToRadians(target.x);
        float λ1 = ToRadians(loc.y);
        float λ2 = ToRadians(target.y);
        float Δλ = ToRadians(target.y - loc.y);
        //float y = Mathf.Sin(λ2 - λ1) * Mathf.Cos(φ2);
        //float x = Mathf.Cos(φ1) * Mathf.Sin(φ2) -
        //        Mathf.Sin(φ1) * Mathf.Cos(φ2) * Mathf.Cos(λ2 - λ1);
        //float brng = ToDegrees(Mathf.Atan2(y, x));
        //brng = (brng + 360) % 360;
        //return brng;

        float Δψ = Mathf.Log(Mathf.Tan(Mathf.PI / 4 + φ2 / 2) / Mathf.Tan(Mathf.PI / 4 + φ1 / 2));

        // if dLon over 180° take shorter rhumb line across the anti-meridian:
        if (Mathf.Abs(Δλ) > Mathf.PI) Δλ = Δλ > 0 ? -(2 * Mathf.PI - Δλ) : (2 * Mathf.PI + Δλ);

        float brng = ToDegrees(Mathf.Atan2(Δλ, Δψ));
        return brng;
    }

    /// <summary>
    /// Changes degrees to radians. Was too lazy to replace with the Mathf equivalent when I found it, so this will do
    /// </summary>
    /// <param name="degrees">Degrees</param>
    /// <returns>Radians</returns>
    float ToRadians(float degrees)
    {
        return Mathf.Deg2Rad * degrees;
    }

    /// <summary>
    /// Changes radians to degrees. Was too lazy to replace with the Mathf equivalent when I found it, so this will do
    /// </summary>
    /// <param name="radians">Radians</param>
    /// <returns>Degrees</returns>
    float ToDegrees(float radians)
    {
        return Mathf.Rad2Deg * radians;
    }

    #endregion

    public void ChangeObjectLocation()
    {
        latitude = float.Parse(latitudeInput.text);
        longitude = float.Parse(longitudeInput.text);
        altitude = float.Parse(altitudeInput.text);
        scale = float.Parse(scaleInput.text);

        PlayerPrefs.SetFloat("latitude", latitude);
        PlayerPrefs.SetFloat("longitude", longitude);
        PlayerPrefs.SetFloat("altitude", altitude);
        PlayerPrefs.SetFloat("scale", scale);

        transform.localScale = Vector3.one * scale;

        church.transform.position = new Vector3(latitude, altitude, longitude);
        Debug.Log("changed position to: " + latitude + ", " + longitude + ", " + altitude);
        Debug.Log("pos|locPos: " + transform.position + " | " + transform.localPosition);
    }

    public void ChangeObjectLocationToCurrent()
    {

        latitude = location.x;
        longitude = location.z;
        altitude = location.y;

        latitudeInput.text = latitude.ToString();
        longitudeInput.text = longitude.ToString();
        altitudeInput.text = altitude.ToString();

        PlayerPrefs.SetFloat("latitude", latitude);
        PlayerPrefs.SetFloat("longitude", longitude);
        PlayerPrefs.SetFloat("altitude", altitude);
    }

    public void AltitudePlusPressed(bool value)
    {
        if (altitudeMinus)
        {
            altitudePlus = false;
            return;
        }
        altitudePlus = value;
    }

    public void AltitudeMinusPressed(bool value)
    {
        if (altitudePlus)
        {
            altitudeMinus = false;
            return;
        }
        altitudeMinus = value;
    }

    public void ScalePlusPressed(bool value)
    {
        if (scaleMinus)
        {
            scalePlus = false;
            return;
        }
        scalePlus = value;
    }

    public void ScaleMinusPressed(bool value)
    {
        if (scalePlus)
        {
            scaleMinus = false;
            return;
        }
        scaleMinus = value;
    }

    void OnApplicationQuit()
    {
        Input.location.Stop();
    }
}
}