﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Android;
using UnityEngine.XR.ARFoundation;

namespace Overly.ChapelGpsScripts
{
public class GPS_Object : MonoBehaviour
{

    public static GPS_Object Instance { set; get; }

    [SerializeField]
    Text debugText;
    [SerializeField]
    Text currentLocationText;
    [SerializeField]
    InputField latitudeInput;
    [SerializeField]
    InputField longitudeInput;
    [SerializeField]
    InputField altitudeInput;
    [SerializeField]
    InputField scaleInput;
    public GameObject obj;

    float latitude;
    float longitude;
    float altitude;
    float scale;

    float objLat;
    float objLong;
    float objAlt;
    float objScale;

    //gyro
    private Gyroscope gyro;
    private GameObject cameraContainer;
    private Quaternion rotation;

    bool canUpdate = false;

    bool altitudePlus = false, altitudeMinus = false;
    bool scalePlus = false, scaleMinus = false;
    static bool gpsStarted;

    Vector3 location;
    Vector3 objLocation;

    public ARCameraBackground arbackground;

    private void Start()
    {
        //StartCoroutine(StartLocationService());

        Instance = this;
        DontDestroyOnLoad(gameObject);

        //gyro
        if (!SystemInfo.supportsGyroscope)
        {
            Debug.Log("Gyro not supported");
            return;
        }

        Input.compass.enabled = true;

        cameraContainer = new GameObject("Camera Container");//PLACE CAMERA UNDER NEW EMPTY GAMEOBJECT
        cameraContainer.transform.position = transform.position;
        transform.SetParent(cameraContainer.transform);

        gyro = Input.gyro;
        gyro.enabled = true;

        cameraContainer.transform.rotation = Quaternion.Euler(90f, 0, 0);
        rotation = new Quaternion(0, 0, 1, 0);

        //put object in coord
        //objLocation = new Vector3(56.961410f, 0f, 24.136068f);

        gpsStarted = false;
    }

    public static void StopGPS()
    {
        if (Input.location.isEnabledByUser)
        {
            gpsStarted = false;
            Input.location.Stop();
        }
    }

    void Update()
    {
        // wait till start coroutine ends 
        //if (!canUpdate)
        //return;

        if (Input.location.status == LocationServiceStatus.Running)//if gps is running/enabled
        {
            latitude = Input.location.lastData.latitude;//get gps data
            altitude = Input.location.lastData.altitude;
            longitude = Input.location.lastData.longitude;

            location = new Vector3(latitude, altitude, longitude);

            Debug.Log("Updating GPS coord... " + location);
        }

        //Debug.Log("current gps pos: [" + location.x + "; " + location.y + "; " + location.z + "]");
        currentLocationText.text = "Current GPS: " + location.x + ", " + location.y + ", " + location.z;

        //Bearing
        float bearing = Bearing(location, new Vector2(latitude, longitude));

        //Distance from user
        //float unitz = CalculateDistanceMeters(location, new Vector2(latitude, longitude));
        float unitz = Vector3.Distance(obj.transform.position, transform.position);
        debugText.text = "Distance: " + unitz + "m";
        //transform.position = new Vector3(Mathf.Sin(ToRadians(bearing)) * unitz, altitude - location.z, Mathf.Cos(ToRadians(bearing)) * unitz);

        //currentLocationText.text = string.Format(currentLocationText.text, location.x, location.y, location.z);
        
        /*
        if (Time.frameCount % 5 == 0)
        {
            if (altitudePlus)
            {
                altitude++;
                altitudeInput.text = altitude.ToString();
            }
            else if (altitudeMinus)
            {
                altitude--;
                altitudeInput.text = altitude.ToString();
            }

            if (scalePlus)
            {
                scale++;
                scaleInput.text = scale.ToString();
                obj.transform.localScale = Vector3.one * scale;
            }
            else if (scaleMinus)
            {
                if ((scale - 1) > 0)
                {
                    scale--;
                }
                else
                {
                    scale = 0.1f;
                }

                scaleInput.text = scale.ToString();
                obj.transform.localScale = Vector3.one * scale;
            }
        }
        */
    }

    #region GPS Calculations

    /// <summary>
    /// Maths to calculate distance from user to object
    /// </summary>
    /// <param name="loc">Current location</param>
    /// <param name="target">Target location</param>
    /// <returns>Distance (meters)</returns>
    float CalculateDistanceMeters(Vector3 loc, Vector2 target)
    {
        float R = 6371000; // metres
        float φ1 = ToRadians(loc.x);
        float φ2 = ToRadians(target.x);
        float Δφ = ToRadians(target.x - loc.x);
        float Δλ = ToRadians(target.y - loc.y);

        float a = Mathf.Sin(Δφ / 2) * Mathf.Sin(Δφ / 2) + Mathf.Cos(φ1) * Mathf.Cos(φ2) * Mathf.Sin(Δλ / 2) * Mathf.Sin(Δλ / 2);
        float c = 2 * Mathf.Atan2(Mathf.Sqrt(a), Mathf.Sqrt(1 - a));
        //float d = Mathf.Acos(Mathf.Sin(φ1) * Mathf.Sin(φ2) + Mathf.Cos(φ1) * Mathf.Cos(φ2) * Mathf.Cos(Δλ)) * R;
        float d = R * c;
        return d;
    }

    /// <summary>
    /// Maths to calculate bearing between current and target location
    /// </summary>
    /// <param name="loc">Current location</param>
    /// <param name="target">Target location</param>
    /// <returns>Bearing (degrees)</returns>
    float Bearing(Vector3 loc, Vector2 target)
    {
        float φ1 = ToRadians(loc.x);
        float φ2 = ToRadians(target.x);
        float λ1 = ToRadians(loc.y);
        float λ2 = ToRadians(target.y);
        float Δλ = ToRadians(target.y - loc.y);
        //float y = Mathf.Sin(λ2 - λ1) * Mathf.Cos(φ2);
        //float x = Mathf.Cos(φ1) * Mathf.Sin(φ2) -
        //        Mathf.Sin(φ1) * Mathf.Cos(φ2) * Mathf.Cos(λ2 - λ1);
        //float brng = ToDegrees(Mathf.Atan2(y, x));
        //brng = (brng + 360) % 360;
        //return brng;

        float Δψ = Mathf.Log(Mathf.Tan(Mathf.PI / 4 + φ2 / 2) / Mathf.Tan(Mathf.PI / 4 + φ1 / 2));

        // if dLon over 180° take shorter rhumb line across the anti-meridian:
        if (Mathf.Abs(Δλ) > Mathf.PI) Δλ = Δλ > 0 ? -(2 * Mathf.PI - Δλ) : (2 * Mathf.PI + Δλ);

        float brng = ToDegrees(Mathf.Atan2(Δλ, Δψ));
        return brng;
    }

    /// <summary>
    /// Changes degrees to radians. Was too lazy to replace with the Mathf equivalent when I found it, so this will do
    /// </summary>
    /// <param name="degrees">Degrees</param>
    /// <returns>Radians</returns>
    float ToRadians(float degrees)
    {
        return Mathf.Deg2Rad * degrees;
    }

    /// <summary>
    /// Changes radians to degrees. Was too lazy to replace with the Mathf equivalent when I found it, so this will do
    /// </summary>
    /// <param name="radians">Radians</param>
    /// <returns>Degrees</returns>
    float ToDegrees(float radians)
    {
        return Mathf.Rad2Deg * radians;
    }

    #endregion

    public void ChangeObjectLocation()//change object location to input values = values from ui input field
    {
        Debug.Log("changed loc from " + obj.transform.position + " ===");
        objLat    = float.Parse(latitudeInput.text);//get values from text
        objLong   = float.Parse(longitudeInput.text);
        objAlt    = float.Parse(altitudeInput.text);
        objScale  = float.Parse(scaleInput.text);

        objLocation = new Vector3(objLong, objAlt, objLat);

        PlayerPrefs.SetFloat("latitude", latitude);//save values
        PlayerPrefs.SetFloat("longitude", longitude);
        PlayerPrefs.SetFloat("altitude", altitude);
        PlayerPrefs.SetFloat("scale", scale);

        obj.transform.localScale = Vector3.one * objScale;
    }

    public void ChangeObjectLocationToCurrent()//change object position to users current gps position
    {
        latitude    = location.x;
        longitude   = location.z;
        altitude    = location.y;

        latitudeInput.text = latitude.ToString();
        longitudeInput.text = longitude.ToString();
        altitudeInput.text = altitude.ToString();

        PlayerPrefs.SetFloat("latitude", latitude);
        PlayerPrefs.SetFloat("longitude", longitude);
        PlayerPrefs.SetFloat("altitude", altitude);
    }

    float distance(float lat1, float lon1, float lat2, float lon2)
    {  // generally used geo measurement function
        var R = 6378.137; // Radius of earth in KM
        var dLat = (lat2 - lat1) * Mathf.PI / 180;
        var dLon = (lon2 - lon1) * Mathf.PI / 180;
        var a = Mathf.Sin(dLat / 2) * Mathf.Sin(dLat / 2) +
            Mathf.Cos(lat1 * Mathf.PI / 180) * Mathf.Cos(lat2 * Mathf.PI / 180) *
            Mathf.Sin(dLon / 2) * Mathf.Sin(dLon / 2);
        var c = 2 * Mathf.Atan2(Mathf.Sqrt(a), Mathf.Sqrt(1 - a));
        var d = R * c;
        return (float)(d * 1000); // meters
    }

    public void AltitudePlusPressed(bool value)
    {
        if (altitudeMinus)
        {
            altitudePlus = false;
            return;
        }
        altitudePlus = value;
    }

    public void AltitudeMinusPressed(bool value)
    {
        if (altitudePlus)
        {
            altitudeMinus = false;
            return;
        }
        altitudeMinus = value;
    }

    public void ScalePlusPressed(bool value)
    {
        if (scaleMinus)
        {
            scalePlus = false;
            return;
        }
        scalePlus = value;
    }

    public void ScaleMinusPressed(bool value)
    {
        if (scalePlus)
        {
            scaleMinus = false;
            return;
        }
        scaleMinus = value;
    }

    void OnApplicationQuit()
    {
        StopGPS();

        Input.location.Stop();    
    }
}
}