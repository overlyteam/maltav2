﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Overly.ChapelGpsScripts
{
public class arrowNavigation : MonoBehaviour
{
    public GameObject objectToLookAt;
    public GameObject arCamera;
    public GameObject arrow;
    public Transform arrowMimicPos;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()//update arrows rotation to look at camera
    {
        Vector3 editMimicPos = new Vector3(arrowMimicPos.position.x, arrowMimicPos.position.y, arrowMimicPos.position.z);
        transform.position = Vector3.Lerp(transform.position, editMimicPos, Time.time * 5f);

        arrow.transform.LookAt(objectToLookAt.transform);
    }
}
}