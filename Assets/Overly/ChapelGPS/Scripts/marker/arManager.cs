﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using TMPro;
using UnityEngine.UI;
using Overly.ChapelGpsScripts;

public class arManager : MonoBehaviour
{
    public GameObject arCamera;
    public ARSessionOrigin arSessionOrigin;
    public ARTrackedImageManager imageManager;
    public ARReferencePointManager refPointManager;
    ARTrackedImage trackedImage;

    private bool hasScanned;
    private bool executed;

    public GameObject tutorial;

    //FOR SLIDERS
    public float xValueChange;
    public float yValueChange;
    public float zValueChange;
    public float sizeValueChange;

    public TextMeshProUGUI xValue;
    public TextMeshProUGUI yValue;
    public TextMeshProUGUI zValue;
    public TextMeshProUGUI sizeValue;

    public Slider xSlider;
    public Slider ySlider;
    public Slider zSlider;
    public Slider sizeSlider;

    public TextMeshProUGUI xRotationText;
    public TextMeshProUGUI yRotationText;
    public TextMeshProUGUI zRotationText;

    private float currRotX;
    private float currRotY;
    private float currRotZ;
    //public MeshRenderer skyPlane_material;
    public ARPointCloudManager aRPointCloudManager;

    private float multiplier = 1;

    ARReferencePoint referencePointChapel;
    ARReferencePoint referencePointMarker;

    //private Vector3 churchPos;

    void Start()
    {
        imageManager.trackedImagesChanged += OnTrackedImagesChanged;//subsribe to action, that scans images
        hasScanned = false;
        executed = false;

        if (PlayerPrefs.HasKey("x"))//Getting saved values from the last game
            xValueChange = PlayerPrefs.GetFloat("x", 0f);
        if (PlayerPrefs.HasKey("y"))
            yValueChange = PlayerPrefs.GetFloat("y", 0f);
        if (PlayerPrefs.HasKey("z"))
            zValueChange = PlayerPrefs.GetFloat("z", 0f);
        if (PlayerPrefs.HasKey("size"))
            sizeValueChange = PlayerPrefs.GetFloat("size", 0.2f);

        //SLIDERS
        xSlider.value = xValueChange;//apply saved values to sliders
        ySlider.value = yValueChange;
        zSlider.value = zValueChange;
        sizeSlider.value = sizeValueChange;

        spawnPoint_X_change(xValueChange);//change to saved values
        spawnPoint_Y_change(yValueChange);
        spawnPoint_Z_change(zValueChange);
        sizeChange(sizeValueChange);

        xValue.text = "Right / Left: " + xValueChange;
        yValue.text = "Up / Down: " + yValueChange;
        zValue.text = "Further / Closer:: " + zValueChange;
        sizeValue.text = "Size: " + sizeValueChange;


        if (PlayerPrefs.HasKey("xRot"))//saved church rotation 
            currRotX = PlayerPrefs.GetFloat("xRot");
        if (PlayerPrefs.HasKey("yRot"))
            currRotY = PlayerPrefs.GetFloat("yRot");
        if (PlayerPrefs.HasKey("zRot"))
            currRotZ = PlayerPrefs.GetFloat("zRot");

        xRotationText.text = Mathf.Round(currRotX).ToString();
        yRotationText.text = Mathf.Round(currRotY).ToString();
        zRotationText.text = Mathf.Round(currRotZ).ToString();

    }

    void Update()
    {
        //print("CHURCH POS ===== " + church.transform.position);
        isRecognized();

        //skyPlane_material.material = arbackround.material;
    }

    public void isRecognized()
    {
        //if (arObj.activeSelf)
        //if(imageManager.trackedImagePrefab.gameObject.activeSelf)
        if (hasScanned && !executed)//if has scanned, place arObject on markers position
        {
            Debug.LogError("Recognised");
            tutorial.SetActive(false);
            Vector3 pos = trackedImage.transform.position;
            Quaternion rot = trackedImage.transform.rotation;
            Pose pose = new Pose(pos, rot);
            referencePointMarker = refPointManager.AddReferencePoint(pose);
            foreach (Renderer r in referencePointMarker.transform.GetComponentsInChildren<Renderer>())
            {
                r.enabled = false;
            }
            applySavedValues();

            TrackPosition();
            LookAt lookAtObject = FindObjectOfType<LookAt>();
            lookAtObject.target = referencePointChapel.transform;

            executed = true;//update location or not / false = always update / true = update once
        }
    }

    public void ResetExpierience()
    {
        Debug.LogError("Resets expierience");
        SceneManager.LoadScene(gameObject.scene.name);
    }

    void TrackPosition()
    {
        Debug.LogError("Tracking position");
        Transform reftransform = referencePointMarker.transform;
        Vector3 newDelta = reftransform.forward * xValueChange + reftransform.right * (zValueChange) + Vector3.up * (reftransform.position.y + yValueChange);
        Vector3 newPosition = referencePointMarker.transform.position + newDelta;
        Vector3 savedRoation = referencePointMarker.transform.eulerAngles + new Vector3(currRotX, currRotY, currRotZ);
        Quaternion rotation = Quaternion.Euler(savedRoation);// Quaternion.Euler(savedRoation);
        Pose pose = new Pose(newPosition, rotation);
        if (referencePointChapel != null)
        {
            refPointManager.RemoveReferencePoint(referencePointChapel);
        }
        referencePointChapel = refPointManager.AddReferencePoint(pose);
        RefreshPose();
    }

    void RefreshPose()
    {
        if (referencePointChapel != null)
        {
            Debug.LogError("Refreshing position");
            Transform reftransform = referencePointMarker.transform;
            Vector3 newDelta = reftransform.forward * xValueChange + reftransform.right * (zValueChange) + Vector3.up * (reftransform.position.y + yValueChange);
            Vector3 newPosition = referencePointMarker.transform.position + newDelta;
            Vector3 savedRoation = referencePointMarker.transform.eulerAngles +  new Vector3(currRotX, currRotY, currRotZ);
            Quaternion rotation = Quaternion.Euler(savedRoation);
            referencePointChapel.transform.position = newPosition;
            referencePointChapel.transform.rotation = rotation;
            referencePointChapel.transform.localScale = new Vector3(1, 1, 1) * sizeValueChange;
        }
    }

    public void OnBackButtonPressed(){
        SceneManager.LoadScene(GameSettings.instance.LevelToReturnTo);
    }
    public void OnTrackedImagesChanged(ARTrackedImagesChangedEventArgs eventArgs)//scanning marker
    {

        Debug.LogError("Image tracked");
        for (int i = 0; i < eventArgs.updated.Count; i++)//if marker is updating = scanning marker
        {
            trackedImage = eventArgs.updated[i];

            if (trackedImage.trackingState == TrackingState.Tracking)
            {
                //print("image loc: " + trackedImage.transform.position);

                hasScanned = true;
                imageManager.trackedImagesChanged -= OnTrackedImagesChanged;//subsribe to action, that scans images
            }
            else
            {
                //cannon.SetActive(false);
            }
        }
    }

    //==========================CHANGE POSITION OF CHURCH=======================================================
    public void spawnPoint_X_change(float value)
    {
        xValueChange = value * multiplier;//get value from slider
        PlayerPrefs.SetFloat("x", xValueChange);//save that value
        xValue.text = "Right / Left: " + xValueChange;
        RefreshPose();
    }

    public void spawnPoint_Y_change(float value)
    {
        yValueChange = value * multiplier;
        PlayerPrefs.SetFloat("y", yValueChange);

        yValue.text = "Up / Down: " + yValueChange;
        RefreshPose();
    }

    public void spawnPoint_Z_change(float value)
    {
        zValueChange = value * multiplier;
        PlayerPrefs.SetFloat("z", zValueChange);

        zValue.text = "Further / Closer: " + zValueChange;
        RefreshPose();
    }

    public void sizeChange(float value)
    {
        sizeValueChange = value * multiplier;
        PlayerPrefs.SetFloat("size", sizeValueChange);

        sizeValue.text = "Size: " + sizeValueChange;
        RefreshPose();
    }

    public void SetMultiplier(float value)
    {
        multiplier = value;
    }

    public void ResetRotationValues()
    {
        currRotX = 0;
        currRotY = 0;
        currRotZ = 0;
        PlayerPrefs.SetFloat("xRot", currRotX);
        PlayerPrefs.SetFloat("yRot", currRotY);
        PlayerPrefs.SetFloat("zRot", currRotZ);
    }
    #region rotationUI

    //==========================APPLY SAVED ROTATION=======================================================
    public void applySavedValues()
    {
        Vector3 savedRoation = new Vector3(currRotX, currRotY, currRotZ);

        spawnPoint_X_change(xValueChange);
        int intRotx = Mathf.RoundToInt(currRotX);
        spawnPoint_Y_change(yValueChange);
        int intRoty = Mathf.RoundToInt(currRotY);
        spawnPoint_Z_change(zValueChange);
        int intRotz = Mathf.RoundToInt(currRotZ);
        sizeChange(sizeValueChange);
        xRotationText.text = intRotx.ToString();
        yRotationText.text = intRoty.ToString();
        zRotationText.text = intRotz.ToString();
        RefreshPose();
    }

    //==========================CHANGE ROTAION OF CHURCH=======================================================
    //===================X ROTATION=========================
    public void plusRotationX()
    {
        currRotX = referencePointChapel.transform.rotation.eulerAngles.x - referencePointMarker.transform.eulerAngles.x;
        currRotX += 1f * multiplier;
        PlayerPrefs.SetFloat("xRot", currRotX);

        int intRot = Mathf.RoundToInt(currRotX);
        xRotationText.text = intRot.ToString();
        RefreshPose();
    }

    public void minusRotationX()
    {
        currRotX = referencePointChapel.transform.rotation.eulerAngles.x - referencePointMarker.transform.eulerAngles.x;
        currRotX -= 1f * multiplier;
        PlayerPrefs.SetFloat("xRot", currRotX);

        int intRot = Mathf.RoundToInt(currRotX);
        xRotationText.text = intRot.ToString();
        RefreshPose();
    }

    //==============Y ROTATION==============
    public void plusRotationY()
    {
        currRotY = referencePointChapel.transform.rotation.eulerAngles.y - referencePointMarker.transform.eulerAngles.y;
        currRotY += 1f * multiplier;
        PlayerPrefs.SetFloat("yRot", currRotY);

        int intRot = Mathf.RoundToInt(currRotY);
        yRotationText.text = intRot.ToString();
        RefreshPose();
    }


    public void minusRotationY()
    {
        currRotY = referencePointChapel.transform.rotation.eulerAngles.y - referencePointMarker.transform.eulerAngles.y;
        currRotY -= 1f * multiplier;
        PlayerPrefs.SetFloat("yRot", currRotY);

        int intRot = Mathf.RoundToInt(currRotY);
        yRotationText.text = intRot.ToString();
        RefreshPose();
    }


    //==============Z ROTATION==============
    public void plusRotationZ()
    {
        currRotZ = referencePointChapel.transform.rotation.eulerAngles.z - referencePointMarker.transform.eulerAngles.z;
        currRotZ += 1f * multiplier;
        PlayerPrefs.SetFloat("zRot", currRotZ);

        int intRot = Mathf.RoundToInt(currRotZ);
        zRotationText.text = intRot.ToString();
        RefreshPose();
    }


    public void minusRotationZ()
    {
        currRotZ = referencePointChapel.transform.rotation.eulerAngles.z - referencePointMarker.transform.eulerAngles.z;
        currRotZ -= 1f * multiplier;
        PlayerPrefs.SetFloat("zRot", currRotZ);

        int intRot = Mathf.RoundToInt(currRotZ);
        zRotationText.text = intRot.ToString();
        RefreshPose();
    }
    #endregion rotationUI

    private void OnApplicationQuit()
    {
        PlayerPrefs.Save();//SAVE VALUES WHEN CLOSING APPLICATION
    }
}
