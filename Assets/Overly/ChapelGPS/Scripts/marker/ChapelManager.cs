﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;

namespace Overly.ChapelGpsScripts
{
    public class ChapelManager : MonoBehaviour
    {
        enum STATE { WAIT_FOR_SCAN, SCANED };
        STATE state = STATE.WAIT_FOR_SCAN;

        [HideInInspector]
        public static ChapelManager instance;

        [Header("AR components")]
        ARSessionOrigin arSessionOrigin;
        ARTrackedImageManager imageManager;
        ARReferencePointManager refPointManager;

        [Header("UI components")]
        public GameObject instructions;
        public GameObject spawnButton;

        [Header("Objects to spawn")]
        public GameObject chapel;

        [Header("UI")]
        public TMP_InputField inputX;
        public TMP_InputField inputY;
        public TMP_InputField inputZ;
        public TMP_InputField inputS;

        public TMP_InputField inputRotX;
        public TMP_InputField inputRotY;
        public TMP_InputField inputRotZ;

        float moveStep = 1f;
        float rotStep = 15f;

        ARTrackedImage trackedImage;
        ARReferencePoint referencePointChapel;
        ARReferencePoint referencePointMarker;
        List<ARReferencePoint> additionalReferences;

        Vector3 chapelPosition;
        Quaternion chapelRotation;
        float chapelScale = 1;


        public TextMeshProUGUI debugvalue;

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(this);
                return;
            }

            InitArComponents();
            imageManager.trackedImagesChanged += OnTrackedImagesChanged;
            LoadPosition();
            LoadRotation();
        }

        private void OnTrackedImagesChanged(ARTrackedImagesChangedEventArgs eventArgs)
        {
            if (state == STATE.WAIT_FOR_SCAN)
            {
                for (int i = 0; i < eventArgs.updated.Count; i++)//if marker is updating = scanning marker
                {
                    trackedImage = eventArgs.updated[i];

                    if (trackedImage.trackingState == TrackingState.Tracking)
                    {
                        state = STATE.SCANED;
                        instructions.SetActive(false);
                        spawnButton.SetActive(true);
                    }
                }
            }

            if (state == STATE.SCANED)
            {
                for (int i = 0; i < eventArgs.removed.Count; i++)//if marker is removed
                {
                    trackedImage = eventArgs.removed[i];
                    Debug.LogError(trackedImage.trackingState);
                    if (trackedImage.trackingState == TrackingState.None)
                    {
                        state = STATE.WAIT_FOR_SCAN;
                        instructions.SetActive(true);
                        spawnButton.SetActive(false);
                    }
                }
            }
        }

        public IEnumerator PositionReferencePoints()
        {

            if (additionalReferences == null)
            {
                additionalReferences = new List<ARReferencePoint>();
            }
            foreach (ARReferencePoint point in additionalReferences)
            {
                refPointManager.RemoveReferencePoint(point);
                additionalReferences.Remove(point);
            }


            // track the position of marker with additional referencepoint
            Transform markerTransform = referencePointMarker.transform;
            // marker position and transform
            Vector3 euler = markerTransform.rotation.eulerAngles;
            euler.x = 0;
            euler.z = 0;
            Quaternion rot = Quaternion.Euler(euler);

            Vector3 startPos = markerTransform.position;
            Vector3 targetPos = startPos + rot * chapelPosition;

            startPos.y = targetPos.y;

            for (int i = 0; i < 10; i++)
            {
                Debug.LogError(i);
                float l = 0.1f * i;

                Vector3 pointPosition = Vector3.Lerp(startPos, targetPos, l);

                Pose pose = new Pose(pointPosition, rot);
                ARReferencePoint point = refPointManager.AddReferencePoint(pose);
                while (true)
                {
                    yield return null;
                    if (!point.pending)
                    {
                        additionalReferences.Add(point);
                        break;
                    }
                }

            }

            StartCoroutine(PositionChapelReferencePoint());
        }

        public void LookUpAndPosition()
        {
            StartCoroutine(WaitLookUp());
        }

        public IEnumerator WaitLookUp()
        {
            StartCoroutine(PlaceMarkerReference());
            while (true)
            {
                yield return null;
                debugvalue.text = Vector3.Cross(Camera.main.transform.up, Vector3.up).magnitude.ToString("0.00");
                if (Vector3.Cross(Camera.main.transform.up, Vector3.up).magnitude < 0.1f)
                {
                    spawnButton.SetActive(false);
                    Handheld.Vibrate();
                    break;
                }
            }
            StartCoroutine(PositionReferencePoints());
        }



        public IEnumerator PlaceMarkerReference()
        {
            // track the position of marker with additional referencepoint
            Transform markerTransform = trackedImage.transform;
            // marker position and transform
            Vector3 euler = markerTransform.rotation.eulerAngles;
            euler.x = 0;
            euler.z = 0;
            Quaternion rot = Quaternion.Euler(euler);

            Pose pose = new Pose(markerTransform.position, rot);
            // if reference point exists, destroy it
            if (referencePointMarker != null)
            {
                refPointManager.RemoveReferencePoint(referencePointMarker);
            }
            // make a new referencepoint
            referencePointMarker = refPointManager.AddReferencePoint(pose);

            while (true)
            {
                yield return null;
                if (!referencePointMarker.pending)
                {
                    break;
                }
            }
        }

        public void ShowAdditionalMarkers(bool show)
        {
            foreach (AdditionalMarker marker in FindObjectsOfType<AdditionalMarker>())
            {
                marker.ShowMeshRenderer(show);
            }
        }

        public IEnumerator PositionChapelReferencePoint()
        {
            // position chapel relative to marker reference point
            Transform markerTransform = referencePointMarker.transform;
            Vector3 euler = markerTransform.rotation.eulerAngles;
            euler.x = 0;
            euler.z = 0;
            Quaternion rot = Quaternion.Euler(euler);


            Vector3 pos = markerTransform.rotation * chapelPosition;
            Pose pose = new Pose(markerTransform.position + pos, rot * chapelRotation);
            if (referencePointChapel != null)
            {
                refPointManager.RemoveReferencePoint(referencePointChapel);
            }
            referencePointChapel = refPointManager.AddReferencePoint(pose);
            while (true)
            {
                yield return null;
                if (!referencePointChapel.pending)
                {
                    break;
                }
            }
            Instantiate(chapel, referencePointChapel.transform.position, referencePointChapel.transform.rotation, referencePointChapel.transform);
            referencePointChapel.transform.localScale = new Vector3(1, 1, 1) * chapelScale;
            // update input fields
            UpdateInputUI();
            // set pointing direction of markers arrow
            LookAt lookAtObject = FindObjectOfType<LookAt>();
            lookAtObject.target = referencePointChapel.transform;
        }

        void InitArComponents()
        {
            arSessionOrigin = FindObjectOfType<ARSessionOrigin>();
            imageManager = FindObjectOfType<ARTrackedImageManager>();
            refPointManager = FindObjectOfType<ARReferencePointManager>();
        }


        public void ResetPosition()
        {
            Vector3 newPos = new Vector3();
            SetPosition(newPos);
        }


        void SetPosition(Vector3 position)
        {
            chapelPosition = position;
            StartCoroutine(PositionChapelReferencePoint());
            UpdateInputUI();
        }

        void UpdateInputUI()
        {
            inputX.text = chapelPosition.x.ToString("00.00");
            inputY.text = chapelPosition.y.ToString("00.00");
            inputZ.text = chapelPosition.z.ToString("00.00");
            inputS.text = chapelScale.ToString("00.00");

            Vector3 rot = chapelRotation.eulerAngles;
            inputRotX.text = rot.x.ToString("00.00");
            inputRotY.text = rot.y.ToString("00.00");
            inputRotZ.text = rot.z.ToString("00.00");
        }

        public void SetX(float value)
        {
            Vector3 newPos = chapelPosition + new Vector3(1, 0, 0) * value;
            SetPosition(newPos);
        }

        public void SetY(float value)
        {
            Vector3 newPos = chapelPosition + new Vector3(0, 1, 0) * value;
            SetPosition(newPos);
        }

        public void SetZ(float value)
        {
            Vector3 newPos = chapelPosition + new Vector3(0, 0, 1) * value;
            SetPosition(newPos);
        }

        public void SetS(float value)
        {
            // set SCALE
            chapelScale += value;
            referencePointChapel.transform.localScale = new Vector3(1, 1, 1) * chapelScale;
            UpdateInputUI();
        }

        public void SetX(string value)
        {
            Vector3 newPos = chapelPosition;
            newPos.x = float.Parse(value);
            SetPosition(newPos);
        }

        public void SetY(string value)
        {
            Vector3 newPos = chapelPosition;
            newPos.y = float.Parse(value);
            SetPosition(newPos);
        }

        public void SetZ(string value)
        {
            Vector3 newPos = chapelPosition;
            newPos.z = float.Parse(value);
            SetPosition(newPos);
        }

        public void SetS(string value)
        {
            chapelScale = float.Parse(value);
            referencePointChapel.transform.localScale = new Vector3(1, 1, 1) * chapelScale;
        }

        public void MoveX(int dir)
        {
            float value = moveStep * dir;
            SetX(value);
        }

        public void MoveY(int dir)
        {
            float value = moveStep * dir;
            SetY(value);
        }

        public void MoveZ(int dir)
        {
            float value = moveStep * dir;
            SetZ(value);
        }
        public void Scale(int dir)
        {
            float value = moveStep * dir;
            SetS(value);
        }

        public void SetStep(float step)
        {
            this.moveStep = step;
        }

        public void SavePosition()
        {
            PlayerPrefs.SetFloat("chapelPositionX", chapelPosition.x);
            PlayerPrefs.SetFloat("chapelPositionY", chapelPosition.y);
            PlayerPrefs.SetFloat("chapelPositionZ", chapelPosition.z);
            PlayerPrefs.SetFloat("chapelScale", chapelScale);

        }
        public void SetTestPos()
        {
            Vector3 pos = new Vector3(6.29f, -4.4f, 90.26f);
            chapelPosition = pos;
            chapelScale = 2.62f;

            Vector3 rot = new Vector3(0, 89f, 0);
            chapelRotation = Quaternion.Euler(rot);
            if (state == STATE.SCANED)
            {
                StartCoroutine(PositionChapelReferencePoint());
            }
        }
        public void LoadPosition()
        {
            Vector3 pos = new Vector3(6.29f, -4.4f, 90.26f);
            chapelScale = 4.62f;
            // load & set position
            if (PlayerPrefs.HasKey("chapelPositionX"))//Getting saved values from the last game
                pos.x = PlayerPrefs.GetFloat("chapelPositionX", 6.29f);
            if (PlayerPrefs.HasKey("chapelPositionY"))//Getting saved values from the last game
                pos.y = PlayerPrefs.GetFloat("chapelPositionY", -4.4f);
            if (PlayerPrefs.HasKey("chapelPositionZ"))//Getting saved values from the last game
                pos.z = PlayerPrefs.GetFloat("chapelPositionZ", 90.26f);
            chapelPosition = pos;

            // load & set scale
            if (PlayerPrefs.HasKey("chapelScale"))//Getting saved values from the last game
                chapelScale = PlayerPrefs.GetFloat("chapelScale", 4.62f);


            if (state == STATE.SCANED)
            {
                // if marker scanned, position the chapel
                StartCoroutine(PositionChapelReferencePoint());
            }
        }

        #region ROTATION
        public void SaveRotation()
        {
            Vector3 rot = chapelRotation.eulerAngles;
            PlayerPrefs.SetFloat("chapelRotationX", rot.x);
            PlayerPrefs.SetFloat("chapelRotationY", rot.y);
            PlayerPrefs.SetFloat("chapelRotationZ", rot.z);

        }
        public void LoadRotation()
        {
            Vector3 rot = new Vector3(0, 89f, 0);
            if (PlayerPrefs.HasKey("chapelRotationX"))//Getting saved values from the last game
                rot.x = PlayerPrefs.GetFloat("chapelRotationX", 0f);
            if (PlayerPrefs.HasKey("chapelRotationY"))//Getting saved values from the last game
                rot.y = PlayerPrefs.GetFloat("chapelRotationY", 89f);
            if (PlayerPrefs.HasKey("chapelRotationZ"))//Getting saved values from the last game
                rot.z = PlayerPrefs.GetFloat("chapelRotationZ", 0f);
            chapelRotation = Quaternion.Euler(rot);


            if (state == STATE.SCANED)
            {
                StartCoroutine(PositionChapelReferencePoint());
            }
        }
        public void ResetRotation()
        {
            Vector3 rot = new Vector3();
            chapelRotation = Quaternion.Euler(rot);
            StartCoroutine(PositionChapelReferencePoint());
            UpdateInputUI();
        }

        public void RotateX(int dir)
        {
            float value = rotStep * dir;
            chapelRotation *= Quaternion.AngleAxis(value, Vector3.right);
            StartCoroutine(PositionChapelReferencePoint());
            UpdateInputUI();
        }
        public void RotateY(int dir)
        {
            float value = rotStep * dir;
            chapelRotation *= Quaternion.AngleAxis(value, Vector3.up);
            StartCoroutine(PositionChapelReferencePoint());
            UpdateInputUI();
        }
        public void RotateZ(int dir)
        {
            float value = rotStep * dir;
            chapelRotation *= Quaternion.AngleAxis(value, Vector3.forward);
            StartCoroutine(PositionChapelReferencePoint());
            UpdateInputUI();
        }

        public void SetRotX(string value)
        {
            // chapelRotation *= Quaternion.AngleAxis(float.Parse(value), Vector3.right);
        }

        public void SetRotY(string value)
        {
            // chapelRotation *= Quaternion.AngleAxis(float.Parse(value), Vector3.up);
        }

        public void SetRotZ(string value)
        {
            // chapelRotation *= Quaternion.AngleAxis(float.Parse(value), Vector3.forward);
        }

        public void SetRotStep(float step)
        {
            rotStep = step;
        }
        #endregion

    }
}
