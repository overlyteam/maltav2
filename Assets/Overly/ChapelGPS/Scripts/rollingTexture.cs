﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class rollingTexture : MonoBehaviour
{
    public Material material;
    public float speed;

    Vector2 offset;
    float slider;

    // Start is called before the first frame update
    void Start()
    {
        //material = GetComponent<Material>();
        slider = 0f;
        offset = material.mainTextureOffset;
    }

    // Update is called once per frame
    void Update()
    {
        if(slider >= 100f)
        {
            slider = 0f;
        }

        slider += speed * Time.deltaTime;
        offset = new Vector2(slider, 0f);
        material.mainTextureOffset = offset;
    }
}
