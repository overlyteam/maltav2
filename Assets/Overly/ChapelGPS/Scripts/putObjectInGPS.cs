﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class putObjectInGPS : MonoBehaviour
{
    public float obj_longitude;
    public float obj_langitude;
    public float obj_latitude;

    // Start is called before the first frame update
    void Start()
    {
        Vector3 location = new Vector3(obj_langitude, obj_latitude, obj_longitude);
        transform.position = location;

        
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("object pos: " + transform.position);
    }
}
