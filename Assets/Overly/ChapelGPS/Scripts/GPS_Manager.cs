﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ARLocation;
using TMPro;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace Overly.ChapelGpsScripts
{
    public class GPS_Manager : MonoBehaviour
    {
        public GameObject ArSession;
        public GameObject ArSessionOrigin;
        public List<GpsPosition> gpsPositions;
        bool allowTracking = false;
        bool positionAreSet = false;

        public Animator gpsAnimator;
        public GameObject pins;
        // Start is called before the first frame update
        private void Awake()
        {

        }
        void OnEnable()
        {
            //if (!positionAreSet && allowTracking)
            //{
            //    foreach (GpsPosition pos in gpsPositions)
            //    {
            //        Debug.Log("Try Set");
            //        AssignPosition(pos);
            //    }
            //}
            allowTracking = false;
            positionAreSet = false;

            if (!positionAreSet)
            {
                StartInitialisation();
            }
        }
        private void OnDestroy()
        {
            Destroy(ArSession);
            Destroy(ArSessionOrigin);

        }
        private void Start()
        {
            //if (!positionAreSet && allowTracking)
            //{
            //    foreach (GpsPosition pos in gpsPositions)
            //    {
            //        Debug.Log("Try Set");
            //        AssignPosition(pos);
            //    }
            //}
            //else
            //{
            // ARLocationManager.Instance.ResetARSession(StartInitialisation);
            if (!positionAreSet)
            {
                StartInitialisation();
            }

        }


        void StartInitialisation()
        {
            StopAllCoroutines();
            StartCoroutine(WaitForLocation());
        }

        IEnumerator WaitForLocation()
        {
            Debug.Log("Restart!!!!!");
            gpsAnimator.SetBool("Show", true);
            yield return new WaitForSeconds(10f);

            pins.SetActive(true);
            gpsAnimator.SetBool("Show", false);
            while (true)
            {
                Debug.Log("No Location ");
                yield return new WaitForSeconds(1f);
                if (allowTracking == true)
                {
                    break;
                }
            }
            if (!positionAreSet && allowTracking)
            {
                foreach (GpsPosition pos in gpsPositions)
                {
                    Debug.Log("Try Set");
                    AssignPosition(pos);
                }
            }
        }

        public void AllowTracking(bool allow = true)
        {
            Debug.Log("Allow tracking");
            allowTracking = allow;
        }


        public void OnBackBtnPressed()
        {
            SceneManager.LoadScene(GameSettings.instance.LevelToReturnTo);
        }

        void AssignPosition(GpsPosition pos)
        {
            var opts = new PlaceAtLocation.PlaceAtOptions()
            {
                HideObjectUntilItIsPlaced = true,
                MaxNumberOfLocationUpdates = 0,
                MovementSmoothing = 0.1f,
                UseMovingAverage = true
            };

            Transform target = PlaceAtLocation.CreatePlacedInstance(pos.prefabObject, pos.location, opts).transform;
            pos.label.text = pos.location.Label;

            if (pos.pin != null)
            {
                pos.pin.gameObject.SetActive(true);
                pos.pin.target = target;
                pos.pin.text.text = pos.location.Label;
            }
            if (pos.pointer.GetComponent<LookAt>() != null)
            {
                pos.pointer.GetComponent<LookAt>().target = target;
                Debug.Log("Set");
            }
            else
            {
                Debug.Log("Cant set");
            }
            positionAreSet = true;
        }

        [System.Serializable]
        public class GpsPosition
        {
            [Header("UI")]
            public TextMeshProUGUI label;
            public GameObject pointer;
            public PlaceOnScreen pin;
            [Header("Info")]
            public GameObject prefabObject;
            public Location location;

        }
    }
}