﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

namespace Overly.ChapelGpsScripts
{
    public class PlaceOnScreen : MonoBehaviour
    {
        public bool debug;
        public TextMeshProUGUI text;
        [HideInInspector]
        public Transform target;

        // Update is called once per frame
        void Update()
        {
            if (target != null)
            {
                Vector3 pos = Camera.main.WorldToScreenPoint(target.position);
                //text.text = pos.ToString();
                if (pos.z > 0)
                {
                    pos.z = 0.1f;
                }
                else
                {
                    pos.z *= 1000;
                }
                if (debug)
                {
                    Debug.LogError(pos);
                }
                GetComponent<RectTransform>().position = pos;
            }
            else
            {
                Debug.Log("No target");
            }
        }
    }
}