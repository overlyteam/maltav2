﻿using UnityEngine;
using UnityEngine.UI;
using UnityEditor;

public class HotKeys
{
    [MenuItem("Edit/HotKeys/ToggleGameObject &1")]
    private static void ToggleGameObjects()
    {
        GameObject[] SelectedObjects = Selection.gameObjects;
        foreach (var obj in SelectedObjects)
        {
            obj.SetActive(!obj.activeInHierarchy);
            EditorUtility.SetDirty(obj);
        }
    }

    [MenuItem("Edit/HotKeys/ToggleCanvasOrImage &2")]
    private static void ToggleCanvas()
    {
        GameObject[] SelectedObjects = Selection.gameObjects;
        foreach (var obj in SelectedObjects)
        {
            if (obj.GetComponent<Canvas>())
            {
                obj.GetComponent<Canvas>().enabled = !obj.GetComponent<Canvas>().enabled;
                EditorUtility.SetDirty(obj);
            }
            if (obj.GetComponent<Image>())
            {
                obj.GetComponent<Image>().enabled = !obj.GetComponent<Image>().enabled;
                EditorUtility.SetDirty(obj);
            }
            if (obj.GetComponent<Text>())
            {
                obj.GetComponent<Text>().enabled = !obj.GetComponent<Text>().enabled;
                EditorUtility.SetDirty(obj);
            }
        }
    }

    [MenuItem("Edit/HotKeys/ToggleImageAlpha &3")]
    private static void ToggleAlpha()
    {
        GameObject[] SelectedObjects = Selection.gameObjects;
        foreach (var obj in SelectedObjects)
        {
            Image img = obj.GetComponent<Image>();
            if (img)
            {
                if (img.color.a != 1)
                {
                    img.color = new Color(img.color.r, img.color.g, img.color.b, 1f);
                }
                else
                {
                    img.color = new Color(img.color.r, img.color.g, img.color.b, 0.5f);
                }
                EditorUtility.SetDirty(obj);
            }
        }
    }
}