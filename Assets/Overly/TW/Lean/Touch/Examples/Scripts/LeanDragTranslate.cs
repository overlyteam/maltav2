using UnityEngine;
using Overly.TWScripts;
using serginian.Media;
using System.Collections.Generic;

namespace Lean.Touch
{
    /// <summary>This component allows you to translate the current GameObject relative to the camera using the finger drag gesture.</summary>
    [HelpURL(LeanTouch.HelpUrlPrefix + "LeanDragTranslate")]
    [AddComponentMenu(LeanTouch.ComponentPathPrefix + "Drag Translate")]
    public class LeanDragTranslate : MonoBehaviour
    {
        /// <summary>The method used to find fingers to use with this component. See LeanFingerFilter documentation for more information.</summary>
        public LeanFingerFilter Use = new LeanFingerFilter(true);
        public Transform moveThisObject = null;
        public bool positionOnGroundPlane = false;
        public bool faceCamera = false;
        public GameObject enableThis;

        public bool active = false;
        bool wasHit = false;

        float fingerGroundDistance;
        Vector3 touchGroundDelta;


        float timer = 0;

        /// <summary>The camera the translation will be calculated using.\n\nNone = MainCamera.</summary>
        [Tooltip("The camera the translation will be calculated using.\n\nNone = MainCamera.")]
        public Camera Camera;

        protected virtual void Awake()
        {
            LeanTouch.OnFingerDown += OnFingerDown;
            LeanTouch.OnFingerUp += OnFingerUp;
            LeanTouch.OnGesture += OnGesture;
            LeanTouch.OnFingerTap += OnTap;

            if (moveThisObject == null)
            {
                moveThisObject = transform;
            }
        }

        private void OnDestroy()
        {
            LeanTouch.OnFingerDown -= OnFingerDown;
            LeanTouch.OnFingerUp -= OnFingerUp;
            LeanTouch.OnGesture -= OnGesture;
            LeanTouch.OnFingerTap -= OnTap;
        }

        public void OnGesture(List<LeanFinger> fingers)
        {
            if (wasHit)
            {
                if (fingers.Count == Use.RequiredFingerCount)
                {
                    var screenDelta = LeanGesture.GetScreenDelta(fingers);
                    if (screenDelta != Vector2.zero)
                    {
                        // Perform the translation
                        if (moveThisObject is RectTransform)
                        {
                            TranslateUI(screenDelta);
                        }
                        else
                        {
                            Translate(LeanGesture.GetLastScreenCenter(fingers));
                        }
                    }
                }
            }
        }

        public void OnTap(LeanFinger finger)
        {

            if (IsHit(finger) && (LeanTouch.Fingers.Count == Use.RequiredFingerCount))
            {
                FindObjectOfType<touchManager>().changeVideos(gameObject);
            }
        }

        public void OnFingerDown(LeanFinger finger)
        {
            if (!finger.IsOverGui && (LeanTouch.Fingers.Count == Use.RequiredFingerCount))
            {
                bool canbeActive = true;
                foreach (LeanDragTranslate drg in FindObjectsOfType<LeanDragTranslate>())
                {
                    if (drg.active && drg != this)
                    {
                        if (drg.gameObject == gameObject)
                        {
                            // other leandrag of this object
                            drg.Disable();
                            break;
                        }
                        canbeActive = false;
                        break;
                    }
                }
                if (canbeActive)
                {
                    if (IsHit(finger))
                    {
                        if (!active)
                        {
                            timer = Time.deltaTime;
                            if (FindObjectOfType<touchManager>().EnterDragMode(gameObject))
                            {
                                SetFingerGroundDelta(finger);
                                SetEnableThisState(true);
                                wasHit = true;
                                active = true;
                            }
                        }
                    }
                }
            }
        }

        void SetFingerGroundDelta(LeanFinger finger)
        {
            var camera = LeanTouch.GetCamera(Camera, gameObject);
            // Screen position of the transform
            var screenPoint = finger.ScreenPosition;
            touchGroundDelta = camera.WorldToScreenPoint(moveThisObject.position) - new Vector3(screenPoint.x, screenPoint.y, 0);
            touchGroundDelta.z = 0;

            //Ray ray = camera.ScreenPointToRay(screenPoint);
            //Plane videoPlane = new Plane(moveThisObject.forward, moveThisObject.position);
            //Plane groundplane = TWManager.instnance.infiniteGroundPlane;

            //if (videoPlane.Raycast(ray, out float dist))
            //{
            //    Vector3 touchWorldPosition = camera.ScreenToWorldPoint(new Vector3(screenPoint.x, screenPoint.y, dist));
            //    fingerGroundDistance = groundplane.GetDistanceToPoint(touchWorldPosition);
            //}


        }

        public void OnFingerUp(LeanFinger finger)
        {
            if (active)
            {
                FindObjectOfType<touchManager>().ExitDragMode();
                Disable();
            }
        }

        public void Disable()
        {
            if (active)
            {
                wasHit = false;
                active = false;
                SetEnableThisState(false);
            }
        }

        private bool IsHit(LeanFinger finger)
        {
            if (TWManager.instnance.contentType == TWManager.CONTENT_TYPE.SINGLE)
            {
                return true;
            }
            return Raycaster.instance.CheckRaycastHitVideoAlpha(finger.GetRay(), GetComponent<Collider>());
        }

        private void SetEnableThisState(bool state)
        {
            if (enableThis != null)
            {
                if (enableThis.activeSelf != state)
                {
                    enableThis.SetActive(state);
                }
            }
        }

        private void TranslateUI(Vector2 screenDelta)
        {
            var camera = LeanTouch.GetCamera(Camera, gameObject);

            // Screen position of the transform
            var screenPoint = RectTransformUtility.WorldToScreenPoint(camera, moveThisObject.position);

            // Add the deltaPosition
            screenPoint += screenDelta;

            // Convert back to world space
            var worldPoint = default(Vector3);

            if (RectTransformUtility.ScreenPointToWorldPointInRectangle(moveThisObject.parent as RectTransform, screenPoint, camera, out worldPoint) == true)
            {
                moveThisObject.position = worldPoint;
            }
        }

        private void Translate(Vector2 screenDelta)
        {
            // Make sure the camera exists
            var camera = LeanTouch.GetCamera(Camera, gameObject);

            if (camera != null)
            {
                // Screen position of the transform
                var screenPoint = camera.WorldToScreenPoint(moveThisObject.position);

                // Add the deltaPosition
                screenPoint = (Vector3)screenDelta;
                screenPoint.z = 0;

                Plane ProjectionPlane;
                Ray ray;
                Vector3 newVideoPosition;

                if (positionOnGroundPlane)
                {
                    ProjectionPlane = TWManager.instnance.infiniteGroundPlane;
                    screenPoint += touchGroundDelta;
                    ray = camera.ScreenPointToRay(screenPoint);
                    //ray.origin -= Vector3.up * fingerGroundDistance;
                    if (ProjectionPlane.Raycast(ray, out float dist))
                    {
                        Vector3 newScreenPoint = camera.transform.position + ray.direction * dist - Vector3.up * fingerGroundDistance;
                        moveThisObject.position = camera.ScreenToWorldPoint(new Vector3(screenPoint.x, screenPoint.y, dist));
                    }
                }
                else
                {
                    ProjectionPlane = new Plane(moveThisObject.parent.forward, moveThisObject.parent.position);
                    ray = camera.ScreenPointToRay(screenPoint);
                    if (ProjectionPlane.Raycast(ray, out float dist))
                    {
                        float x = moveThisObject.position.x;
                        float z = moveThisObject.position.z;
                        Vector3 pos = camera.ScreenToWorldPoint(new Vector3(screenPoint.x, screenPoint.y, dist));
                        pos.x = x; 
                        pos.z = z;
                        moveThisObject.position = pos;
                    }
                }

                if (faceCamera)
                {
                    Vector3 dir = camera.transform.forward;
                    dir.y = 0;
                    moveThisObject.forward = dir;
                }

            }
            else
            {
                Debug.LogError("Failed to find camera. Either tag your camera as MainCamera, or set one in this component.", this);
            }
        }
    }
}