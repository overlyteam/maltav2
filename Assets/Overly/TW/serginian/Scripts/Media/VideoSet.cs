﻿using serginian.Media;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

[RequireComponent(typeof(VideoPlayerTW))]
public class VideoSet : MonoBehaviour
{
    public static VideoSet CurrentVideoSet = null;

    // public List<VideoClip> videosToRotate;
    public List<string> videosToRotate;

    public int curVideo = 0;
    public bool IsPlaying { get { return GetComponent<VideoPlayerTW>().IsPlaying; } }
    public bool IsLoopReached { get { return GetComponent<VideoPlayerTW>().IsPlaying; } }
    private VideoPlayerTW videoPlayer;
    private int arrayLength;

    public void Play()
    {
        GetComponent<VideoPlayerTW>().Play();
    }

    public void Stop()
    {
        GetComponent<VideoPlayerTW>().ResetVideo();
    }




    /***************************** PUBLIC INTERFACE ******************************/

    public void Reset()
    {
        //curVideo = 0;
        //videoPlayer.Load(ClipFront);
        curVideo = 0;
        videoPlayer.Load(videosToRotate[curVideo]);

        arrayLength = videosToRotate.Count;

        if (CurrentVideoSet != this)
        {
            //CurrentVideoSet?.gameObject.SetActive(false);//this turns off new videos = always will be only one active video in scene
            //CurrentVideoSet = this;
        }
    }

    public void rotateVideos()
    {
        

        /*if(curVideo <= arrayLength)
        {
            print("playing next video..");
            curVideo++;
            videoPlayer.Load(videosToRotate[curVideo]);
        }
        else
        {
            print("reached max, switching to first video..");
            curVideo = 0;
            videoPlayer.Load(videosToRotate[curVideo]);
        }*/

        if(curVideo + 1 < arrayLength)
        {
            curVideo++;
            videoPlayer.Load(videosToRotate[curVideo]);
            print("playing " + curVideo);
        }
        else
        {
            print("reached max, switching to first video.. " + curVideo);
            curVideo = 0;
            videoPlayer.Load(videosToRotate[curVideo]);
        }
        
    }
    /***************************** INNER LOGIC ******************************/

    private void Awake()
    {
        videoPlayer = GetComponent<VideoPlayerTW>();
    }

    private void OnEnable()
    {
    }

    private void Update()
    {

    }

}
