﻿using System;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using UnityEngine.Events;
using Overly.TWScripts;
using System.Collections;

namespace serginian.Media
{
    public enum VideoType { Default, Masked, Alpha }
    public enum VideoState { Playing, Paused, Stopped, None }


    [AddComponentMenu("serginian/Video Player - Extended")]
    public class VideoPlayerTW : MonoBehaviour
    {
        [Header("Settings")]
        public VideoType VideoType = VideoType.Default;
        public bool AutoPlay = false;
        public bool Loop = false;
        [Range(0f, 10f)]
        public float PlaybackSpeed = 1f;
        public GameObject LoadingIcon;

        [Header("Video")]
        public VideoSource VideoMode = VideoSource.VideoClip;
        public string URL;
        public VideoClip Clip;
        public Renderer[] TargetVideoPlanes;
        public RawImage[] TargetVideoImages;

        [Header("Audio")]
        public VideoAudioOutputMode AudioMode = VideoAudioOutputMode.AudioSource;
        public AudioSource AudioSource;

        [Header("Events")]
        public VideoPlayerEvent OnStart;
        public VideoPlayerEvent OnEnd;

        // Delegates
        public delegate void VideoLoaded();
        public VideoLoaded OnVideoLoaded;

        private Texture2D defTexture = null;
        private Texture2D alphaTexture = null;
        private VideoPlayer _videoPlayer;
        private float _savedVolume = 1f;
        private bool awakeMethodWasExecuted = false;

        public bool IsInitialized { get; private set; }
        public bool IsPlaying { get { return _videoPlayer.isPlaying; } }
        public bool IsPrepared { get { return _videoPlayer.isPrepared; } }
        public bool IsMuted { get; private set; }
        public float Length { get { return _videoPlayer.frameCount / _videoPlayer.frameRate; } }
        public int Position { get { return System.Convert.ToInt32(_videoPlayer.time); } }
        public VideoState State { get; private set; } = VideoState.None;



        /********************************* INTERNAL LOGIC *********************************/

        private void Awake()
        {
            foreach (Renderer target in TargetVideoPlanes)
            {
                target.enabled = false;
            }
            if (!awakeMethodWasExecuted)
            {
                _videoPlayer = gameObject.AddComponent<VideoPlayer>();
                _videoPlayer.renderMode = VideoRenderMode.APIOnly;
                _videoPlayer.waitForFirstFrame = true;
                _videoPlayer.playOnAwake = false;

                if (TargetVideoImages.Length == 0 && TargetVideoPlanes.Length == 0)
                {
                    RawImage rimg = transform.GetComponent<RawImage>();
                    Renderer rndr = transform.GetComponent<Renderer>();

                    if (rimg != null)
                        TargetVideoImages = new RawImage[] { rimg };

                    if (rndr != null)
                        TargetVideoPlanes = new Renderer[] { rndr };
                }

                _videoPlayer.loopPointReached += _videoPlayer_loopPointReached;
                _videoPlayer.frameReady += _videoPlayer_frameReady;
                /*_videoPlayer.prepareCompleted += (e) =>
                {
                    if (AutoPlay && State == VideoState.Playing)
                        _videoPlayer.Play();
                };*/

                awakeMethodWasExecuted = true;
            }
        }

        private void Start()
        {
            foreach (Renderer target in TargetVideoPlanes)
            {
                target.enabled = false;
            }
            if (!IsInitialized)
                _Prepare();
        }

        private void _videoPlayer_frameReady(VideoPlayer source, long frameIdx)
        {
            _videoPlayer.sendFrameReadyEvents = false;

            TargetVideoPlanes.ToList().ForEach(t => t.material.mainTexture = source.texture);
            TargetVideoImages.ToList().ForEach(t => t.texture = source.texture);

            if (VideoType == VideoType.Alpha)
            {
                TargetVideoPlanes.ToList().ForEach(t => t.material.SetTexture("_Mask", source.texture));
                TargetVideoImages.ToList().ForEach(t => t.material.SetTexture("_Mask", source.texture));
            }

            OnStart?.Invoke(this);
        }

        private void _videoPlayer_loopPointReached(VideoPlayer source)
        {
            if (Loop)
                _videoPlayer.Play();

            if (OnEnd != null)
                OnEnd.Invoke(this);
        }

        // Update is called once per frame
        private void Update()
        {
            if (LoadingIcon != null)
            {
                if (_videoPlayer.isPlaying && LoadingIcon.activeSelf)
                    LoadingIcon.SetActive(false);
                else if (!_videoPlayer.isPlaying && !LoadingIcon.activeSelf)
                    LoadingIcon.SetActive(true);
            }
        }

        private void OnEnable()
        {
            foreach (Renderer target in TargetVideoPlanes)
            {
                target.enabled = false;
            }
            _videoPlayer.sendFrameReadyEvents = true;
            // if (State == VideoState.Playing)
            //    Play();
        }

        private void OnDisable()
        {
            if (defTexture != null)
            {
                TargetVideoPlanes.Where(t => t != null).ToList().ForEach(t => t.material.mainTexture = defTexture);
                TargetVideoImages.Where(t => t != null).ToList().ForEach(t => t.texture = defTexture);
            }

            if (alphaTexture != null && VideoType == VideoType.Alpha)
            {
                TargetVideoPlanes.ToList().ForEach(t => t.material.SetTexture("_Mask", alphaTexture));
                TargetVideoImages.ToList().ForEach(t => t.material.SetTexture("_Mask", alphaTexture));
            }
        }



        /********************************* INTERNAL LOGIC *********************************/

        public void Mute()
        {
            if (AudioMode == VideoAudioOutputMode.AudioSource && !IsMuted)
            {
                if (!IsInitialized)
                    _Prepare();

                _savedVolume = AudioSource.volume;
                AudioSource.volume = 0;
                IsMuted = true;
            }
        }

        public void Unmute()
        {
            if (AudioMode == VideoAudioOutputMode.AudioSource && IsMuted)
            {
                if (!IsInitialized)
                    _Prepare();

                AudioSource.volume = _savedVolume;
                IsMuted = false;
            }
        }

        public void SetVolume(float volume)
        {
            if (AudioMode == VideoAudioOutputMode.AudioSource)
            {
                if (!IsInitialized)
                    _Prepare();

                AudioSource.volume = volume;
            }
        }

        public void Play()
        {
            if (!IsInitialized)
                _Prepare();
            if (!_videoPlayer.isPlaying)
                _videoPlayer.Play();
            State = VideoState.Playing;
        }

        public void Pause()
        {
            if (!IsInitialized)
                _Prepare();
            if (_videoPlayer.isPlaying)
                _videoPlayer.Pause();
            State = VideoState.Paused;
        }

        public void Stop()
        {
            if (!IsInitialized)
                _Prepare();

            _videoPlayer.sendFrameReadyEvents = true;
            _videoPlayer.Stop();
            State = VideoState.Stopped;
        }

        public void Load(string url)
        {
            Debug.LogError("Loading " + url);
            if (!System.IO.File.Exists(url))
            {
                Debug.LogError("video not found " + url);
                return;
            }
            if (!IsInitialized)
                _Prepare();

            Stop();
            _videoPlayer.source = VideoSource.Url;
            _videoPlayer.url = url;
            //_videoPlayer.prepareCompleted += OnVideoLoaded;
            _videoPlayer.sendFrameReadyEvents = true;
            _videoPlayer.frameReady += OnVideoLoadDone;
            foreach (Renderer target in TargetVideoPlanes)
            {
                target.enabled = false;
            }
            Play();
        }

        public void Load(VideoClip clip)
        {
            if (!IsInitialized)
                _Prepare();

            Stop();
            _videoPlayer.source = VideoSource.VideoClip;
            _videoPlayer.clip = clip;
            //_videoPlayer.prepareCompleted += OnVideoLoaded;
            _videoPlayer.sendFrameReadyEvents = true;
            _videoPlayer.frameReady += OnVideoLoadDone;
            foreach (Renderer target in TargetVideoPlanes)
            {
                target.enabled = false;
            }
            Play();
        }

        private void OnVideoLoadDone(VideoPlayer source, long frameIdx)
        {
            HidePlanes();
            _videoPlayer.frameReady -= OnVideoLoadDone;
            _videoPlayer.sendFrameReadyEvents = false;
            _videoPlayer.seekCompleted += StartVideoAlignment;
            Seek(0.1f);
            _videoPlayer.Pause();
        }

        void RefreshVideos()
        {
            // Raycaster.instance.RefreshVideoList();
        }

        void StartVideoAlignment(VideoPlayer source)
        {
            _videoPlayer.seekCompleted -= StartVideoAlignment;
            StartCoroutine("AlignVideoToMask");
        }

        IEnumerator AlignVideoToMask()
        {
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            yield return new WaitForEndOfFrame();
            Texture texture = _videoPlayer.texture;// TargetVideoPlanes[0].material.GetTexture("_Mask");
            Texture2D texture2D = new Texture2D(texture.width, texture.height, TextureFormat.RGBA32, false);
            //Texture2D outputTexture = new Texture2D(texture.width, texture.height / 2, TextureFormat.RGBA32, false);

            RenderTexture currentRendText = RenderTexture.active;
            RenderTexture renderTexture = new RenderTexture(texture.width, texture.height, 32);
            RenderTexture.active = renderTexture;

            Graphics.Blit(texture, renderTexture);
            texture2D.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0);
            texture2D.Apply();
            renderTexture.Release();
            Color[] pixels = texture2D.GetPixels();
            Color[] alphapixels = new Color[pixels.Length / 2];
            Array.Copy(pixels, 0, alphapixels, 0, pixels.Length / 2);

            RenderTexture.active = currentRendText;

            int height = texture.height / 2;
            int width = texture.width;
            int lastLine = height;
            //for (int i = alphapixels.Length; i >= width; i -= width)

            for (int i = 0; i < alphapixels.Length; i += width)
            {
                Color[] line = new Color[width];
                Array.Copy(alphapixels, i, line, 0, width);
                bool isMasked = true;
                for (int c = line.Length - 1; c >= 0; c--)
                {
                    Color col = line[c];
                    if (col.r > 0.6f || col.g > 0.6f || col.b > 0.6f)
                    {
                        isMasked = false;
                        break;
                    }
                }
                if (!isMasked)
                {
                    break;
                }
                lastLine--;
            }
            float proportion = 1 - (float)lastLine / (float)height;
            Vector3 oldPos = TargetVideoPlanes[0].transform.localPosition;
            Vector3 newPos = new Vector3(oldPos.x, TargetVideoPlanes[0].transform.localScale.y * (0.5f - proportion), oldPos.z);
            TargetVideoPlanes[0].transform.localPosition = newPos;
            ShowPlanes();
            OnVideoLoaded?.Invoke();
        }

        public void ResetVideo()
        {
            Seek(0.1f);
            _videoPlayer.Pause();
        }

        public void ShowPlanes()
        {
            foreach (Renderer target in TargetVideoPlanes)
            {
                target.enabled = true;
            }
        }

        public void HidePlanes()
        {
            foreach (Renderer target in TargetVideoPlanes)
            {
                target.enabled = false;
            }
        }

        public void Seek(int seconds)
        {
            if (!IsInitialized)
                _Prepare();

            _videoPlayer.time = seconds;
        }

        public void Seek(float percentage)
        {
            if (!IsInitialized)
                _Prepare();

            _videoPlayer.time = Length * percentage;
        }

        public void SetMask(Texture2D mask)
        {
            if (VideoType == VideoType.Masked)
            {
                TargetVideoPlanes.ToList().ForEach(t => t.material.SetTexture("_Mask", mask));
                TargetVideoImages.ToList().ForEach(t => t.material.SetTexture("_Mask", mask));
            }
        }

        protected void _Prepare()
        {
            if (!awakeMethodWasExecuted)
                Awake();

            _videoPlayer.playbackSpeed = PlaybackSpeed;

            _videoPlayer.source = VideoMode;
            if (VideoMode == VideoSource.VideoClip)
                _videoPlayer.clip = Clip;
            else if (!string.IsNullOrEmpty(URL))
                _videoPlayer.url = URL;

            _videoPlayer.audioOutputMode = AudioMode;
            if (AudioMode == VideoAudioOutputMode.AudioSource)
            {
                if (AudioSource == null)
                    AudioSource = gameObject.AddComponent<AudioSource>();
                _videoPlayer.SetTargetAudioSource(0, AudioSource);
            }

            _videoPlayer.sendFrameReadyEvents = true;

            if (TargetVideoImages == null)
                TargetVideoImages = new RawImage[0];
            if (TargetVideoPlanes == null)
                TargetVideoPlanes = new Renderer[0];

            defTexture = TargetVideoImages.Select(t => t.mainTexture).Concat(TargetVideoPlanes.Select(t => t.material.mainTexture)).Where(t => t != null).FirstOrDefault() as Texture2D;
            if (VideoType == VideoType.Alpha)
                alphaTexture = TargetVideoPlanes.Select(t => t.material.GetTexture("_Mask")).Where(t => t != null).FirstOrDefault() as Texture2D;

            _savedVolume = 1;
            IsInitialized = true;
            State = VideoState.None;
        }


    }

    [Serializable]
    public class VideoPlayerEvent : UnityEvent<VideoPlayerTW>
    {

    }
}
