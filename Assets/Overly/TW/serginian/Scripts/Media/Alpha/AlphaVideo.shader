﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "serginian/Video/Alpha video (splitted)" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" {}
		_Mask("Mask (RGB)", 2D) = "white" {}
	}

		SubShader{
		Tags{ "Queue" = "transparent" "RenderType" = "transparent" }
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
		Lighting Off

		Pass
	{
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag

#include "UnityCG.cginc"

	struct v2f {
		float4  pos : POSITION;
		float2  uv : TEXCOORD0;
	};

	sampler2D _MainTex;
	sampler2D _Mask;
	float4 _Mask_ST;
	float4 _MainTex_ST;

	v2f vert(v2f v)
	{
		v2f o;
		o.pos = UnityObjectToClipPos(v.pos);
		o.uv = v.uv;
		return o;
	}

	fixed4 frag(v2f i): COLOR
	{
		float2 uv2 = TRANSFORM_TEX(i.uv, _Mask);
		float2 uv1 = TRANSFORM_TEX(i.uv, _MainTex);
		fixed4 base = tex2D(_MainTex, uv1);
		base.a = tex2D(_Mask, uv2).b;
		return base;
	}

		ENDCG
		}

	}
}