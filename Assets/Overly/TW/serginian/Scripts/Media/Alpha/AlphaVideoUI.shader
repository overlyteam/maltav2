﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "serginian/Video/Alpha video UI (splitted)" {
	Properties{
		_MainTex("Base (RGB)", 2D) = "white" {}
	}

		SubShader{
		Tags{ "Queue" = "transparent" "RenderType" = "transparent" }
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
		Lighting Off

		Pass
	{
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag

#include "UnityCG.cginc"

	struct v2f {
		float4  pos : POSITION;
		float2  uv : TEXCOORD0;
	};

	sampler2D _MainTex;
	float4 _MainTex_ST;

	v2f vert(v2f v)
	{
		v2f o;
		o.pos = UnityObjectToClipPos(v.pos);
		o.uv = v.uv;
		return o;
	}

	fixed4 frag(v2f i): COLOR
	{
		float2 uv = i.uv;
		uv.y = uv.y/2;
		float2 uv_main = TRANSFORM_TEX(uv + float2(0,0.5), _MainTex);
		float2 uv_mask = TRANSFORM_TEX(uv, _MainTex);

		fixed4 base = tex2D(_MainTex, uv_main);
		base.a = tex2D(_MainTex, uv_mask).b;
		return base;
	}

		ENDCG
		}

	}
}