﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Overly.TWScripts
{
    [DisallowMultipleComponent]
    public class ContentLoader : MonoBehaviour
    {
        public static ContentLoader instance;
        public List<Content.ContentSection> contentDBList;
        public delegate void ContentLoaded();
        public ContentLoaded OnContentLoaded;
        // Start is called before the first frame update
        public List<GameObject> ARBuilderSpecificObjects;
        public List<GameObject> SceneBuilderSpecificObjects;


        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }
        void Start()
        {
            contentDBList = new List<Content.ContentSection>();
            if (GameSettings.instance.gameSettings != "")
            {
                EnableSpecificObjects(SPECIFIC_CONTENT.SCENE_BUILDER);
                string json = GameSettings.instance.gameSettings;
                Content.ContentSection sect = new Content.ContentSection();
                sect.videoList = new Content.VideoList[1];
                sect.videoList[0] = JsonUtility.FromJson<Content.VideoList>(json);
                sect.name = "Scenes";
                TWManager.instnance.contentType = TWManager.CONTENT_TYPE.SINGLE;

                createVideo createVideo = GetComponent<createVideo>();//access prefab components
                string[] urls = new string[sect.videoList[0].videos.Length];
                for (int i = 0; i < sect.videoList[0].videos.Length; i++)
                {
#if UNITY_ANDROID
                    urls[i] = System.IO.Path.Combine(Application.persistentDataPath, GameSettings.pathToARContent, sect.videoList[0].videos[i]);
#else
                urls[i] = System.IO.Path.Combine(Application.persistentDataPath, GameSettings.pathToARContent, sect.videoList[0].videos[i]);
#endif
                }
                createVideo.videoUrls = urls;
                //contentDBList.Add(sect);
            }
            else
            {
                EnableSpecificObjects(SPECIFIC_CONTENT.AR_BUILDER);
                string defaultContentDescriptionFile = Path.Combine(Application.persistentDataPath, GameSettings.pathToARContent, "default.json");
                if (File.Exists(defaultContentDescriptionFile))
                {
                    // has the default file - can generate default button
                    Content content = JsonUtility.FromJson<Content>(GameSettings.ReadDefaultSettings());

                    foreach (Content.ContentSection sect in content.availableContent)
                    {
                        contentDBList.Add(sect);
                    }
                    TWManager.instnance.contentType = TWManager.CONTENT_TYPE.DATABASE;
                }
                else
                {
                    Debug.Log("No data");
                    TWManager.instnance.contentType = TWManager.CONTENT_TYPE.NO_DATA;
                }
            }
            OnContentLoaded?.Invoke();
        }

        void EnableSpecificObjects(SPECIFIC_CONTENT content)
        {
            switch (content)
            {
                case SPECIFIC_CONTENT.AR_BUILDER:
                    foreach (GameObject obj in ARBuilderSpecificObjects)
                    {
                        obj.SetActive(true);
                    }

                    foreach (GameObject obj in SceneBuilderSpecificObjects)
                    {
                        obj.SetActive(false);
                    }
                    break;
                case SPECIFIC_CONTENT.SCENE_BUILDER:
                    foreach (GameObject obj in ARBuilderSpecificObjects)
                    {
                        obj.SetActive(false);
                    }

                    foreach (GameObject obj in SceneBuilderSpecificObjects)
                    {
                        obj.SetActive(true);
                    }
                    break;
            }
        }

        enum SPECIFIC_CONTENT { AR_BUILDER, SCENE_BUILDER};
    }
}