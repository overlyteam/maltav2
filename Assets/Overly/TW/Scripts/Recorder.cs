﻿using NatCorderU.Core;
using NatCorderU.Core.Clocks;
using NatCorderU.Core.Recorders;

//using  Overly.Drawing;
using System;
using System.Collections;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Events;

namespace Overly.TWScripts
{
    public class Recorder : MonoBehaviour
    {
        public Texture2D watermark;
        public GameObject GifLogo;
        public Container Format = Container.MP4;
        public int baseResolution = 1024;
        public int FPS = 30;
        public float MaxRecordTime = 15;
        public int CullingLayer = 10;
        public TextMeshProUGUI timerText;
        public bool RecordAudio = true;
        public bool IsRecording { get; private set; }
        public bool IsInLandscape { get { return Screen.width > Screen.height; } }

        private CameraRecorder videoRecorder;
        private AudioRecorder audioRecorder;
        private IClock recordingClock;
        private float time;

        /**************************************** PUBLIC INTERFACE *******************************************/

        public UnityEvent OnCaptureStart;
        [Serializable]
        public class ScreenshotTakenEvent : UnityEvent<Texture2D> { }
        public ScreenshotTakenEvent OnScreenshotTaken;
        [Serializable]
        public class VideoTakenEvent : UnityEvent<string> { }
        public VideoTakenEvent OnVideoTaken;

        public void StartRecording()
        {
            if (!IsRecording)
            {
                OnCaptureStart.Invoke();
                float x = baseResolution;
                float y = baseResolution * (IsInLandscape ? Convert.ToSingle(Screen.height) / Convert.ToSingle(Screen.width) : Convert.ToSingle(Screen.width) / Convert.ToSingle(Screen.height)); // less than x
                var width = Convert.ToInt32(IsInLandscape ? x : y);
                var height = Convert.ToInt32(IsInLandscape ? y : x);
                Debug.LogWarning(width + "x" + height);

                var videoFormat = new VideoFormat(width, height, FPS);
                var audioFormat = (RecordAudio && Format == Container.MP4) ? AudioFormat.Unity : AudioFormat.None;
                recordingClock = new RealtimeClock();

                NatCorder.StartRecording(Format, videoFormat, audioFormat, Done);


                Camera recordCamera = GameObject.FindGameObjectWithTag("RecordCamera").GetComponent<Camera>();
                videoRecorder = CameraRecorder.Create(recordCamera, recordingClock);
                
                if (Format == Container.GIF)
                    videoRecorder.recordEveryNthFrame = 5;

                if (RecordAudio)
                    audioRecorder = AudioRecorder.Create(Camera.main.GetComponent<AudioListener>(), recordingClock);

                Camera.main.cullingMask = Camera.main.cullingMask;// | (1 << CullingLayer);
                IsRecording = true;
            }
        }

        public void StopRecording()
        {
            if (IsRecording)
            {
                Debug.Log("StopRecording");
                time = 0;
                audioRecorder?.Dispose();
                audioRecorder = null;
                videoRecorder?.Dispose();
                videoRecorder = null;
                NatCorder.StopRecording();
                Camera.main.cullingMask = Camera.main.cullingMask;// & ~(1 << CullingLayer);
                IsRecording = false;
            }
        }

        public void TakeSceenshot()
        {
            OnCaptureStart.Invoke();
            StartCoroutine(takeScreenshot());
        }



        /****************************************** INTERNAL LOGIC ********************************************/

        private void Done(string path)
        {
            Debug.Log("Video recorded: " + path);

            OnVideoTaken?.Invoke(path);
        }

        private IEnumerator takeScreenshot()
        {
            yield return new WaitForEndOfFrame();
            Texture2D _texture = new Texture2D(Screen.width, Screen.height, TextureFormat.RGB24, true);
            _texture.ReadPixels(new Rect(0f, 0f, Screen.width, Screen.height), 0, 0, false);
            _texture.Apply();
            if (watermark != null)
            {
                _texture = Draw.AddWatermark(_texture, watermark);
            }
            Debug.LogError("Got texture");
            OnScreenshotTaken?.Invoke(_texture);
        }

        private void Update()
        {
            if (IsRecording)
            {
                if (time >= MaxRecordTime)
                {
                    StopRecording();
                }
                else
                {
                    time += Time.deltaTime;
                    timerText.text = "Recording " + time.ToString("00.0");
                }
            }
        }

    }

}
