﻿using UnityEngine;

namespace Overly.TWScripts
{

    internal static class Draw
    {
        internal static Texture2D RotateTexture(Texture2D source)
        {
            Color32[] pix = source.GetPixels32();
            System.Array.Reverse(pix);
            Texture2D destTex = new Texture2D(source.width, source.height);
            destTex.SetPixels32(pix);
            destTex.Apply();
            return destTex;
        }

        internal static Texture2D AddWatermark(Texture2D background, Texture2D watermark)
        {

            int startX = background.width - watermark.width - 25;
            int startY = 15;// background.height - watermark.height - 15;
                            //175 & 395
            for (int x = startX; x < background.width - 25; x++)
            {
                for (int y = startY; y < 15 + watermark.height - 394; y++)
                {
                    Color bgColor = background.GetPixel(x, y);
                    Color wmColor = watermark.GetPixel(x - startX + 0, y - startY + 0);
                    if (wmColor.a == 0)
                        continue;
                    Color final_color = Color.Lerp(bgColor, wmColor, wmColor.a / 2.0f);

                    background.SetPixel(x, y, final_color);
                }
            }

            background.Apply();
            return background;
        }
    }

}