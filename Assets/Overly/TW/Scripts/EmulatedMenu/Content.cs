﻿public class Content
{
    public string game;
    public ContentSection[] availableContent;

    [System.Serializable]
    public class VideoList
    {
        public string name;
        public string thumbnail;
        public string[] videos;
    }

    [System.Serializable]
    public class ContentSection
    {
        public string name;
        public VideoList[] videoList;
    }
}
