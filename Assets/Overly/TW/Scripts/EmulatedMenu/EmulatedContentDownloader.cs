﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;
using UnityEngine.Networking;

public partial class EmulatedContentDownloader : MonoBehaviour
{
    public GameObject buttonPrefab;
    public GameObject labelPrefab;
    public GameObject ButtonContainer;
    public GameObject LoadingUI;

    Image loadingImage;
    TextMeshProUGUI loadingText;

    private float loadingAmount;

    string levelToReturnTo = "";

    public float LoadingAmount
    {
        get => loadingAmount;
        set
        {
            loadingAmount = value;
            loadingImage.fillAmount = loadingAmount;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        loadingImage = LoadingUI.GetComponentInChildren<Image>();
        loadingText = LoadingUI.GetComponentInChildren<TextMeshProUGUI>();
        levelToReturnTo = GameSettings.instance.LevelToReturnTo;
        GameSettings.instance.SetReturnLevel(SceneManager.GetActiveScene().name);
        CheckContentAndGenerateButtons();
    }

    public void OnBackButtonPressed()
    {
        SceneManager.LoadScene("TestMenu");
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void SetLoading(bool active)
    {
        LoadingAmount = 0;
        LoadingUI.SetActive(active);
    }

    public void LoadLevel(string name)
    {
        Content content = JsonUtility.FromJson<Content>(GameSettings.ReadDefaultSettings());

        foreach (Content.ContentSection sect in content.availableContent)
        {
            if (sect.name == "Scenes")
            {
                string path = Path.Combine(Application.persistentDataPath, GameSettings.pathToARContent, "videos", sect.name);
                foreach (Content.VideoList videoList in sect.videoList)
                {
                    if (videoList.name == name)
                    {
                        GameSettings.instance.gameSettings = JsonUtility.ToJson(videoList);
                        SceneManager.LoadScene("TimeWindows");
                        return;
                    }
                }
            }
        }
        GameSettings.instance.gameSettings = "";
        SceneManager.LoadScene("TimeWindows");
    }

    public void DownloadMissingFiles()
    {
        StartCoroutine(DownloadFiles());
    }

    IEnumerator DownloadFiles()
    {
        string path = Path.Combine(Application.streamingAssetsPath, "videoDescriptors");
        Debug.Log(path);
        string dataAsJson = "";
        // get "online" descriptor
#if UNITY_ANDROID
        using (UnityWebRequest uwr = UnityWebRequest.Get(path))
        {
            yield return uwr.SendWebRequest();
            if (uwr.isNetworkError || uwr.isHttpError)
            {
                Debug.LogError("www error:" + uwr.error + " " + path);
                yield break; // exit if no  file
            }
            else
            {
                Debug.Log(uwr.downloadHandler.text);
                dataAsJson = uwr.downloadHandler.text;
            }
        }
#elif UNITY_IOS
        // iOS can access StreamingAssets folder as normal files
        if (File.Exists(path))
        {
            dataAsJson = File.ReadAllText(path);
        }
        
#endif
        Content cloudDescriptor = JsonUtility.FromJson<Content>(dataAsJson);

        // download data and create local descriptor
        Content content = new Content();
        content.game = "AR Builder";
        content.availableContent = new Content.ContentSection[cloudDescriptor.availableContent.Length];
        float contentAmount = (float)1 / (float)cloudDescriptor.availableContent.Length;
        SetLoading(true);
        for (int i = 0; i < cloudDescriptor.availableContent.Length; i++)
        {
            // sections - Objects, poses, scenes
            yield return new WaitForSeconds(0.1f);
            string sectionName = cloudDescriptor.availableContent[i].name; // get top directory name
            Content.VideoList[] sectionContent = cloudDescriptor.availableContent[i].videoList;

            content.availableContent[i] = new Content.ContentSection();
            content.availableContent[i].name = sectionName;
            content.availableContent[i].videoList = new Content.VideoList[sectionContent.Length];
            loadingText.text = "Loading " + sectionName;
            float sectionAmount = (float)contentAmount / (float)sectionContent.Length;
            for (int j = 0; j < sectionContent.Length; j++)
            {
                string sectionContentName = sectionContent[j].name;
                string sectionThumbnail = sectionContent[j].thumbnail;
                string[] videos = sectionContent[j].videos;

                content.availableContent[i].videoList[j] = new Content.VideoList();
                content.availableContent[i].videoList[j].name = sectionContentName;
                content.availableContent[i].videoList[j].videos = new string[videos.Length];
                Debug.Log(" # " + sectionContentName + " " + videos.Length);

                if (sectionThumbnail != null)
                {
                    // copy thumbnail
                    string thumbnailPath = sectionThumbnail;
                    string thumbnailSourcePath = Path.Combine(Application.streamingAssetsPath, sectionThumbnail);
                    string thumbnailDestPath = Path.Combine(Application.persistentDataPath, GameSettings.pathToARContent, thumbnailPath);
#if UNITY_ANDROID
                    thumbnailPath = thumbnailPath.Replace("\\", "/");
                    thumbnailSourcePath = thumbnailSourcePath.Replace("\\", "/");
                    thumbnailDestPath = thumbnailDestPath.Replace("\\", "/");
#elif UNITY_IOS
                    thumbnailPath = thumbnailPath.Replace(@"\", @"/");
                    thumbnailSourcePath = thumbnailSourcePath.Replace(@"\", @"/");
                    thumbnailDestPath = thumbnailDestPath.Replace(@"\", @"/");
#endif
                    content.availableContent[i].videoList[j].thumbnail = thumbnailPath;
                    Debug.Log("Copy");
                    Debug.Log("From  - " + thumbnailSourcePath);
                    Debug.Log("To    - " + thumbnailDestPath);
                    if (!File.Exists(thumbnailDestPath))
                    {
                        if (!Directory.Exists(Path.GetDirectoryName(thumbnailDestPath))) // if there is no directory, then create it
                        {
                            Directory.CreateDirectory(Path.GetDirectoryName(thumbnailDestPath));
                        }
                    }
                    else
                    {
                        File.Delete(thumbnailDestPath);
                        Debug.Log("OldF thumbnaail deleted");
                    }
                    // copy files
#if UNITY_ANDROID
                    UnityWebRequest uwr = new UnityWebRequest(thumbnailSourcePath);
                    uwr.downloadHandler = new DownloadHandlerBuffer();
                    yield return uwr.SendWebRequest();
                    if (uwr.isNetworkError || uwr.isHttpError)
                    {
                        Debug.Log("#### " + uwr.error);
                    }
                    else
                    {
                        File.WriteAllBytes(thumbnailDestPath, uwr.downloadHandler.data);
                    }
#elif UNITY_IOS
                    File.Copy(thumbnailSourcePath, thumbnailDestPath);
                    if (File.Exists(thumbnailDestPath))
                    {
                        Debug.Log("New thumbnail Copied");
                    }
                    else
                    {
                        Debug.Log("Failed to copy");
                    }
#endif
                }

                float videosAmount = (float)sectionAmount / (float)videos.Length;
                // copy videos
                for (int k = 0; k < videos.Length; k++)
                {
                    Debug.Log(i.ToString() + "." + j.ToString() + "." + k.ToString());
                    string videoPath = videos[k];
                    string videoSourcePath = Path.Combine(Application.streamingAssetsPath, videos[k]);
                    string videoDestPath = Path.Combine(Application.persistentDataPath, GameSettings.pathToARContent, videoPath);
#if UNITY_ANDROID
                    videoPath = videoPath.Replace("\\", "/");
                    videoSourcePath = videoSourcePath.Replace("\\", "/");
                    videoDestPath = videoDestPath.Replace("\\", "/");
#elif UNITY_IOS
                    videoPath = videoPath.Replace(@"\", @"/");
                    videoSourcePath = videoSourcePath.Replace(@"\", @"/");
                    videoDestPath = videoDestPath.Replace(@"\", @"/");

#endif
                    Debug.Log("Copy");
                    Debug.Log("From  - " + videoSourcePath);
                    Debug.Log("To    - " + videoDestPath);
                    content.availableContent[i].videoList[j].videos[k] = videoPath;// videoDestPath;

                    if (!File.Exists(videoDestPath))
                    {
                        if (!Directory.Exists(Path.GetDirectoryName(videoDestPath))) // if there is no directory, then create it
                        {
                            Directory.CreateDirectory(Path.GetDirectoryName(videoDestPath));
                        }

                        // copy files
#if UNITY_ANDROID
                        UnityWebRequest uwr = new UnityWebRequest(videoSourcePath);
                        uwr.downloadHandler = new DownloadHandlerBuffer();
                        yield return uwr.SendWebRequest();
                        if (uwr.isNetworkError || uwr.isHttpError)
                        {
                            Debug.Log("#### " + uwr.error);
                        }
                        else
                        {
                            File.WriteAllBytes(videoDestPath, uwr.downloadHandler.data);
                        }
#elif UNITY_IOS
                        File.Copy(videoSourcePath, videoDestPath);
                        if (File.Exists(videoDestPath))
                        {
                            Debug.Log("VideoCopied");
                        }
                        else
                        {
                            Debug.Log("Failed to copy");
                        }
#endif

                    }
                    LoadingAmount += videosAmount;
                }
            }
        }
        SaveDescription(JsonUtility.ToJson(content));
        CheckContentAndGenerateButtons();

        SetLoading(false);
    }

    public void CheckContentAndGenerateButtons()
    {
        foreach (Transform child in ButtonContainer.transform)
        {
            Destroy(child.gameObject);
        }

        string defaultContentDescriptionFile = Path.Combine(Application.persistentDataPath, GameSettings.pathToARContent, "default.json");
        if (File.Exists(defaultContentDescriptionFile))
        {
            // has the default file - can generate default button
            CreateButton("AR Builder");
            Content content = JsonUtility.FromJson<Content>(GameSettings.ReadDefaultSettings());

            foreach (Content.ContentSection sect in content.availableContent)
            {
                if (sect.name == "Scenes")
                {
                    foreach (Content.VideoList videoList in sect.videoList)
                    {
                        CreateButton(videoList.name);
                    }
                }
            }
        }
        else
        {
            CreateLabel("Please, download the content!");
        }
    }

    public void CreateButton(string name)
    {
        GameObject btnObject = Instantiate(buttonPrefab, ButtonContainer.transform);
        Button button = btnObject.GetComponent<Button>();
        button.onClick.AddListener(() => { LoadLevel(name); });
        TextMeshProUGUI txtButton = btnObject.GetComponentInChildren<TextMeshProUGUI>();
        txtButton.text = name.Replace("_", " ");
    }

    public void CreateLabel(string text)
    {
        GameObject lblObject = Instantiate(labelPrefab, ButtonContainer.transform);
        TextMeshProUGUI txtLabel = lblObject.GetComponent<TextMeshProUGUI>();
        txtLabel.text = text.Replace("_", " ");
    }

    public void EraseContent()
    {
        string defaultContentDescriptionFile = Path.Combine(Application.persistentDataPath, GameSettings.pathToARContent, "default.json");
        if (File.Exists(defaultContentDescriptionFile))
        {
            string json;
            using (StreamReader reader = new StreamReader(defaultContentDescriptionFile))
            {
                json = reader.ReadToEnd();
                reader.Close();
            }
            Content content = JsonUtility.FromJson<Content>(json);

            foreach (Content.ContentSection sect in content.availableContent)
            {
                string path = Path.Combine(Application.persistentDataPath, GameSettings.pathToARContent, "Videos", sect.name);
                if (Directory.Exists(path))
                {
                    Directory.Delete(path, true);
                }
            }
        }
        File.Delete(defaultContentDescriptionFile);
        CheckContentAndGenerateButtons();
    }

    void SaveDescription(string json)
    {
        string defaultContentDescriptionFile = Path.Combine(Application.persistentDataPath, GameSettings.pathToARContent, "default.json");
        if (!Directory.Exists(Path.Combine(Application.persistentDataPath, GameSettings.pathToARContent)))
        {
            Directory.CreateDirectory(Path.Combine(Application.persistentDataPath, GameSettings.pathToARContent));
        }
        // if theres no default description, we should create it
        StreamWriter writer = new StreamWriter(defaultContentDescriptionFile, false);
        writer.Write(json);
        writer.Close();
        Debug.Log("Data saved");

    }
}
