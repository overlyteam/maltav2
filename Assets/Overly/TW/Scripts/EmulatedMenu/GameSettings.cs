﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[DisallowMultipleComponent]
public class GameSettings : MonoBehaviour
{
    public static string pathToARContent = "ARContent"; // in application.persistantdatapath
    public static GameSettings instance;
    public string gameSettings;
    private string levelToReturnTo;

    public string LevelToReturnTo { get => levelToReturnTo;}

    // Start is called before the first frame update
    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void SetReturnLevel(string levelName)
    {
        levelToReturnTo = levelName;
    }

    public static string ReadDefaultSettings()
    {
        string defaultContentDescriptionFile = Path.Combine(Application.persistentDataPath, pathToARContent, "default.json");
        string json;
        using (StreamReader reader = new StreamReader(defaultContentDescriptionFile))
        {
            json = reader.ReadToEnd();
            reader.Close();
        }
        return json;
    }
}
