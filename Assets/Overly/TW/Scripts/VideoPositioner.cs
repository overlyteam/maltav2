﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using serginian.Media;

namespace Overly.TWScripts
{
    public class VideoPositioner : MonoBehaviour
    {
        // Start is called before the first frame update
        void Start()
        {

        }

        // Update is called once per frame
        void Update()
        {

        }
        public void PlaceVideo(string[] videoUrls)
        {
            VideoSet videoSet = GetComponentInChildren<VideoSet>();

            if (videoSet.videosToRotate == null)
            {
                videoSet.videosToRotate = new List<string>();
            }
            foreach (string videoUrl in videoUrls)
            {
                videoSet.videosToRotate.Add(videoUrl);
            }
            videoSet.Reset();

            Transform trVideoSet = videoSet.transform;
            trVideoSet.position = transform.position + transform.up * 2;
            trVideoSet.rotation = transform.rotation;
            trVideoSet.localScale = new Vector3(trVideoSet.localScale.x * 10, trVideoSet.localScale.y * 10, 1);

            videoSet.GetComponent<VideoPlayerTW>().HidePlanes();
            //videoSet.GetComponent<VideoPlayerEx>().OnVideoLoaded += PositionVideo;
            videoSet.GetComponent<videoProperties>().nrOfVideoAssigned = videoUrls.Length;
            //videoLayer.GetComponent<videoProperties>().useCharacterVideos = true;
            videoSet.GetComponent<videoProperties>().createCharacterVideo();
        }

        public void PositionVideo()
        {
            VideoPlayerTW videoPlayer = GetComponentInChildren<VideoPlayerTW>();
            MeshRenderer videoPlane = (MeshRenderer)videoPlayer.TargetVideoPlanes[0];
            Texture2D tex = videoPlane.material.GetTexture("_Mask") as Texture2D;

            Color32[] pixels = tex.GetPixels32();
            int[] rows = new int[tex.height];
            for (int i = 0; i < tex.height; i++)
            {
                int col = 0;
                for (int j = 0; j < tex.width; j++)
                {
                    col += pixels[j + i * tex.width].r;
                }
                rows[i] = col / tex.width;
            }
            int row = 0;
            for (int i = 0; i < rows.Length; i++)
            {
                if (rows[i] > 0)
                {
                    row = i;
                    break;
                }
            }
            float delta = (float)row / tex.height;
            videoPlayer.transform.position += new Vector3(0, delta, 0) * videoPlayer.transform.localScale.y;
        }
    }
}