﻿using UnityEngine.Events;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using TMPro;

namespace Overly.TWScripts
{
    //USED TO POPULATE CHARACTER AND OBJECT GRID WITH BUTTON PREFABS
    public class populateGrid : MonoBehaviour
    {
        public string sectionName;
        public Sprite sprite;
        public GameObject prefab_gridElement;
        public UnityEvent OnChildClicked;
        public populateSubGrid subgrid;

        bool populated = false;
        private void Awake()
        {
            ContentLoader.instance.OnContentLoaded += PopulateGridWithContent;
        }


        private void Update()
        {
            if (!populated)
            {
                PopulateGridWithContent();
            }
        }

        void PopulateGridWithContent()
        {
            if (ContentLoader.instance.contentDBList == null || ContentLoader.instance.contentDBList?.Count == 0)
            {
                Debug.LogError("Empty video content list of DB!");
                return;
            }

            Content.ContentSection mySect = null;
            foreach (Content.ContentSection sect in ContentLoader.instance.contentDBList)
            {
                if (sect.name == sectionName)
                {
                    mySect = sect;
                }
            }
            if (mySect == null)
            {
                Destroy(gameObject);
                return;
            }

            GameObject newObj;
            foreach (Content.VideoList videoList in mySect.videoList)
            {
                newObj = (GameObject)Instantiate(prefab_gridElement, transform);

                string thumbnailUrl = "";
#if UNITY_ANDROID
                thumbnailUrl = System.IO.Path.Combine(Application.persistentDataPath, GameSettings.pathToARContent, videoList.thumbnail);
#else
                thumbnailUrl = System.IO.Path.Combine(Application.persistentDataPath, GameSettings.pathToARContent, videoList.thumbnail);
#endif
                byte[] data = File.ReadAllBytes(thumbnailUrl);
                Texture2D texture = new Texture2D(64, 64, TextureFormat.ARGB32, false);
                texture.LoadImage(data);
                Sprite thumbnail = Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f));

                newObj.GetComponent<Image>().sprite = thumbnail;
                
               // newObj.GetComponent<Image>().color = Random.ColorHSV(0, 1, 0.5f, 1, 0.4f, 0.6f);
                //newObj.GetComponent<Button>().onClick.AddListener(OnChildClicked.Invoke);
                newObj.GetComponent<Button>().onClick.AddListener(delegate { subgrid.PopulateGridWithContent(videoList); OnChildClicked.Invoke(); });

                //                createVideo createVideo = newObj.GetComponent<createVideo>();//access prefab components
                //                string[] urls = new string[videoList.videos.Length];
                //                for (int i = 0; i < videoList.videos.Length; i++)
                //                {
                //#if UNITY_ANDROID
                //                    urls[i] = System.IO.Path.Combine(Application.persistentDataPath, GameSettings.pathToARContent, videoList.videos[i]);
                //#else
                //                urls[i] = System.IO.Path.Combine(Application.persistentDataPath, GameSettings.pathToARContent, videoList.videos[i]);
                //#endif

                //                }
                //                createVideo.videoUrls = urls;
                TextMeshProUGUI element_name_text = newObj.GetComponentInChildren<TextMeshProUGUI>();
                element_name_text.text = videoList.name;

            }
            populated = true;
        }
    }
}