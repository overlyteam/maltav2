﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Overly.TWScripts
{
public class videoManager : MonoBehaviour
{
    [HideInInspector]public GameObject playIcon;
    [HideInInspector]public GameObject binIcon;

    void Start()
    {
        Screen.sleepTimeout = (int)SleepTimeout.NeverSleep;

        if (playIcon == null)
            playIcon = GameObject.FindGameObjectWithTag("playIcon");
        if (binIcon == null)
            binIcon = GameObject.FindGameObjectWithTag("binIcon");

        //print("play and bin icon: " + playIcon.name + " " + binIcon.name);

        playIcon?.SetActive(false);
        binIcon?.SetActive(false);
    }





    void Update()
    {

    }

    public void switchToBinIcon()
    {
        //playIcon.SetActive(false);
        //binIcon.SetActive(true);
    }

    public void switchToPlayIcon()
    {
        playIcon.SetActive(true);
        binIcon.SetActive(false);
    }
}
}