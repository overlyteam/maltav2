﻿using serginian.Media;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using UnityEngine.XR.ARFoundation;

namespace Overly.TWScripts
{
    public class createVideo : MonoBehaviour
    {
        [HideInInspector] public string[] videoUrls;


        [HideInInspector] public int nrOfVideoAssigned;
        [HideInInspector] public int nrOfVideoAssigned_object;
        [HideInInspector] public int nrOfVideoAssigned_scene;
        public GameObject prefVideoPositioner;
        public GameObject prefVideoContainer;
        public videoHolder videoLayerHolder;
        public GameObject videoLayer_prefab_1;
        public GameObject videoLayer_prefab_2;
        public GameObject videoLayer_prefab_3;
        [HideInInspector] public GameObject maxObjectsReached_warning;

        private Camera arCamera;
        private ARSessionOrigin AROrigin;
        [HideInInspector] public static int nrOfVideosCreated;
        [HideInInspector] public int nrOfVideosCreated_forTouchManager;
        private bool maxObjectsReached;

        ARRaycastManager arRayCastManager;

        private void Awake()
        {
            arRayCastManager = FindObjectOfType<ARRaycastManager>();
        }

        void Start()
        {
            videoLayerHolder = FindObjectOfType<videoHolder>();
            AROrigin = FindObjectOfType<ARSessionOrigin>();
            arCamera = Camera.main;
            nrOfVideosCreated = 0;
        }


        void Update()
        {
            maxObjectsReached = FindObjectOfType<videoHolder>().hasReachedMaxLayers;//check if have reached max

            nrOfVideosCreated_forTouchManager = nrOfVideosCreated;//how many created videos?
        }

        public void ShowVideo()
        {
            ShowElement();
        }


        public void ShowElement()
        {
            if (!maxObjectsReached)//if havent reached max video layers, then we can create more
            {
                Vector3 videoPosition = arCamera.transform.forward * 0.2f;//make video appear x units in front of camera
                Vector3 videoRotation = new Vector3(0f, arCamera.transform.eulerAngles.y, 0f);//turn video against camera

                Vector3 spawnPoint;
                //Vector3 spawnPoint = arCamera.ScreenToWorldPoint(new Vector3(Screen.width / 2f, Screen.height / 2f, 0.5f));//spawn video in the middle of the screen 0.5units away from camera

                Ray cameraRay = Camera.main.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
                bool intersects = TWManager.instnance.infiniteGroundPlane.Raycast(cameraRay, out float dist); // intersects groundplane

                if (intersects && dist > 0 && dist < 6)
                {
                    spawnPoint = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, dist)); // spawn video on the ground in the center of viewport
                }
                else
                {
                    Vector3 rayOrigin = Camera.main.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 5f)); // 5 meters forward
                    Ray ray = new Ray(rayOrigin, Vector3.down);
                    intersects = TWManager.instnance.infiniteGroundPlane.Raycast(ray, out dist);
                    if (intersects)
                    {
                        spawnPoint = rayOrigin + Vector3.down * dist; // spawn video on the ground 5m before you
                    }
                    else
                    {
                        spawnPoint = arCamera.ViewportToWorldPoint(new Vector3(0.5f, 0.5f, 5f)); // spawn video 5m forward
                    }
                }


                GameObject videoPositioner = Instantiate(prefVideoPositioner, spawnPoint, Quaternion.Euler(videoRotation), AROrigin.trackablesParent.transform);

                videoPositioner.GetComponent<VideoPositioner>().PlaceVideo(videoUrls);
            }
            else //here we have 3 video layers already, so show warning, that we have reached max number of videos
            {
                videoLayerHolder.showWarning = true;
            }
        }

    }
}