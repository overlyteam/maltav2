﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.ARFoundation;
using UnityEngine.SceneManagement;

namespace Overly.TWScripts
{
    public class ScanFloor : MonoBehaviour
    {
        enum STATES { SCAN, SCANNED };
        STATES state = STATES.SCAN;

        [Header("UI")]
        public Image imgPercentageDone;
        public GameObject scanMessage;
        public GameObject placeElement_screen;
        public GameObject charactersButton;
        public GameObject objectsButton;
        [Header("Debug UI")]
        public GameObject dbgDoneScaningButton;
        [Header("Controls")]
        public bool scan = true;
        [Header("Values")]
        public ARPlane groundPlane;
        public ARPlane largestPlane;


        private ARPlaneManager planeManager;
        private ARRaycastManager raycastManager;
        private touchManager touchManager;


        private ARPlane tempPlane;

        private void OnApplicationFocus(bool focus)
        {
            if (groundPlane != null)
            {
                //  SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
            }
        }

        public void ReloadScene()
        {
            foreach (VideoPositioner video in FindObjectsOfType<VideoPositioner>())
            {
                Destroy(video.gameObject);
            }
            ShowButtons(false);
            touchManager.enabled = false;
            ShowScanProgress(true);
            InitScanning();
        }
        private void Awake()
        {
            planeManager = FindObjectOfType<ARPlaneManager>();
            raycastManager = FindObjectOfType<ARRaycastManager>();
            touchManager = gameObject.GetComponent<touchManager>();
            touchManager.enabled = false;

        }
        // Start is called before the first frame update
        void Start()
        {
            ShowScanProgress(true);
            InitScanning();
        }


        public void SkipGroundCheck()
        {
            if (groundPlane == null && largestPlane != null)
            {
                groundPlane = largestPlane;
            }
            if (groundPlane == null)
            {
                DoneScanning();
            }
        }
        private void CheckPlanes(ARPlanesChangedEventArgs planes)
        {
            foreach (ARPlane plane in planes.added)
            {
            }
            float fillAmount = 0;
            foreach (ARPlane plane in planes.updated)
            {
                if (largestPlane == null)
                {
                    largestPlane = plane;
                }
                else if (plane.size.x * plane.size.y > largestPlane.size.x * largestPlane.size.y)
                {
                    largestPlane = plane;
                }
                plane.gameObject.SetActive(true);
                if (plane.size.x > 1.5f && plane.size.y > 1.5f && plane.isActiveAndEnabled)
                {
                    groundPlane = plane;
                    return;
                }
                else
                {
                    if (plane.isActiveAndEnabled)
                    {
                        float tempfillAmount = (plane.size.x / 1.5f) * (plane.size.y / 1.5f) * 0.9f;
                        if (tempfillAmount > fillAmount)
                        {
                            if (Debug.isDebugBuild)
                            {
                                if (tempPlane == null)
                                {
                                    ShowDebugButtons();
                                }
                            }
                            tempPlane = plane;
                            fillAmount = tempfillAmount;
                        }
                    }
                }
            }
            if (groundPlane != null)
            {
                fillAmount = 1;
            }
            else
            {
                fillAmount *= 0.9f;
            }
            imgPercentageDone.fillAmount = fillAmount;
        }

        void ShowDebugButtons(bool show = true)
        {
            if (Debug.isDebugBuild)
            {
                dbgDoneScaningButton.SetActive(show);
            }
        }

        // Update is called once per frame
        void Update()
        {
            if (scan)
            {
                if (groundPlane != null)
                {
                    DoneScanning();
                }
            }
        }


        public void InitScanning()
        {
            groundPlane = null;
            HideUnusedPlanes();
            planeManager.planesChanged += CheckPlanes;
            planeManager.enabled = true;
            scanMessage.SetActive(true);
            scan = true;
            touchManager.enabled = false;
            placeElement_screen.SetActive(false);
        }

        public void DoneScanning()
        {
            if (Debug.isDebugBuild)
            {
                ShowDebugButtons(false);
            }
            //HideGroundPlane();
            HideUnusedPlanes();
            planeManager.planesChanged -= CheckPlanes;
            planeManager.enabled = false;
            scanMessage.SetActive(false);
            scan = false;
            touchManager.enabled = true;
            ShowScanProgress(false);
            placeElement_screen.SetActive(true);
        }

        public void ShowButtons(bool show)
        {
            charactersButton.SetActive(show);
            objectsButton.SetActive(show);
        }

        public void ShowScanProgress(bool show)
        {
            imgPercentageDone.color = Color.white;
            imgPercentageDone.fillAmount = 0;
            imgPercentageDone.gameObject.SetActive(show);
        }

        public void HideGroundPlane()
        {
            TWManager.instnance.infiniteGroundPlane = groundPlane.infinitePlane;
            groundPlane.gameObject.SetActive(false);
            //foreach (Renderer r in groundPlane.GetComponents<Renderer>())
            //{
            //    Debug.LogError(r.name);
            //    r.enabled = false;
            //}
        }
        public void ShowGroundPlane()
        {
            //{
            //    foreach (Renderer r in groundPlane.GetComponents<Renderer>())
            //    {
            //        r.enabled = true;
            //    }
        }

        void HideUnusedPlanes()
        {

            foreach (ARPlane plane in planeManager.trackables)
            {
                if (plane != groundPlane)
                {
                    plane.gameObject.SetActive(false);
                }
            }

        }
    }
}