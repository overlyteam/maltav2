﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR.ARFoundation;
using TMPro;
using UnityEngine.EventSystems;

namespace Overly.TWScripts
{
    public class scanTutorial : MonoBehaviour
    {
        private Camera mainCamera;
        private bool isRotated;
        private float CamerasAngle;
        private touchManager touchManager;

        private bool firstScreenNext;
        private bool phoneRotated;
        private List<ARRaycastHit> hits;

        public ARPlaneManager planeManager;
        public ARRaycastManager raycastManager;

        public GameObject positionAsInImage;
        public GameObject positionIMG;
        public GameObject keepRotatingIMG;

        public GameObject intructionsToLowerPhone;

        public GameObject objectToInstantiate;
        public GameObject hasScannedFloorUI;

        public GameObject instructionsToUpwardPhone;
        public GameObject placeElement_screen;
        public GameObject howToInsertElements;
        public GameObject charactersButton;
        public GameObject scenesButton;
        public GameObject objectsButton;
        public GameObject closeAR_button;
        public GameObject infoButton;
        public GameObject infoDescription;

        private bool hasCompletedAll = false;

        void Start()
        {
            mainCamera = Camera.main;

            CamerasAngle = mainCamera.transform.eulerAngles.z;

            touchManager = gameObject.GetComponent<touchManager>();
            touchManager.enabled = false;

            positionAsInImage.SetActive(true);
            positionIMG.SetActive(true);

            firstScreenNext = false;
            phoneRotated = false;
            planeManager.enabled = false;
            isRotated = false;

            intructionsToLowerPhone.SetActive(false);
            keepRotatingIMG.SetActive(false);
            objectToInstantiate.SetActive(false);
            hasScannedFloorUI.SetActive(false);
            instructionsToUpwardPhone.SetActive(false);
            placeElement_screen.SetActive(false);
            howToInsertElements.SetActive(false);
            charactersButton.SetActive(false);
            objectsButton.SetActive(false);
            scenesButton.SetActive(false);
            closeAR_button.SetActive(false);
            hits = new List<ARRaycastHit>();

        }

        void Update()
        {
            screenTilt();//first screen = rotate in landscape phone

            if (phoneRotated && !hasCompletedAll)//seconds screen = scanning floor process
                calibrateFloor();
        }


        public void readyToRotateButton()
        {
            //Use delay so that when we touch button, program doesnt register is as touch for the next method
            StartCoroutine(delayReadyToRotate(0.2f));
            print("READY TO ROTATE!");
        }

        IEnumerator delayReadyToRotate(float time)
        {
            yield return new WaitForSeconds(time);
            firstScreenNext = true;
        }

        void screenTilt()//**** TILT PHONE HORIZONTALLY (IN LANDSCAPE MODE)
        {
            if (phoneRotated)//skip everything further, if we have already done this
            {
                //print("at return");
                return;
            }

            if (firstScreenNext)//if we pressed the first button
            {
                //print("here");
                touchManager.enabled = false;//disable touchManager script, so program doesnt process inputs
                CamerasAngle = mainCamera.transform.eulerAngles.z;//get phones rotation

                //closeAR_button.SetActive(true);

                //delayPhoneRotated(2f);

                if (CamerasAngle <= 95f && CamerasAngle >= 85f && !phoneRotated)//if phone is in right angle
                {
                    //READY FOR NEXT SCREEN
                    phoneRotated = true;
                    StartCoroutine(delayCalibrateFloor(0.2f));
                    //planeManager.enabled = true;
                    //calibrateFloor();
                    keepRotatingIMG.SetActive(false);
                    intructionsToLowerPhone.SetActive(true);
                }
                else
                {
                    //NOT RIGHT ANGLE
                    keepRotatingIMG.SetActive(true);
                    if (Input.touchCount > 0)
                    {
                        if (Input.GetTouch(0).tapCount > 0)//SKIP THIS STEP WITH A TOUCH TO A SCREEN
                        {
                            phoneRotated = true;
                            StartCoroutine(delayCalibrateFloor(0.2f));
                            //planeManager.enabled = true;
                            //calibrateFloor();
                            keepRotatingIMG.SetActive(false);
                            intructionsToLowerPhone.SetActive(true);
                        }
                    }
                }
            }
        }

        IEnumerator delayPhoneRotated(float time)
        {
            yield return new WaitForSeconds(time);
            print("done");

        }

        IEnumerator delayFirstScreen(float time)
        {
            yield return new WaitForSeconds(time);

        }

        //VARIABLES FOR 2ND SCREEN
        public bool hasTilted = false;
        public bool hasPlaced = false;
        public bool isReadyToTiltUp = false;
        public bool hasTiltedUp = false;
        private bool executedTiltedUp_Coroutine = false;

        void calibrateFloor()//FLOOR SCANNING METHOD, HAS 4 PARTS
        {
            keepRotatingIMG.SetActive(false);

            float CamerasAngleX = mainCamera.transform.eulerAngles.x;//GET PHONE ANGLE

            if (!hasTilted)//******TILT PHONE DOWN
            {
                print("1");
                if (CamerasAngleX <= 70f && CamerasAngleX >= 60f)
                {
                    //READY FOR NEXT SCREEN
                    intructionsToLowerPhone.SetActive(false);
                    hasTilted = true;
                }
            }

            //yield return new WaitForSeconds(1f);

            if (hasTilted && !hasPlaced)//****WHEN TILTED DOWN STAR SCANNING FLOOR, AND INSTANTIATE INVISIBLE CUBE, SO WE KNOW WE HAVE AR PLANE
            {
                print("2");
                planeManager.enabled = true;//ENABLE SCANNING
                if (planeManager.trackables.count >= 1)//IF WE HAVE 1 OR MORE AR PLANES
                {
                    instantiateObject();//RAYCAST FROM SCREEN TO AR PLANE, IF THERE IS A HIT, CREATE INVISIBLE OBJECT
                }
            }

            //yield return new WaitForSeconds(1f);

            if (hasPlaced && isReadyToTiltUp)//****WHEN FLOOR SCANNED, MOVE PHONE UP
            {
                print("3");
                if (CamerasAngleX <= 355f && CamerasAngleX >= 345f)
                {
                    //READY FOR NEXT SCREEN
                    print("TILTED UP!");
                    instructionsToUpwardPhone.SetActive(false);
                    hasTiltedUp = true;
                    isReadyToTiltUp = false;
                }
            }



            if (hasTiltedUp && !executedTiltedUp_Coroutine)//PHONE MOVED BACK UP, REACY FOR OBJECT PLAEMENT
            {
                print("4");
                placeElement_screen.SetActive(true);


                if (!EventSystem.current.IsPointerOverGameObject() && Input.GetTouch(0).phase == TouchPhase.Ended)
                {
                    //SKIP PRESSING BUTTON WITH PRESSING SCREEN
                    placeElement_screen.SetActive(false);
                    pressed_Place_element_button();
                    infoButton.SetActive(true);
                    infoDescription.SetActive(true);
                    hasCompletedAll = true;
                    executedTiltedUp_Coroutine = true;
                }

                //StartCoroutine(hasTiltedDelay());
            }
        }

        IEnumerator hasTiltedDelay()
        {
            executedTiltedUp_Coroutine = true;

            yield return new WaitForSeconds(2f);

            placeElement_screen.SetActive(true);


        }

        //***************A PART OF GAME LOGIC*********************

        void instantiateObject()//***INSTANTIATE AR OBJECT
        {
            Ray ray = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0f));//RAY FROM MIDDLE OF SCREEN
            if (raycastManager.Raycast(ray, hits))//IF THERE IS A HIT
            {
                Pose pose = hits[0].pose;

                //Instantiate(objectToInstantiate, pose.position, pose.rotation);
                objectToInstantiate.transform.position = pose.position;
                objectToInstantiate.transform.rotation = pose.rotation;
                //objectToInstantiate.SetActive(true);

                hasScannedFloorUI.SetActive(true);

                hasPlaced = true;
            }
        }

        bool IsPointerOverUIObject(Vector2 pos)//CHECK IF TOUCH IS OVER UI
        {
            if (EventSystem.current == null)
                return false;

            PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
            eventDataCurrentPosition.position = new Vector2(pos.x, pos.y);
            List<RaycastResult> results = new List<RaycastResult>();
            EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
            return results.Count > 0;
        }

        public void readyToTiltUp()//****READY TO TILT THE PHONE UP
        {
            //isReadyToTiltUp = true;
            StartCoroutine(delayReadyToTiltUp(1f));
        }

        IEnumerator delayReadyToTiltUp(float time)
        {
            yield return new WaitForSeconds(time);
            isReadyToTiltUp = true;
        }

        public void pressed_Place_element_button()
        {
            //hasTiltedUp = false;
            StartCoroutine(delayPressedPlaceElementButton(1f));
        }

        IEnumerator delayPressedPlaceElementButton(float time)
        {
            yield return new WaitForSeconds(time);
            hasTiltedUp = false;
        }

        bool hasShownAlready = false;
        public void showHowToUseButtons()//SHOW BUTTONS
        {
            if (!hasShownAlready)
            {
                howToInsertElements.SetActive(true);
                charactersButton.SetActive(true);
                objectsButton.SetActive(true);
                scenesButton.SetActive(true);

                touchManager.enabled = true;//ENABLE toucManager SCRIPT
            }
        }

        public void resetHowToUseButtons()//RESET TUTORIAL, WHEN STARTING GAME AGAIN
        {
            hasShownAlready = false;
            howToInsertElements.SetActive(false);
            touchManager.enabled = true;
        }

        public void hideHowToUseButtons()//WE NEED BUTTONS TO SHOW ONLY ONCE
        {
            hasShownAlready = true;
            howToInsertElements.SetActive(false);
        }

        IEnumerator delayCalibrateFloor(float time)
        {
            yield return new WaitForSeconds(time);
            calibrateFloor();

        }

        public void closeAR()//CLOSE AR EXPERIENCE - NAVIGATE TO THE FIRST SCREEN (start of the whole game)
        {
            touchManager.enabled = false;

            positionAsInImage.SetActive(true);
            positionIMG.SetActive(true);

            firstScreenNext = false;
            phoneRotated = false;
            planeManager.enabled = false;
            isRotated = false;

            intructionsToLowerPhone.SetActive(false);
            keepRotatingIMG.SetActive(false);
            objectToInstantiate.SetActive(false);
            hasScannedFloorUI.SetActive(false);
            instructionsToUpwardPhone.SetActive(false);
            placeElement_screen.SetActive(false);
            howToInsertElements.SetActive(false);
            charactersButton.SetActive(false);
            objectsButton.SetActive(false);
            scenesButton.SetActive(false);

            hasTilted = false;
            hasPlaced = false;
            isReadyToTiltUp = false;
            hasTiltedUp = false;
            hasCompletedAll = false;

            executedTiltedUp_Coroutine = false;
        }
    }
}