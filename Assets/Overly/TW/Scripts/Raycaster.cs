﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using Lean.Touch;

namespace Overly.TWScripts
{
    [DisallowMultipleComponent]
    public class Raycaster : MonoBehaviour
    {
        [HideInInspector]
        public static Raycaster instance;

        RaycastHit[] lastHits;
        public RaycastHit closestHit = new RaycastHit();

        public Dictionary<GameObject, Texture2D> textureList = new Dictionary<GameObject, Texture2D>();

        private void Awake()
        {
            if (instance == null)
            {
                instance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }

        void LateUpdate()
        {

            lastHits = null;
        }

        public bool CheckRaycastHit(Ray ray, Collider collider)
        {
            if (lastHits == null)
            {
                lastHits = Physics.RaycastAll(ray);
            }
            float closestDistance = -1;
            foreach (RaycastHit hit in lastHits)
            {
                if (hit.distance < closestDistance || closestDistance < 0)
                {
                    closestDistance = hit.distance;
                    closestHit = hit;
                }
            }
            if (closestDistance > 0)
            {
                if (closestHit.collider == collider)
                {
                    return true;
                }
            }

            return false;
        }

        bool CheckVideoHit(RaycastHit hitInfo)
        {
            Vector2 uv = hitInfo.textureCoord;

            if (!textureList.ContainsKey(hitInfo.collider.gameObject))
            {
                RefreshVideoList();
            }
            Texture2D texture2D = textureList[hitInfo.collider.gameObject];

            Vector2 clickPos = new Vector2((int)(uv.x * texture2D.width), (int)(uv.y * texture2D.height));

            if (texture2D.GetPixel((int)clickPos.x, (int)clickPos.y).r > 0.1f)
            {
                return true;
            }
            return false;
        }

        public void RefreshVideoList()
        {
            GameObject[] gameObjects = textureList.Keys.ToArray<GameObject>();
            Texture2D[] textures2d = textureList.Values.ToArray<Texture2D>();

            for (int i = textureList.Count - 1; i >= 0; i--)
            {
                textureList.Remove(gameObjects[i]);
                Destroy(textures2d[i]);
            }
            foreach (serginian.Media.VideoPlayerTW vplayer in FindObjectsOfType<serginian.Media.VideoPlayerTW>())
            {
                Material mat = vplayer.GetComponent<Renderer>().material;
                if (mat != null)
                {
                    Texture textureOrigin = mat.GetTexture("_Mask");
                    if (textureOrigin != null)
                    {
                        RenderTexture currentRT = RenderTexture.active;

                        RenderTexture renderTexture = new RenderTexture(textureOrigin.width, textureOrigin.height, 32);
                        Graphics.Blit(textureOrigin, renderTexture);

                        Texture2D texture2D = new Texture2D(textureOrigin.width, textureOrigin.height / 2, TextureFormat.RGBA32, false);
                        RenderTexture.active = renderTexture;
                        texture2D.ReadPixels(new Rect(0, renderTexture.height / 2, renderTexture.width, renderTexture.height / 2), 0, 0);
                        texture2D.Apply();
                        RenderTexture.active = currentRT;

                        textureList.Add(vplayer.gameObject, texture2D);
                        Destroy(renderTexture);
                    }
                    else
                    {
                        Debug.LogError("No texture");
                    }
                }
                else
                {
                    Debug.LogError("No material");
                }
            }

        }


        public bool CheckRaycastHitVideoAlpha(Ray ray, Collider collider)
        {
            if (textureList.Count == 0)
            {
                RefreshVideoList();
            }
            if (lastHits == null)
            {
                lastHits = Physics.RaycastAll(ray);
            }
            float closestDistance = -1;

            foreach (RaycastHit hit in lastHits)
            {
                if (hit.collider.GetComponent<serginian.Media.VideoPlayerTW>() != null) // if has video
                {
                    bool wasHit = CheckVideoHit(hit);
                    if ((hit.distance < closestDistance || closestDistance < 0) && wasHit)
                    {
                        closestDistance = hit.distance;
                        closestHit = hit;
                    }
                }
                else
                {
                    if ((hit.distance < closestDistance || closestDistance < 0))
                    {
                        closestDistance = hit.distance;
                        closestHit = hit;
                    }
                }
            }

            if (closestDistance > 0)
            {
                if (closestHit.collider.gameObject == collider.gameObject)
                {
                    return true;
                }
            }
            return false;
        }
    }
}
