﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Overly.TWScripts
{
public class CameraHelper: MonoBehaviour
{
    // Start is called before the first frame update
    void Awake()
    {
        Camera camera = GetComponent<Camera>();
        camera.depthTextureMode = DepthTextureMode.DepthNormals;
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
}