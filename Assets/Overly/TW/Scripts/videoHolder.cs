﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

using UnityEngine.Video;

namespace Overly.TWScripts
{
public class videoHolder : MonoBehaviour
{

    public GameObject[] ObjectsArray;
    public GameObject[] CharacterArray;

    [HideInInspector] public int activeLayersInScene = 0;

    public GameObject bin_IMG;
    public GameObject warningLabel;
    private string path;

    [HideInInspector] public bool showWarning = false;

    void Start()
    {

    }

    void Update()
    {
        countActiveLayers();//AWLAYS COUNT HOW MANY VIDEOS ARE IN THE SCENE

        if (showWarning)//IF MAX REACHED, THEN SHOW WARNING
        {
            StartCoroutine(turnWarningOn(1f));//SHOW WARNING FOR 1 SECOND
        }
    }


    [HideInInspector] public bool hasReachedMaxLayers = false;
    public void countActiveLayers()
    {
        activeLayersInScene = GameObject.FindGameObjectsWithTag("VideoLayer").Length;//FIND ALL GAMEOBJECTS WOTH TAG
        //print("active layers: " + activeLayersInScene);
        if (activeLayersInScene < 3 && activeLayersInScene >= 0)//IF HAVENT REACHED MAX LAYERS
        {
            //can add layers
            hasReachedMaxLayers = false;
            //print("has reached: " + hasReachedMaxLayers);
            return;

        }
        else if (activeLayersInScene >= 3)//HAVE REACHED MAX LAYERS
        {
            //cant add more layers
            hasReachedMaxLayers = true;
            //print("has reached: " + hasReachedMaxLayers);
            return;
        }
    }

    public IEnumerator turnWarningOn(float time)
    {
        showWarning = false;
        warningLabel.SetActive(true);

        yield return new WaitForSeconds(time);

        warningLabel.SetActive(false);
    }

    private GameObject[] layersToDestroy;
    public void destroyVideosOnClose()//DESTROY ALL ACTIVE VIDEO LAYERS ON CLOSE (WHEN GO TO START, WHEB PRESSED CLOSE BUTTON)
    {

        //}
        //layersToDestroy = GameObject.FindGameObjectsWithTag("VideoLayer");
        //print("active in scene: " + layersToDestroy);

        //foreach (var layer in layersToDestroy)
        //{
        //    layer.GetComponent<videoProperties>().destroyThisObject();
        //}

        print("active in scene: " + activeLayersInScene + " | " + hasReachedMaxLayers);
        print("Left after destroy: " + GameObject.FindGameObjectsWithTag("VideoLayer").Length);
    }
}
}