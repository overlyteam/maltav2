﻿using serginian.Media;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.Video;

namespace Overly.TWScripts
{
public class videoProperties : EventTrigger
{
    [HideInInspector] public int nrOfVideoAssigned;
    [HideInInspector] public videoHolder videoLayerHolder;
    [HideInInspector] public bool useCharacterVideos;

    private Camera cameraMain;
    private VideoPlayerTW _videoPlayer;
    private GameObject videoLayer;
    private bool hasPlayed = false;
    protected bool canCreate = false;
    private videoManager videoManager;
    private GameObject playIcon;
    private GameObject binIcon;
    private Vector3 lookAtAngle; 
    void Start()
    {
        if (videoManager == null)
            videoManager = FindObjectOfType<videoManager>();
        if (playIcon == null)
            playIcon = videoManager.playIcon;
        if (binIcon == null)
            binIcon = videoManager.binIcon;

        cameraMain = Camera.main;
    }


    void Update()
    {
    }

    IEnumerator prepareDelay(float time)
    {
        yield return new WaitForSeconds(time);
    }

    public void createCharacterVideo()
    {
        _videoPlayer = GetComponent<VideoPlayerTW>();//get components from gameobject

        videoLayerHolder = FindObjectOfType<videoHolder>();

//        _videoPlayer.Clip = videoLayerHolder.videoClips_character[nrOfVideoAssigned];//assign video to this specific gameobject
//        _videoPlayer.Load(videoLayerHolder.videoClips_character[nrOfVideoAssigned]);//laod it
        gameObject.SetActive(true);//make it active 
        //_videoPlayer.Play();// dont play it right away
    }

    public void createObjectVideo()
    {
        _videoPlayer = GetComponent<VideoPlayerTW>();
        videoLayerHolder = FindObjectOfType<videoHolder>();

 //       _videoPlayer.Clip = videoLayerHolder.videoClips_object[nrOfVideoAssigned];
 //       _videoPlayer.Load(videoLayerHolder.videoClips_object[nrOfVideoAssigned]);

        gameObject.SetActive(true);
        // _videoPlayer.Play();// dont play it right away
    }

    public void destroyThisObject()
    {
        Destroy(obj: gameObject);//destroy this gameobject
        print("destroyed " + gameObject.name);
    }
}
}