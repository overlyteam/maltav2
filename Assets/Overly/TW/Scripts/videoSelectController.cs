﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using TMPro;
using UnityEngine.XR.ARFoundation;
using UnityEngine.XR.ARSubsystems;
using UnityEngine.EventSystems;

namespace Overly.TWScripts
{
public class videoSelectController : MonoBehaviour
{
    [Header("VIDEOS")]
    public VideoClip[] videoClipsFront;
    public VideoClip[] videoClipsBack;
    public VideoClip[] videoClipsFull;

    [Header("SETTINGS")]
    public TextMeshProUGUI videoText;
    public VideoSet frontVideo;
    public TextMeshProUGUI button;
    public GameObject ArObject;
    public GameObject arCamera;

    public ARRaycastManager m_RaycastManager;
    public ARPlaneManager m_PlaneManager;


    private int currentVideo;
    private int currentBackVideo;
    private bool fullVideoActive;
    private bool canPress;
    private bool isEnabled;
    static List<ARRaycastHit> hits = new List<ARRaycastHit>();

    // Start is called before the first frame update
    void Start()
    {
        currentVideo = 0;
        switchVideo(currentVideo);

        videoText.text = string.Format("{0}. video's name = {1}", currentVideo , videoClipsFront[currentVideo].name);
        fullVideoActive = false;
        canPress = true;

        ArObject.SetActive(false);

        button.text = "Full video = " + fullVideoActive;

        
    }

    // Update is called once per frame
    void Update()
    {
       
        if (EventSystem.current.IsPointerOverGameObject())//CHECKS FOR UI OBJECTS
            return;

        if (!TryGetTouchPosition(out Vector2 touchPosition))
            return;

        if (m_RaycastManager.Raycast(touchPosition, hits, TrackableType.PlaneWithinPolygon) && canPress)
        {
            print("touched plane!");
            canPress = false;//first touch has been registered, so setting canPress to false so user can't reposition videoLayer. Resets to true at video change
            
            var hitPose = hits[0].pose;

            ArObject.SetActive(true);

            Vector3 sameHeight = new Vector3(hitPose.position.x, arCamera.transform.position.y, hitPose.position.z);
            ArObject.transform.position = sameHeight;
            ArObject.transform.rotation = hitPose.rotation;

            SetAllPlanesActive(false);//disable detected floor
            m_PlaneManager.detectionMode = PlaneDetectionMode.None;//dont scan floor
            m_PlaneManager.enabled = false;
            m_RaycastManager.enabled = false;

        }

        if (fullVideoActive)
        {
            videoText.text = string.Format("{0}. video's name = {1}", currentVideo, videoClipsFull[currentVideo].name);
            return;
        }
        videoText.text = string.Format("{0}. video's name = {1}", currentVideo, videoClipsFront[currentVideo].name);

    }


    //******************************BUTTONS****************************
    public void nextVideo()
    {
        if(currentVideo < videoClipsFront.Length)
        {
            if (fullVideoActive)
            {
                currentVideo++;
                switchVideo(currentVideo);

                frontVideo.gameObject.SetActive(false);//resets so new video shows up
                frontVideo.gameObject.SetActive(true);

                return;
            }

            currentVideo++;
            switchVideo(currentVideo);

            frontVideo.gameObject.SetActive(false);//resets so new video shows up
            frontVideo.gameObject.SetActive(true);

            print("NEXT ONE");
        }

    }

    public void prevVideo()
    {
        if (currentVideo >= 0)
        {
            if (fullVideoActive)
            {
                currentVideo--;
                switchVideo(currentVideo);

                frontVideo.gameObject.SetActive(false);//resets so new video shows up
                frontVideo.gameObject.SetActive(true);

                return;
            }

            currentVideo--;
            switchVideo(currentVideo);

            frontVideo.gameObject.SetActive(false);//resets so new video shows up
            frontVideo.gameObject.SetActive(true);

            print("PREVIOUS ONE");
        }
    }

    //-----------------------SOME-VOIDS---------------------------------
    public void switchVideo(int i)
    {
        if (fullVideoActive)
        {
           // frontVideo.ClipFront = videoClipsFull[i];
            return;
        }

       // frontVideo.ClipFront = videoClipsFront[i];

        print("SWITCHED TO " + i + ". video // " + videoClipsBack[i].name);
    }

    public void switchToFullVideo()
    {
        fullVideoActive = !fullVideoActive;
        button.text = "Full video = " + fullVideoActive;
    }

    public void SetAllPlanesActive(bool value)
    {
        foreach (var plane in m_PlaneManager.trackables)
            plane.gameObject.SetActive(value);
    }

    public void Put(Vector3 position)
    {

        ArObject.transform.position = position;

        var m_Camera = Camera.main;
        transform.LookAt(transform.position + m_Camera.transform.rotation * Vector3.forward, m_Camera.transform.rotation * Vector3.up);
        var w = transform.eulerAngles;
        w.x = 0f;
        w.z = 0f;
        transform.eulerAngles = w;

    }

    private bool TryGetTouchPosition(out Vector2 touchPosition)
    {
        if(Input.touchCount > 0)
        {
            touchPosition = Input.GetTouch(0).position;
            return true;
        }

        touchPosition = default;
        return false;
    }

    public void resetScan()
    {
        SetAllPlanesActive(true);
        m_PlaneManager.detectionMode = PlaneDetectionMode.Horizontal;
        m_PlaneManager.enabled = true;
        m_RaycastManager.enabled = true;

        canPress = true;
    }

    public void distancePlus()
    {
        //float z = ArObject.transform.position.z;

        float z = 0.1f;

        ArObject.transform.position = new Vector3(ArObject.transform.position.x, ArObject.transform.position.y, ArObject.transform.position.z + z);
    }

    public void distanceMinus()
    {
        //float z = ArObject.transform.position.z;

        float z = 0.1f;

        ArObject.transform.position = new Vector3(ArObject.transform.position.x, ArObject.transform.position.y, ArObject.transform.position.z - z);
    }
}
}