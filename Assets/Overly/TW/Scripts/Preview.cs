﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using serginian.MediaBack;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.Android;
using UnityEngine.Events;
using NatSuite.Sharing;
#if UNITY_IOS
using UnityEngine.iOS;
#endif
namespace Overly.TWScripts
{
    public class Preview : MonoBehaviour
    {
        public Texture defTexture;
        public RawImage targetImage;
        public VideoPlayerExBack player;

        private string videoPath = null;
        Texture2D screenshot = null;
        //    private Texture2D screenshot = null;

        public void Show(Texture2D screenshot)
        {
            Open();
            this.screenshot = screenshot;
            targetImage.texture = screenshot; // Sprite.Create(screenshot, new Rect(0, 0, screenshot.width, screenshot.height), new Vector2(0.5f, 0.5f));
            targetImage.enabled = true;
        }

        public void Show(string path)
        {
            Open();
            targetImage.transform.GetComponent<AspectRatioFitter>().aspectRatio = Convert.ToSingle(Screen.width) / Convert.ToSingle(Screen.height);

            videoPath = path;
            // bool res = base.Show(o);
            player.AutoPlay = true;
            Debug.LogError("Preview of this video: " + videoPath);
            player.Load(videoPath);
            //EnableImage();
            Invoke("EnableImage", 0.1f);
        }

        public void EnableImage()
        {
            targetImage.enabled = true;
        }

        void Open()
        {
            gameObject.SetActive(true);
        }

        public void CloseAndRelease()
        {
            player.Stop();

            //if (File.Exists(videoPath))
            //   TryDelete(videoPath);
            videoPath = null;

            if (screenshot != null)
                Destroy(screenshot);
            screenshot = null;

            targetImage.texture = defTexture;
            gameObject.SetActive(false);
        }

        public void Cancel()
        {
            CloseAndRelease();
        }

        public void Share()
        {
            // StartCoroutine(CheckPermissionAndSave());
            //Save();
            ShareToNetwork();
        }

        public void Save()
        {
            StartCoroutine(CheckPermissionAndSave());
        }

        private IEnumerator CheckPermissionAndSave()
        {
#if UNITY_ANDROID
            if (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite))
            {
                Permission.RequestUserPermission(Permission.ExternalStorageWrite);
            }
            int tries = 100;
            while (!Permission.HasUserAuthorizedPermission(Permission.ExternalStorageWrite) && tries > 0)
            {
                yield return new WaitForSeconds(0.2f);
                tries--;
            }
            Debug.Log("Save");
#endif
            SaveToCameraRoll();
            // iOS allows saving in sharescreen
            yield return null;
        }

        async void ShareContent()
        {
            if (screenshot != null)
            {
                var success = await new SharePayload().AddImage(screenshot).Commit();
                Debug.Log($"Successfully shared item: {success}");
            }
            if (videoPath != null)
            {
                var success = await new SharePayload().AddMedia(videoPath).Commit();
                Debug.Log($"Successfully shared item: {success}");
            }
        }

        async void SaveContent()
        {
            Debug.LogError("Saving");
            if (screenshot != null)
            {
                var success = await new SavePayload().AddImage(screenshot).Commit();
                Debug.Log($"Successfully saved item: {success}");
                CloseAndRelease();
            }
            if (videoPath != null)
            {
                var success = await new SavePayload().AddMedia(videoPath).Commit();
                Debug.Log($"Successfully saved item: {success}");
                CloseAndRelease();
            }
        }

        void SaveToCameraRoll()
        {
            SaveContent();
        }

        void ShareToNetwork()
        {
            ShareContent();
        }

    }
}