﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.SceneManagement;

namespace Overly.TWScripts
{
    public class TWManager : MonoBehaviour
    {
        public static TWManager instnance;

        [HideInInspector]
        public Plane infiniteGroundPlane;
        public enum CONTENT_TYPE { NO_DATA, DATABASE, SINGLE };
        public CONTENT_TYPE contentType;
        public UnityEvent OnDBEnabled;
        public UnityEvent OnSingleEnabled;
        // Start is called before the first frame update
        private void Awake()
        {
            if (instnance == null)
            {
                instnance = this;
            }
            else
            {
                Destroy(gameObject);
            }
        }
        public void DBEnable()
        {
            if (contentType == CONTENT_TYPE.DATABASE)
            {
                OnDBEnabled.Invoke();
            }
        }
        public void SingleEnalbe()
        {
            OnSingleEnabled.Invoke();
        }
        public void StartTW()
        {
            switch (contentType)
            {
                case CONTENT_TYPE.DATABASE:
                    FindObjectOfType<ScanFloor>().HideGroundPlane();
                    OnDBEnabled.Invoke();
                    break;
                case CONTENT_TYPE.SINGLE:
                    FindObjectOfType<ScanFloor>().HideGroundPlane();
                    OnSingleEnabled.Invoke();
                    break;
                default:
                    EndTW();
                    break;
            }
        }

        public void EndTW()
        {
         //   SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
            SceneManager.LoadScene(GameSettings.instance.LevelToReturnTo);
        }
    }
}