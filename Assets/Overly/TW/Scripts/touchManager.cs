﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using Lean.Touch;

namespace Overly.TWScripts
{
    public class touchManager : MonoBehaviour
    {
        public RectTransform binObject;
        public GameObject playIcon;
        public float lerpSpeed;

        public GameObject screenShotPanel;

        public bool playModeOn = false;


        [HideInInspector] public bool isHoveringOver;
        [HideInInspector] public bool isHoveringOverDelay;

        List<RaycastResult> hitObjects = new List<RaycastResult>();
        private GameObject objectToDestroy;
        private scanTutorial scanTutorial;
        void Start()
        {
            screenShotPanel.SetActive(false);
            scanTutorial = gameObject.GetComponent<scanTutorial>();
        }

        private void LateUpdate()
        {
            isHoveringOver = isHoveringOverDelay;
        }
        public bool EnterDragMode(GameObject dragingThisObject)
        {
            if (!playModeOn)
            {
                binObject.gameObject.SetActive(true);
                playIcon.SetActive(false);
                turnOffButtons();
                objectToDestroy = dragingThisObject;
                objectToDestroy.GetComponent<LeanDragTranslate>().enabled = true;//ENABLE LEAN CONTROLS
                if (objectToDestroy.GetComponent<LeanTwistRotate>() != null)
                {
                    objectToDestroy.GetComponent<LeanTwistRotate>().enabled = true;
                }
                if (objectToDestroy.GetComponent<LeanTwistRotate>() != null)
                {
                    objectToDestroy.GetComponent<LeanPinchScale>().enabled = true;
                }
                return true;
            }
            else
            {
                return false;
            }
        }

        public void ExitDragMode()
        {
            binObject.gameObject.SetActive(false);
            playIcon.SetActive(true);
            if (isHoveringOver)
            {
                destroyTouchedObject();
            }
            if (objectToDestroy != null)
            {
                objectToDestroy.GetComponent<LeanDragTranslate>().Disable();
                objectToDestroy.GetComponent<LeanDragTranslate>().enabled = false;//FOR LEAN TOUCH CONTROLS
                if (objectToDestroy.GetComponent<LeanTwistRotate>() != null)
                {
                    objectToDestroy.GetComponent<LeanTwistRotate>().enabled = false;
                }
                if (objectToDestroy.GetComponent<LeanTwistRotate>() != null)
                {
                    objectToDestroy.GetComponent<LeanPinchScale>().enabled = false;
                }
            }
            turnOnButtons();
        }

        public void changeVideos(GameObject videoObject)
        {
            objectToDestroy = videoObject;
            VideoSet temp = objectToDestroy.GetComponent<VideoSet>();
            if (temp != null)
            {
                VideoSet videoSet = objectToDestroy.GetComponent<VideoSet>();
                if (videoSet.videosToRotate.Count > 1)//IF THERE IS MORE THAN 1 VIDEO
                {
                    videoSet.rotateVideos();//CHANGE VIDEO
                }
            }

            objectToDestroy.GetComponent<LeanDragTranslate>().Disable();
            objectToDestroy.GetComponent<LeanDragTranslate>().enabled = false;//FOR LEAN TOUCH CONTROLS
            if (objectToDestroy.GetComponent<LeanTwistRotate>() != null)
            {
                objectToDestroy.GetComponent<LeanTwistRotate>().enabled = false;
            }
            if (objectToDestroy.GetComponent<LeanTwistRotate>() != null)
            {
                objectToDestroy.GetComponent<LeanPinchScale>().enabled = false;
            }

            objectToDestroy = null;//RESET ACTIVE OBJECT
        }

        void turnOnButtons()
        {
#if UNITY_EDITOR
            return;
#endif
            //  scanTutorial.objectsButton.SetActive(true);
            //  scanTutorial.charactersButton.SetActive(true);
        }

        void turnOffButtons()
        {
#if UNITY_EDITOR
            return;
#endif
            // scanTutorial.objectsButton.SetActive(false);
            //  scanTutorial.charactersButton.SetActive(false);
        }

        public void destroyTouchedObject()//doesnt work from event triggers
        {
            if (objectToDestroy.GetComponentInParent<VideoPositioner>() != null)
            {
                Destroy(objectToDestroy.transform.parent.gameObject);
            }
            else
            {
                Destroy(objectToDestroy);
            }
        }

        public void hoveringOverBin()//IF IS OVER BIN, MAKE IT BIGGER
        {
            isHoveringOverDelay = true;
            Vector2 startSize = binObject.sizeDelta;
            Vector2 bigIcon = new Vector2(300f, 300f);

            Vector2 sizeLerp = Vector2.Lerp(binObject.sizeDelta, bigIcon, Time.deltaTime * lerpSpeed);
            binObject.sizeDelta = sizeLerp;
        }

        public void exitHoverOverBin()//IF USER MOVES OUT OF BIN IMAGE
        {
            isHoveringOverDelay = false;
            Vector2 normalIcon = new Vector2(200f, 200f);
            Vector2 startSize = binObject.sizeDelta;

            Vector2 sizeLerp = Vector2.Lerp(startSize, normalIcon, Time.deltaTime * lerpSpeed);
            binObject.sizeDelta = sizeLerp;//MAKE IT SMALLER

            // destroyTouchedObject();// Destroy(objectToDestroy);//DESTROY TOUCHED OBJECT (ACTIVE OBJECT)
            //objectToDestroy = null;//THERE IS NO ACTIVE OBJECT
            // createVideo.nrOfVideosCreated--;//MINUS 1 VIDEO IN THE SCENE

            //   playIcon.SetActive(true);
            // binObject.gameObject.SetActive(false);

            //  isDragging = false;
            //  playModeOn = false;

            //  turnOnButtons();
        }

        public void enteredPlayMode()
        {
            GetComponent<ScanFloor>().HideGroundPlane();
            Debug.Log("entered play mode");
            turnOffButtons();

            foreach (GameObject videoObject in GameObject.FindGameObjectsWithTag("VideoLayer"))
            {
                VideoSet videoSet = videoObject.GetComponent<VideoSet>();
                if (videoObject.GetComponent<VideoSet>() != null)
                {
                    if (!videoSet.IsPlaying)
                    {
                        videoSet.Play();
                    }
                }
                else
                {
                    videoObject.GetComponent<Renderer>().enabled = false;
                }
            }
            playModeOn = true;

        }

        public void exitPlayMode()
        {
            GetComponent<ScanFloor>().ShowGroundPlane();
            turnOnButtons();

            foreach (GameObject videoObject in GameObject.FindGameObjectsWithTag("VideoLayer"))
            {
                VideoSet videoSet = videoObject.GetComponent<VideoSet>();
                if (videoSet != null)
                {
                    videoSet.Stop();
                }
                else
                {
                    videoObject.GetComponent<Renderer>().enabled = true;
                }
            }
            playModeOn = false;

        }

        IEnumerator screenshotPanel_onOff()
        {
            screenShotPanel.SetActive(true);

            yield return new WaitForSeconds(1.5f);

            screenShotPanel.SetActive(false);
        }


        public void screenshotTaken(Texture2D screenshot)
        {
            Debug.Log("SaveImage");


            StartCoroutine(screenshotPanel_onOff());
        }

        public void SaveScreenshot(Texture2D screenshot)
        {
            StartCoroutine(screenshotPanel_onOff());
        }
    }
}