﻿using UnityEngine.Events;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

namespace Overly.TWScripts
{
    //USED TO POPULATE CHARACTER AND OBJECT GRID WITH BUTTON PREFABS
    public class populateSubGrid : MonoBehaviour
    {
        public string sectionName;
        public GameObject prefab_subgridElement;
        public UnityEvent OnChildClicked;
        public void PopulateGridWithContent(Content.VideoList videoList)
        {
            // destroy previous elements
            if (transform.childCount > 0)
            {
                for (int i = transform.childCount - 1; i >= 0; i--)
                {
                    Destroy(transform.GetChild(i).gameObject);
                }
            }

            // add new ones
            GameObject newObj;
            for (int i = 0; i < videoList.videos.Length; i++)
            {
                newObj = (GameObject)Instantiate(prefab_subgridElement, transform);
                newObj.GetComponent<Button>().onClick.AddListener(OnChildClicked.Invoke);

                TextMeshProUGUI element_name_text = newObj.GetComponentInChildren<TextMeshProUGUI>();
                element_name_text.text = videoList.name + " " + i;

                string url = "";
#if UNITY_ANDROID
                url = System.IO.Path.Combine(Application.persistentDataPath, GameSettings.pathToARContent, videoList.videos[i]);
#else
                url = System.IO.Path.Combine(Application.persistentDataPath, GameSettings.pathToARContent, videoList.videos[i]);
#endif
                serginian.MediaBack.VideoPlayerExBack preview = newObj.GetComponent<serginian.MediaBack.VideoPlayerExBack>();
                preview.Load(url);

                createVideo createVideo = newObj.GetComponent<createVideo>();//access prefab components
                createVideo.videoUrls = new string[1];
                createVideo.videoUrls[0] = url;

            }
        }

//        void PopulateGridWithContentOld()
//        {
//            if (ContentLoader.instance.contentDBList == null || ContentLoader.instance.contentDBList?.Count == 0)
//            {
//                Debug.LogError("Empty video content list of DB!");
//                return;
//            }

//            Content.ContentSection mySect = null;
//            foreach (Content.ContentSection sect in ContentLoader.instance.contentDBList)
//            {
//                if (sect.name == sectionName)
//                {
//                    mySect = sect;
//                }
//            }
//            if (mySect == null)
//            {
//                Destroy(gameObject);
//                return;
//            }

//            GameObject newObj;
//            foreach (Content.VideoList videoList in mySect.videoList)
//            {
//                newObj = (GameObject)Instantiate(prefab_subgridElement, transform);
//                newObj.GetComponent<Image>().sprite = sprite;
//                newObj.GetComponent<Image>().color = Random.ColorHSV(0, 1, 0.5f, 1, 0.4f, 0.6f);
//                newObj.GetComponent<Button>().onClick.AddListener(OnChildClicked.Invoke);

//                createVideo createVideo = newObj.GetComponent<createVideo>();//access prefab components
//                string[] urls = new string[videoList.videos.Length];
//                for (int i = 0; i < videoList.videos.Length; i++)
//                {
//#if UNITY_ANDROID
//                    urls[i] = System.IO.Path.Combine(Application.persistentDataPath, GameSettings.pathToARContent, videoList.videos[i]);
//#else
//                urls[i] = System.IO.Path.Combine(Application.persistentDataPath, GameSettings.pathToARContent, videoList.videos[i]);
//#endif
//                }
//                createVideo.videoUrls = urls;
//                TextMeshProUGUI element_name_text = newObj.GetComponentInChildren<TextMeshProUGUI>();
//                element_name_text.text = videoList.name;
//            }
//        }
    }
}