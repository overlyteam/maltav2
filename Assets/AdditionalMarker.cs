﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdditionalMarker : MonoBehaviour
{
    public List<MeshRenderer> meshes;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void ShowMeshRenderer(bool show)
    {
        foreach (MeshRenderer mesh in meshes)
        {
            mesh.enabled = show;
        }
    }
}
